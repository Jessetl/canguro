#!/bin/sh
USER=${USER:-root}
HOST=${HOST:-0.0.0.0}

SOURCE=${SOURCE:-build}
TARGET=${TARGET:-/var/www/html/canguro-web}

echo "Removing existing files..."
ssh $USER@$HOST "rm -ifr $TARGET"
echo ""
echo "Uploading new files..."
echo ""
scp -r "$SOURCE" "$USER@$HOST:$TARGET"
echo ""
echo "Done"
