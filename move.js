const fs = require('fs-extra')


const NAME = "/var/www/html/Admin"

fs.remove(`${ NAME }/web`)
  .then(()=>{
    console.log('>>: Carpeta Eliminada')
      fs.rename(`${ NAME }/admin/build`, `${ NAME }/admin/web`, err => {
        if (err) {
          console.error(err)
          return
        }
        console.log("Nombre Modificado");
        
        fs.move(`${ NAME }/admin/web`, `${ NAME }/web`, err => {
          if (err) {
            console.error(err)
            return
        }
        console.log('Web Instalada');
      })
    })
  })
  .catch(err => {
    console.log('>>: Error al eliminar la carpeta', err)
  })
