import { combineReducers } from "redux";
import { TypedUseSelectorHook, useSelector } from "react-redux";
import { user } from "./user";
import { token } from "./token";
import { form } from "./form";
import { contact } from "./contact";

export const rootReducer = combineReducers({
  user,
  token,
  form,
  contact,
});

export type RootState = ReturnType<typeof rootReducer>;

export const useTypedSelector: TypedUseSelectorHook<RootState> = useSelector;

export default rootReducer;
