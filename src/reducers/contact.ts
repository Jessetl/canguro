import { InfoAction, InfoState } from "actions";
import { Reducer } from "redux";

export const contact: Reducer<InfoState, InfoAction> = (
  state = null,
  action
) => {
  switch (action.type) {
    case "Info/SET":
      return action.info;
    case "Info/REMOVE":
      return null;
    default:
      return state;
  }
};
