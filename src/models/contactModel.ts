import { DeliveryPhone } from "./deliveryPhone";

export interface Contact {
  data: ContactModel[];
}

export type ContactModel = {
  id?: number;
  email: string;
  horario: string;
  address: string;
  twitter: string;
  youtube: string;
  instagram: string;
  facebook: string;
  whatsapp: string;
  tlf: string;
  link_app_android: string;
  link_app_ios: string;
  deliveries_phones: DeliveryPhone[];
};
