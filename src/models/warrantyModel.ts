export type WarrantyModel = {
  id?: number;
  politics: string;
  warranty: string;
  shipments: string;
  terms_and_conditions: string;
  created_at?: Date;
  updated_at?: Date;
};
