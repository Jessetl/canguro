export type ImageBlogModel = {
  id: number;
  name: string;
  blog_id: number;
  image_url: string;
  created_at: Date;
  updated_at: Date;
};

export type ImageBlogForm = {
  id?: number;
  name: any;
  image_url: string;
  blog_id?: number;
};
