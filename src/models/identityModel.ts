export interface Identities {
  data: IdentityModel[];
  current_page: number;
  last_page: number;
}

export type IdentityModel = {
  id: number;
  name: string;
  description: string;
  image: string;
  image_url: string;
  created_at: Date;
  updated_at: Date;
};

export type IdentityForm = {
  id?: number;
  name: string;
  description: string;
  image: string;
};
