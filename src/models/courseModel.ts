export interface Courses {
  data: CourseModel[];
  current_page: number;
  last_page: number;
  to: number;
}

export type CourseModel = {
  id: number;
  name: string;
  description: string;
  ponentes: string;
  cost: any;
  cupos_disponibles: number;
  available: number;
  date: Date;
  status: boolean;
  inscriptions_count: number;
};

export type CourseForm = {
  id?: number;
  name: string;
  description: string;
  ponentes: string;
  cost: any;
  cupos_disponibles: number;
  date: Date;
  status: boolean;
};
