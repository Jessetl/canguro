export type DeliveryPhone = {
    id?: number,
    name: string,
    contact_id?: number
}