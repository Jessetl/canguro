export interface Values {
    data: ValueModel[];
  }
  
  export type ValueModel = {
    id?: number;
    name?: string;
    description?: string;
  };
  
