export type Token = {
  token: string;
  type: string;
};
