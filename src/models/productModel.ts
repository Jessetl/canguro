import { ImageProductForm, ImageProductModel } from "models";

export interface Products {
  data: Product[];
  current_page: number;
  last_page: number;
  to: number;
}

export type Product = {
  id: number;
  name: string;
  imageurl: string;
  image_url: string;
  description: string;
  status: number;
  image_products: ImageProductModel[];
  status_text: string;
  created_at: Date;
  updated_at: Date;
};

export type ProductForm = {
  id?: number;
  name: string;
  url: string;
  description: string;
  status: number;
  image_products: ImageProductForm[];
  image_deleted: string[];
};
