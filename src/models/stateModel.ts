import { City } from "./cityModel";

export type StateModel = {
  id: number;
  name: string;
  iso_3166: string;
  cities: City[];
};
