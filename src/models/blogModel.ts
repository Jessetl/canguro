import { ImageBlogModel, ImageBlogForm } from "./imageBlogModel";

export interface Blogs {
  data: BlogModel[];
  current_page: number;
  last_page: number;
  total: number;
  per_page: number;
  to: number;
}

export type BlogModel = {
  id: number;
  title: string;
  description: string;
  type: string;
  status: number;
  image: string;
  image_blogs: ImageBlogModel[];
  status_name: string;
  date_es: Date;
  expire_in: number;
  created_at: Date;
  updated_at: Date;
};

export type BlogForm = {
  id?: number;
  title: string;
  description: string;
  type: string;
  status: number;
  image: string;
  image_blogs: ImageBlogForm[];
  image_deleted: string[];
};
