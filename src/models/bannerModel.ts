export interface Banners {
  data: BannerModel[];
  current_page: number;
  last_page: number;
}

export type BannerModel = {
  id: number;
  name: string;
  type: number;
  url: string;
  texto: string;
  image_url: string;
  created_at: Date;
  updated_at: Date;
};

export type BannerForm = {
  id?: number;
  name: string;
  type: number | "";
  url: string;
  image_name: string;
  texto: string;
};
