export type CountryModel = {
  id: number;
  name: string;
};
