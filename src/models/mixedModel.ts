import { CountryModel } from "./countryModel";
import { StateModel } from "./stateModel";

export type CountryAndState = {
  countries: CountryModel[];
  states: StateModel[];
};
