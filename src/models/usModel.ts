export type UsModel = {
  id?: number;
  map: string;
  mision: string;
  mision_image: string;
  vision: string;
  vision_image: string;
  moral: string;
  us_image: string;
  kangaroo_to_world: string;
  identity_kangaroo: string;
  created_at?: Date;
  updated_at?: Date;
  us_image_url?: string;
  mision_image_url?: string;
  vision_image_url?: string;
};
