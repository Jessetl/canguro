export type ImageProductModel = {
  id: number;
  name: string;
  product_id: number;
  url: string;
  image_url: string;
  created_at: Date;
  updated_at: Date;
};

export type ImageProductForm = {
  id?: number;
  name: any;
  image_url: string;
  blog_id?: number;
};
