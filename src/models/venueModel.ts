import { CountryModel } from "./countryModel";
import { StateModel } from "./stateModel";

export interface Venues {
  data: VenueModel[];
  current_page: number;
  last_page: number;
  to: number;
}

export type VenueModel = {
  id: number;
  name: string;
  address: string;
  image: string;
  phone: string;
  web_url: string;
  ig_url: string;
  country_id: number;
  state_id: number;
  country: CountryModel;
  state: StateModel;
  image_url: string;
  created_at: Date;
  updated_at: Date;
  deleted_at: Date;
};

export type VenueForm = {
  id?: number;
  name: string;
  address: string;
  image_url: string;
  image: string;
  phone: string;
  web_url: string;
  ig_url: string;
  country_id: number | "";
  state_id: number | "";
  type: number | "";
  created_at?: Date;
  updated_at?: Date;
  deleted_at?: Date;
};
