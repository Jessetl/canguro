import { Range } from "./rangeModel";

export type Board = {
  id: number;
  range_id: number;
  type: number;
  name: string;
  min: string;
  max: string;
  range: Range;
  created_at: Date;
  updated_at: Date;
};
