
export type Response = {
    message: string;
    success: boolean;
};
  