export interface Brands {
  data: Brand[];
  current_page: number;
  last_page: number;
  to: number;
}

export type Brand = {
  id: number;
  name: string;
  description: string;
  fb_url: string;
  tw_url: string;
  ig_url: string;
  file: string;
  image_url: string;
  logo: string;
  logoGold: string;
  status: number;
  image_url_full: string;
  logo_url: string;
  logo_gold_url: string;
  link: string;

  created_at: Date;
  updated_at: Date;
  deleted_at: Date;
};

export type BrandForm = {
  id?: number;
  image_url: string;
  name: string;
  description: string;
  fb_url: string;
  tw_url: string;
  ig_url: string;
  logo: string;
  logoGold: string;
  status: number;
};
