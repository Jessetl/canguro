import { CourseModel } from "./courseModel";

export interface Inscriptions {
  data: Inscription[];
  current_page: number;
  last_page: number;
}

export type Inscription = {
  id: number;
  name: string;
  cedula: string;
  email: string;
  phone: string;
  reference: string;
  curso_id: number;
  approve: number;
  voucher: string;
  image_url: string;
  status_text: string;
  courses: CourseModel;
  created_at: Date;
  updated_at: Date;
};
