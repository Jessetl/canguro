import { ContactModel } from "models";
import { Action } from "redux";

export interface SetInfoAction extends Action<"Info/SET"> {
  info: ContactModel;
}

export type RemoveInfoAction = Action<"Info/REMOVE">;
export type InfoAction = SetInfoAction | RemoveInfoAction;
export type InfoState = ContactModel | null;

export const setInfo = (info: ContactModel): InfoAction => ({
  type: "Info/SET",
  info,
});

export const removeInfo = (): InfoAction => ({
  type: "Info/REMOVE",
});
