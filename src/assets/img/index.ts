import logo from "./logo.png";
import loginLeft from "./login-left.png";
import authBackground from "./auth-background.jpg";
import bannerPrincipal from "./banner-principal.jpg";
import bannerGeneral from "./banner-general.jpg";
import usImage from "./imagenes-nosotros.jpg";
import puntos from "./Puntos.png";
import misionImage from "./mision.png";
import visionImage from "./vision.jpg";
import whitePoint from "./white-point.png";
import yellowPoint from "./yellow-point.png";

import fondoCompromiso from "./fondo-compromiso.jpg";
import fondoEquipo from "./fondo-equipo.jpg";
import fondoEspiritualidad from "./fondo-espiritualidad.jpg";
import fondoEvolucion from "./fondo-evolucion.jpg";
import fondoFamiliaridad from "./fondo-familiaridad.jpg";
import fondoOportunidad from "./fondo-oportunidad.jpg";
import fondoValores from "./fondo-valores.jpg";
import anuncio from "./anuncio.jpg";
import icoDescargas from "./canguro-descargas.png";

import marcaToxik from "./Marca-Toxik.jpg";
import marcaBakzer from "./Marcas-Bakzer.jpg";
import marcaKouders from "./Marcas-Kouders.jpg";
import marcaValtik from "./Marcas-Valtik.jpg";

import mapchart from "./mapchart.jpg";


export {
  logo,
  loginLeft,
  authBackground,
  bannerPrincipal,
  bannerGeneral,
  usImage,
  puntos,
  misionImage,
  visionImage,
  fondoCompromiso,
  fondoEquipo,
  fondoEspiritualidad,
  fondoEvolucion,
  fondoFamiliaridad,
  fondoOportunidad,
  fondoValores,
  marcaToxik,
  marcaBakzer,
  marcaKouders,
  marcaValtik,
  mapchart,
  whitePoint,
  yellowPoint,
  anuncio,
  icoDescargas,
};
