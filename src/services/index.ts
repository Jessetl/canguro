import AuthService from "./modules/authService";
import StateService from "./modules/stateService";
import FormService from "./modules/formService";
import RangeService from "./modules/rangeService";
import BoardService from "./modules/boardService";
import TestService from "./modules/testSetvice";
import CourseService from "./modules/courseService";
import BrandService from "./modules/brandService";
import VenueService from "./modules/venueService";
import InscriptionService from "./modules/inscriptionService";
import ProductsService from "./modules/productsService";
import BlogService from "./modules/blogService";
import BannerService from "./modules/bannerService";
import TrabajaConNosotros from "./modules/trabajaConNosotrosService";
import IdentityService from "./modules/identityService";
import ContactService from "./modules/contactService";
import InstagramService from "./modules/instagramService";

export {
  VenueService,
  BrandService,
  AuthService,
  StateService,
  FormService,
  RangeService,
  BoardService,
  TestService,
  CourseService,
  InscriptionService,
  ProductsService,
  BlogService,
  BannerService,
  TrabajaConNosotros,
  IdentityService,
  ContactService,
  InstagramService,
};
