import apiService from "./apiService";
import { AxiosError, AxiosResponse, CancelToken } from "axios";
import { Product, ProductForm, Products } from "models";

type Source = {
  token: CancelToken;
};

const headersType = {
  "Content-Type": "multipart/form-data",
};

class productService {
  static get = (source: Source) => {
    return new Promise<Product[]>((resolve, reject) => {
      apiService
        .get("products", {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<Product[]>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static getAll = (page: number, source: Source) => {
    return new Promise<Products>((resolve, reject) => {
      apiService
        .get(`products/paginated?page=${page}`, {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<Products>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static getById = (id: number) => {
    return new Promise<Product>((resolve, reject) => {
      apiService.get(`products/${id}/edit`).then(
        (response: AxiosResponse<Product>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };

  static store = (form: ProductForm) => {
    return new Promise<Product>((resolve, reject) => {
      apiService
        .post("products", JSON.stringify(form), {
          headers: { ...headersType },
        })
        .then(
          (response: AxiosResponse<Product>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static update = (form: ProductForm) => {
    return new Promise<Product>((resolve, reject) => {
      apiService
        .put(`products/${form.id}`, JSON.stringify(form), {
          headers: { ...headersType },
        })
        .then(
          (response: AxiosResponse<Product>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static delete = (id: number) => {
    return new Promise<Product>((resolve, reject) => {
      apiService.delete(`products/${id}`).then(
        (response: AxiosResponse<Product>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };

  static status = (id: number) => {
    return new Promise<Product>((resolve, reject) => {
      apiService.get(`products/${id}/status`).then(
        (response: AxiosResponse<Product>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };

  static get12products = (source: Source) => {
    return new Promise<Products>((resolve, reject) => {
      apiService
        .get("products/get12products", {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<Products>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };
}

export default productService;
