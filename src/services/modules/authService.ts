import { axios } from "utils";
import { AxiosError, AxiosResponse, CancelToken } from "axios";
import { Auth, User, TokenAuthentication } from "models";
import apiService from "./apiService";

export interface Register extends User {
  password: string;
  password_confirmation: string;
}

type Source = {
  token: CancelToken;
};

class AuthService {
  static login = (auth: Auth, source: Source) => {
    return new Promise<TokenAuthentication>((resolve, reject) => {
      apiService
        .post("login", auth, {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<TokenAuthentication>) =>
            resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static logout = (source: Source) => {
    return new Promise<null>((resolve, reject) => {
      axios
        .get("logout", {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<null>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static me = () => {
    return new Promise<User>((resolve, reject) => {
      axios.post("me").then(
        (response: AxiosResponse<User>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };

  static resetPassword = (email: { email: string }) => {
    return new Promise<string>((resolve, reject) => {
      axios.post("reset/password", email).then(
        (response: AxiosResponse<string>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };

  static restorePassword = (form: {
    password: string;
    password_confirmation: string;
    token: string;
    id: string;
  }) => {
    return new Promise<string>((resolve, reject) => {
      axios.post("restore/password", form).then(
        (response: AxiosResponse<string>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };

  static verifyAccount = (form: { id: string; token: string }) => {
    return new Promise<string>((resolve, reject) => {
      axios.post(`verify`, form).then(
        (response: AxiosResponse<string>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };
}

export default AuthService;
