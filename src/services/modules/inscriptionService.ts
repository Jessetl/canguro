import apiService from "./apiService";
import { AxiosError, AxiosResponse, CancelToken } from "axios";
import { axios } from "utils";
import { Inscription, Inscriptions, Response } from "models";

type Source = {
  token: CancelToken;
};

class inscriptionService {
  static inscriptions = (source: Source) => {
    return new Promise<Inscription[]>((resolve, reject) => {
      apiService
        .get("inscriptions", {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<Inscription[]>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static getAll = (page: number, source: Source) => {
    return new Promise<Inscriptions>((resolve, reject) => {
      apiService
        .get(`inscriptions/paginated?page=${page}`, {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<Inscriptions>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static approve = (inscriptionId: number, source: Source) => {
    return new Promise<Inscription>((resolve, reject) => {
      axios
        .get(`inscriptions/${inscriptionId}/approve`, {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<Inscription>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static reject = (inscriptionId: number, source: Source) => {
    return new Promise<Inscription>((resolve, reject) => {
      axios
        .get(`inscriptions/${inscriptionId}/reject`, {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<Inscription>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static store = (data: any, source: Source) => {
    return new Promise<Response>((resolve, reject) => {
      axios
        .post("inscriptions", data, {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<Response>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };
}

export default inscriptionService;
