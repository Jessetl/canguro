import apiService from "./apiService";
import { AxiosError, AxiosResponse } from "axios";

class InstagramService {
  static getAll = () => {
    return new Promise<any>((resolve, reject) => {
      apiService
        .get(
          "https://graph.instagram.com/?fields=id,media_type,media_url,username,timestamp&access_token=23f9718b8e67afc35a914dd27c02409f"
        )
        .then(
          (response: AxiosResponse<any>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };
}

export default InstagramService;
