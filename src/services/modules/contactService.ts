import apiService from "./apiService";
import { AxiosError, AxiosResponse, CancelToken } from "axios";
import { ContactModel } from "models";

type Source = {
  token: CancelToken;
};

class ContactService {
  static getAll = () => {
    return new Promise<ContactModel[]>((resolve, reject) => {
      apiService.get("contact").then(
        (response: AxiosResponse<ContactModel[]>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };

  static getInformation = (source: Source) => {
    return new Promise<ContactModel>((resolve, reject) => {
      apiService
        .get("contact/information", {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<ContactModel>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static store = (contactModel: ContactModel, source: Source) => {
    return new Promise<ContactModel>((resolve, reject) => {
      apiService
        .post("contact", {
          ...contactModel,
          deliveries_phones: JSON.stringify(contactModel.deliveries_phones),
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<ContactModel>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static update = (id: number, contact: ContactModel) => {
    return new Promise<ContactModel>((resolve, reject) => {
      apiService
        .put(`contact/${id}`, {
          ...contact,
          deliveries_phones: JSON.stringify(contact.deliveries_phones),
        })
        .then(
          (response: AxiosResponse<ContactModel>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };
}

export default ContactService;
