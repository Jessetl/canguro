import apiService from "./apiService";
import { AxiosError, AxiosResponse, CancelToken } from "axios";
import { BannerForm, BannerModel, Banners, Response } from "models";

type Source = {
  token: CancelToken;
};

const headersType = {
  "Content-Type": "multipart/form-data",
};

class bannerService {
  static banners = (source: Source) => {
    return new Promise<BannerModel[]>((resolve, reject) => {
      apiService
        .get("banners", {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<BannerModel[]>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static getAll = (page: number, source: Source) => {
    return new Promise<Banners>((resolve, reject) => {
      apiService
        .get(`banners/paginated?page=${page}`, {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<Banners>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static getById = (id: number) => {
    return new Promise<BannerModel>((resolve, reject) => {
      apiService.get(`banners/${id}/edit`).then(
        (response: AxiosResponse<BannerModel>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };

  static store = (form: BannerForm) => {
    return new Promise<BannerModel>((resolve, reject) => {
      apiService
        .post("banners", JSON.stringify(form), {
          headers: { ...headersType },
        })
        .then(
          (response: AxiosResponse<BannerModel>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static update = (form: BannerForm) => {
    return new Promise<Response>((resolve, reject) => {
      apiService
        .put(`banners/${form.id}`, JSON.stringify(form), {
          headers: { ...headersType },
        })
        .then(
          (response: AxiosResponse<Response>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static delete = (id: number) => {
    return new Promise<BannerModel>((resolve, reject) => {
      apiService.delete(`banners/${id}`).then(
        (response: AxiosResponse<BannerModel>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };

  static enabledisable = (producto: BannerModel, source: Source) => {
    return new Promise<Response>((resolve, reject) => {
      apiService
        .put("banners/enabledisable/" + producto.id, producto, {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<Response>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static getBannerPrincipal = (source: Source) => {
    return new Promise<BannerModel[]>((resolve, reject) => {
      apiService
        .get("banners/getBannerPrincipal", {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<BannerModel[]>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static getBannerGeneral = (source: Source) => {
    return new Promise<BannerModel[]>((resolve, reject) => {
      apiService
        .get("banners/getBannerGeneral", {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<BannerModel[]>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };
}

export default bannerService;
