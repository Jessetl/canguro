import apiService from "./apiService";
import { AxiosError, AxiosResponse, CancelToken } from "axios";
import { VenueForm, VenueModel, Venues } from "models";

type Source = {
  token: CancelToken;
};

const headersType = {
  "Content-Type": "multipart/form-data",
};

class VenueService {
  static getCanguroMundo = (source: Source) => {
    return new Promise<VenueModel[]>((resolve, reject) => {
      apiService
        .get("canguromundo", {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<VenueModel[]>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static get = (source: Source) => {
    return new Promise<Venues>((resolve, reject) => {
      apiService
        .get("venues", {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<Venues>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static getAll = (page: number, source: Source) => {
    return new Promise<Venues>((resolve, reject) => {
      apiService
        .get(`venues/paginated?page=${page}`, {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<Venues>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static getById = (id: number, source: Source) => {
    return new Promise<VenueModel>((resolve, reject) => {
      apiService.get(`venues/${id}/edit`).then(
        (response: AxiosResponse<VenueModel>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };

  static store = (form: VenueForm) => {
    return new Promise<VenueModel>((resolve, reject) => {
      apiService
        .post("venues", JSON.stringify(form), {
          headers: { ...headersType },
        })
        .then(
          (response: AxiosResponse<VenueModel>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static update = (form: VenueForm) => {
    return new Promise((resolve, reject) => {
      apiService
        .put(`venues/${form.id}`, JSON.stringify(form), {
          headers: { ...headersType },
        })
        .then(
          (response: AxiosResponse) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static delete = (id: number) => {
    return new Promise((resolve, reject) => {
      apiService.delete(`venues/${id}`).then(
        (response: AxiosResponse) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };

  static countries = () => {
    return new Promise((resolve, reject) => {
      apiService.post("venues/countries").then(
        (response: AxiosResponse) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };
}

export default VenueService;
