import apiService from "./apiService";
import { AxiosError, AxiosResponse, CancelToken } from "axios";
import { WarrantyModel } from "models";

type Source = {
  token: CancelToken;
};

class WarrantyService {
  static index = () => {
    return new Promise<WarrantyModel[]>((resolve, reject) => {
      apiService.get("warranty", {}).then(
        (response: AxiosResponse<WarrantyModel[]>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };

  static store = (warrantyModel: WarrantyModel, source: Source) => {
    return new Promise<WarrantyModel>((resolve, reject) => {
      apiService
        .post("warranty", warrantyModel, {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<WarrantyModel>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static update = (id: number, wr: WarrantyModel) => {
    return new Promise<WarrantyModel>((resolve, reject) => {
      apiService.put(`warranty/${id}`, wr).then(
        (response: AxiosResponse<WarrantyModel>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };
}

export default WarrantyService;
