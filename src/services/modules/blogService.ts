import apiService from "./apiService";
import { AxiosError, AxiosResponse, CancelToken } from "axios";
import { BlogModel, Blogs, BlogForm } from "models";

type Source = {
  token: CancelToken;
};

const headersType = {
  "Content-Type": "multipart/form-data",
};

class BlogService {
  static getAll = (page: number, source: Source) => {
    return new Promise<Blogs>((resolve, reject) => {
      apiService
        .get(`blogs/paginated?page=${page}`, {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<Blogs>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static getById = (id: number) => {
    return new Promise<BlogModel>((resolve, reject) => {
      apiService.get(`blogs/${id}/edit`).then(
        (response: AxiosResponse<BlogModel>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };

  static store = (form: BlogForm) => {
    return new Promise<BlogModel>((resolve, reject) => {
      apiService
        .post("blogs", JSON.stringify(form), {
          headers: { ...headersType },
        })
        .then(
          (response: AxiosResponse<BlogModel>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static update = (form: BlogForm) => {
    return new Promise<BlogModel>((resolve, reject) => {
      apiService
        .put(`blogs/${form.id}`, JSON.stringify(form), {
          headers: { ...headersType },
        })
        .then(
          (response: AxiosResponse<BlogModel>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static getNews = (page: number) => {
    return new Promise((resolve, reject) => {
      apiService.get("news?page=" + page).then(
        (response: AxiosResponse) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };

  static getPost = (page: number) => {
    return new Promise((resolve, reject) => {
      apiService.get("post?page=" + page).then(
        (response: AxiosResponse) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };

  static getLast3 = (source: Source) => {
    return new Promise<Blogs>((resolve, reject) => {
      apiService
        .get(`post/getlast3`, {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<Blogs>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static delete = (id: number) => {
    return new Promise<BlogModel>((resolve, reject) => {
      apiService.delete(`blogs/${id}`).then(
        (response: AxiosResponse<BlogModel>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };

  static status = (id: number) => {
    return new Promise<BlogModel>((resolve, reject) => {
      apiService.get(`blogs/${id}/status`).then(
        (response: AxiosResponse<BlogModel>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };
}

export default BlogService;
