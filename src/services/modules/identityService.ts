import apiService from "./apiService";
import { AxiosError, AxiosResponse, CancelToken } from "axios";
import { Identities, IdentityForm, IdentityModel } from "models";

type Source = {
  token: CancelToken;
};

const headersType = {
  "Content-Type": "multipart/form-data",
};

class IdentityService {
  static get = (source: Source) => {
    return new Promise<IdentityModel[]>((resolve, reject) => {
      apiService
        .get("identity", {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<IdentityModel[]>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static getAll = (page: number, source: Source) => {
    return new Promise<Identities>((resolve, reject) => {
      apiService
        .get(`identity/paginated?page=${page}`, {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<Identities>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static getById = (id: number) => {
    return new Promise<IdentityModel>((resolve, reject) => {
      apiService.get(`identity/${id}/edit`).then(
        (response: AxiosResponse<IdentityModel>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };

  static store = (form: IdentityForm) => {
    return new Promise<IdentityModel>((resolve, reject) => {
      apiService
        .post("identity", JSON.stringify(form), {
          headers: { ...headersType },
        })
        .then(
          (response: AxiosResponse<IdentityModel>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static update = (form: IdentityForm) => {
    return new Promise<IdentityModel>((resolve, reject) => {
      apiService
        .put(`identity/${form.id}`, JSON.stringify(form), {
          headers: { ...headersType },
        })
        .then(
          (response: AxiosResponse<IdentityModel>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static delete = (id: number) => {
    return new Promise<IdentityModel>((resolve, reject) => {
      apiService.delete(`identity/${id}`).then(
        (response: AxiosResponse<IdentityModel>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };
}

export default IdentityService;
