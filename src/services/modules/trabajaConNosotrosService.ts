import apiService from "./apiService";
import { AxiosError, AxiosResponse, CancelToken } from "axios";
import { TrabajaConNosotrosModel } from "models";

type Source = {
  token: CancelToken;
};

class TrabajaConNosotrosService {
  static sendEmail = (
    TrabajaConNosotrosModel: TrabajaConNosotrosModel,
    source: Source
  ) => {
    return new Promise<TrabajaConNosotrosModel>((resolve, reject) => {
      apiService
        .post("sendemail", TrabajaConNosotrosModel, {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<TrabajaConNosotrosModel>) =>
            resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };
}

export default TrabajaConNosotrosService;
