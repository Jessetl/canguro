import apiService from "./apiService";
import { AxiosError, AxiosResponse, CancelToken } from "axios";
import { CourseForm, CourseModel, Courses } from "models";

type Source = {
  token: CancelToken;
};

class CourseService {
  static get = (source: Source) => {
    return new Promise<CourseModel[]>((resolve, reject) => {
      apiService
        .get("course", {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<CourseModel[]>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static getAll = (page: number, source: Source) => {
    return new Promise<Courses>((resolve, reject) => {
      apiService
        .get(`course/paginated?page=${page}`, {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<Courses>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static getById = (id: number) => {
    return new Promise<CourseModel>((resolve, reject) => {
      apiService.get(`course/${id}/edit`).then(
        (response: AxiosResponse<CourseModel>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };

  static store = (course: CourseForm, source: Source) => {
    return new Promise<CourseModel>((resolve, reject) => {
      apiService
        .post("course", course, {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<CourseModel>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static update = (form: CourseForm, source: Source) => {
    return new Promise<CourseModel>((resolve, reject) => {
      apiService
        .put(`course/${form.id}`, form, {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<CourseModel>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static delete = (id: number) => {
    return new Promise<CourseModel>((resolve, reject) => {
      apiService.delete(`course/${id}`).then(
        (response: AxiosResponse<CourseModel>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };

  static status = (id: number) => {
    return new Promise<CourseModel>((resolve, reject) => {
      apiService.get(`course/${id}/status`).then(
        (response: AxiosResponse<CourseModel>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };
}

export default CourseService;
