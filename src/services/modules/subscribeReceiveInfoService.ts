import apiService from "./apiService";
import { AxiosError, AxiosResponse, CancelToken } from "axios";
import { SubscribeReceiveInfoModel } from "models";

type Source = {
  token: CancelToken;
};

class SubscribeReceiveInfoService {
  static sendEmail = (
    SubscribeReceiveInfoModel: SubscribeReceiveInfoModel,
    source: Source
  ) => {
    return new Promise<SubscribeReceiveInfoModel>((resolve, reject) => {
      apiService
        .post("informationemail/send", SubscribeReceiveInfoModel, {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<SubscribeReceiveInfoModel>) =>
            resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };
}

export default SubscribeReceiveInfoService;