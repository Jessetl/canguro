import apiService from "./apiService";
import { AxiosError, AxiosResponse, CancelToken } from "axios";
import { Brand, BrandForm, Brands } from "models";

type Source = {
  token: CancelToken;
};

const headersType = {
  "Content-Type": "multipart/form-data",
};

class BrandService {
  static getLogo = () => {
    return new Promise<Brand[]>((resolve, reject) => {
      apiService.get("brand/getAll", {}).then(
        (response: AxiosResponse<Brand[]>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };

  static index = (source: Source) => {
    return new Promise<Brands>((resolve, reject) => {
      apiService
        .get("brand", {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<Brands>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static getAll = (page: number, source: Source) => {
    return new Promise<Brands>((resolve, reject) => {
      apiService
        .get(`brand/paginated?page=${page}`, {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<Brands>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static getById = (id: number) => {
    return new Promise<Brand>((resolve, reject) => {
      apiService.get(`brand/${id}/edit`).then(
        (response: AxiosResponse<Brand>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };

  static store = (form: BrandForm) => {
    return new Promise((resolve, reject) => {
      apiService
        .post("brand", JSON.stringify(form), {
          headers: { ...headersType },
        })
        .then(
          (response: AxiosResponse) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static update = (form: BrandForm) => {
    return new Promise((resolve, reject) => {
      apiService
        .put(`brand/${form.id}`, JSON.stringify(form), {
          headers: { ...headersType },
        })
        .then(
          (response: AxiosResponse) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static get = (page: number) => {
    return new Promise((resolve, reject) => {
      apiService.get("brand?page=" + page).then(
        (response: AxiosResponse) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };

  static delete = (id: number) => {
    return new Promise<Brand>((resolve, reject) => {
      apiService.delete(`brand/${id}`).then(
        (response: AxiosResponse<Brand>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };

  static status = (id: number) => {
    return new Promise<Brand>((resolve, reject) => {
      apiService.get(`brand/${id}/status`).then(
        (response: AxiosResponse<Brand>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };
}

export default BrandService;
