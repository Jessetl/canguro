import apiService from "./apiService";
import { AxiosError, AxiosResponse, CancelToken } from "axios";
import { CountryAndState, StateModel } from "models";

type Source = {
  token: CancelToken;
};

class StateService {
  static getStateAndCity = (source: Source) => {
    return new Promise<StateModel[]>((resolve, reject) => {
      apiService
        .get("state/city", {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<StateModel[]>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static getStates = (source: Source) => {
    return new Promise<StateModel[]>((resolve, reject) => {
      apiService
        .get("states", {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<StateModel[]>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static getCountryAndState = (source: Source) => {
    return new Promise<CountryAndState>((resolve, reject) => {
      apiService
        .get("country/state", {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<CountryAndState>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };
}

export default StateService;
