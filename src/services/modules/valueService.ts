import apiService from "./apiService";
import { AxiosError, AxiosResponse, CancelToken } from "axios";
import { Values } from "models";

type Source = {
  token: CancelToken;
};

class ValueService {
  static getAll = (source: Source) => {
    return new Promise<Values>((resolve, reject) => {
      apiService.get("values", {}).then(
        (response: AxiosResponse<Values>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };
}

export default ValueService;