import React, { ReactNode } from "react";
import { Switch, Redirect, Link } from "react-router-dom";
import { RouteProps, RouteComponentProps } from "react-router-dom";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Provider, connect } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";

import { MONTHS } from "utils";

// Screens
import Login from "screens/login/Login";
import Home from "screens/home/home";
import Blog from "screens/blog/Blog";
import Contacto from "screens/contacto/Contacto";

import RestorePassword from "screens/restore/RestorePassword";
import VerifyAccount from "screens/verify/VerifyAccount";

// Screens Tests
import TestList from "screens/tests/TestList";

// Layout
import { PrivateRoute, Icon, Post } from "components";
import LayoutGuest from "screens/layout/LayoutGuest";

// Styles
import "bootstrap/dist/css/bootstrap.min.css";
import "font-awesome/css/font-awesome.min.css";
import "./assets/app.scss";

// Redux
import { store, persistor } from "./store";
import { RootState } from "reducers";
import { UserState } from "actions";

import moment from "moment";
import "moment/locale/es";

import UsCreate from "screens/us/UsCreate";
import Us from "screens/us/Us";
import ContactCreate from "screens/contact/ContactCreate";
import Web from "screens/web/web";
// Brands
import Brands from "screens/brands/BrandList";
import BrandCreate from "screens/brands/BrandCreate";
import BrandEdit from "screens/brands/BrandEdit";
// Banner
import BannerList from "screens/banners/BannerList";
import BannerCreate from "screens/banners/BannerCreate";
import BannerEdit from "screens/banners/BannerEdit";
// Venues
import VenuesList from "screens/venues/VenuesList";
import VenuesCreate from "screens/venues/VenuesCreate";
import VenuesEdit from "screens/venues/VenuesEdit";
// Courses
import Courses from "screens/courses/Courses";
import CourseList from "screens/course/CourseList";
import CourseCreate from "screens/course/CourseCreate";
import CourseEdit from "screens/course/CourseEdit";
// Blog
import BlogList from "screens/blog/BlogList";
import BlogCreate from "screens/blog/BlogCreate";
import BlogEdit from "screens/blog/BlogEdit";
// Products
import ProductList from "screens/products/ProductList";
import ProductCreate from "screens/products/ProductCreate";
import ProductEdit from "screens/products/ProductEdit";
// Identities
import IdentitiesList from "screens/identities/IdentitiesList";
import IdentitiesCreate from "screens/identities/IdentitiesCreate";
import IdentitiesEdit from "screens/identities/IdentitiesEdit";
import Inscriptions from "screens/inscriptions/Inscriptions";
import CoursesForm from "screens/course/CousersForm";
import WarrantyCreate from "screens/warranty/warrantyCreate";

const months = MONTHS;
var meses = months.split("_");
moment.updateLocale("es", { months: meses });

const App: React.FC<{}> = () => {
  const { REACT_APP_PATH: basename } = process.env;
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Router basename={basename}>
          <Switch>
            <Route exact path="/verify/:token/:id" component={VerifyAccount} />

            <Route
              exact
              path="/reset-password/:token/:id"
              component={RestorePassword}
            />

            <GuestRoute exact path="/login" component={Login} />

            <GuestRoute exact path="/home" component={Web} />

            <GuestRoute exact path="/home/:id" component={Web} />

            <GuestRoute exact path="/us" component={Us} />

            <GuestRoute exact path="/blog" component={Blog} />

            <GuestRoute exact path="/contacto" component={Contacto} />

            <GuestRoute exact path="/blog/:id" component={Post} />

            <GuestRoute exact path="/courses" component={Courses} />

            <PrivateRoute exact path="/admin" component={Home} />

            <PrivateRoute exact path="/admin/us" component={UsCreate} />

            <PrivateRoute
              exact
              path="/admin/contact"
              component={ContactCreate}
            />

            <PrivateRoute exact path="/admin/brands" component={Brands} />

            <PrivateRoute
              exact
              path="/admin/brands/create"
              component={BrandCreate}
            />

            <PrivateRoute
              exact
              path="/admin/brands/:id/edit"
              component={BrandEdit}
            />

            <PrivateRoute exact path="/admin/blogs" component={BlogList} />

            <PrivateRoute
              exact
              path="/admin/blogs/create"
              component={BlogCreate}
            />

            <PrivateRoute
              exact
              path="/admin/blogs/:id/edit"
              component={BlogEdit}
            />

            <PrivateRoute exact path="/admin/venues" component={VenuesList} />

            <PrivateRoute
              exact
              path="/admin/venues/create"
              component={VenuesCreate}
            />

            <PrivateRoute
              exact
              path="/admin/venues/:id/edit"
              component={VenuesEdit}
            />

            <PrivateRoute exact path="/admin/course" component={CourseList} />

            <PrivateRoute
              exact
              path="/admin/course/create"
              component={CourseCreate}
            />

            <PrivateRoute
              exact
              path="/admin/course/:id/edit"
              component={CourseEdit}
            />

            <PrivateRoute
              exact
              path="/admin/course/form"
              component={CoursesForm}
            />

            <PrivateRoute
              exact
              path="/admin/inscriptions"
              component={Inscriptions}
            />

            <PrivateRoute
              exact
              path="/admin/products"
              component={ProductList}
            />

            <PrivateRoute
              exact
              path="/admin/products/create"
              component={ProductCreate}
            />

            <PrivateRoute
              exact
              path="/admin/products/:id/edit"
              component={ProductEdit}
            />

            <PrivateRoute exact path="/admin/banners" component={BannerList} />

            <PrivateRoute
              exact
              path="/admin/banners/create"
              component={BannerCreate}
            />

            <PrivateRoute
              exact
              path="/admin/banners/:id/edit"
              component={BannerEdit}
            />

            <PrivateRoute
              exact
              path="/admin/warranty"
              component={WarrantyCreate}
            />

            <PrivateRoute
              exact
              path="/admin/identities"
              component={IdentitiesList}
            />

            <PrivateRoute
              exact
              path="/admin/identities/create"
              component={IdentitiesCreate}
            />

            <PrivateRoute
              exact
              path="/admin/identities/:id/edit"
              component={IdentitiesEdit}
            />

            <PrivateRoute exact path="/admin/tests/:id" component={TestList} />

            <Redirect path="/" to="/home" />
            <PrivateRoute component={PageNotFound} />
          </Switch>
        </Router>
      </PersistGate>
    </Provider>
  );
};

interface GuestRouteProps extends RouteProps {
  component:
    | React.ComponentType<RouteComponentProps<any>>
    | React.ComponentType<any>;
}

type RenderComponent = (props: RouteComponentProps<any>) => React.ReactNode;

const mapState = (state: RootState) => ({
  user: state.user,
});

class GuestRouteComponent extends Route<
  GuestRouteProps & {
    user: UserState;
  }
> {
  render(): ReactNode {
    const { component: Component, ...rest }: GuestRouteProps = this.props;

    const renderComponent: RenderComponent = (props) => (
      <LayoutGuest>
        <Component {...props} />
      </LayoutGuest>
    );

    return <Route {...rest} render={renderComponent} />;
  }
}

export const GuestRoute = connect(mapState)(GuestRouteComponent);

const PageNotFound = () => (
  <div className="container">
    <div className="row">
      <div className="col bg-white">
        <div className="h-100 p-5 text-center">
          <h1 className="text-center">Página no encontrada</h1>
          <p className="text-center">No hemos encontrado lo que buscabas.</p>
          <Icon name="exclamation-circle" style={{ fontSize: "5rem" }} />
          <p>
            <br />
            <br />
            <Link to="/">Volver al inicio</Link>
          </p>
        </div>
      </div>
    </div>
  </div>
);

export default App;
