import ROLES from "./RoleId";

export const SidebarMenu = [
  {
    display_name: "Nosotros",
    path: "/admin/us",
    roles: [ROLES.ROOT, ROLES.DOCTOR, ROLES.COACH],
    submodules: [],
  },
  {
    display_name: "Banners",
    path: "/admin/banners",
    roles: [ROLES.ROOT],
    submodules: [],
  },
  {
    display_name: "Contacto",
    path: "/admin/contact",
    roles: [ROLES.ROOT],
    submodules: [],
  },
  {
    display_name: "Nuestras Marcas",
    path: "/admin/brands",
    roles: [ROLES.ROOT],
    submodules: [],
  },
  {
    display_name: "Sedes",
    path: "/admin/venues",
    roles: [ROLES.ROOT],
    submodules: [],
  },

  {
    display_name: "Cursos",
    path: "/admin/course",
    roles: [ROLES.ROOT],
    submodules: [],
  },
  {
    display_name: "Blogs",
    path: "/admin/blogs",
    roles: [ROLES.ROOT],
    submodules: [],
  },
  // {
  //   display_name: "Perfil",
  //   path: "/admin/profile",
  //   roles: [ROLES.ROOT, ROLES.DOCTOR, ROLES.COACH],
  //   submodules: [],
  // },
  {
    display_name: "Home",
    path: "/home",
    roles: [ROLES.USER],
    submodules: [],
  },
  {
    display_name: "Us",
    path: "/us",
    roles: [ROLES.USER],
    submodules: [],
  },
  {
    display_name: "Inscripciones",
    path: "/admin/inscriptions",
    roles: [ROLES.ROOT],
    submodules: [],
  },
  {
    display_name: "Productos",
    path: "/admin/products",
    roles: [ROLES.ROOT],
    submodules: [],
  },
  {
    display_name: "Garantías",
    path: "/admin/warranty",
    roles: [ROLES.ROOT],
    submodules: [],
  },
  {
    display_name: "Identidades",
    path: "/admin/identities",
    roles: [ROLES.ROOT],
    submodules: [],
  },
];
