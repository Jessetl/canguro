export const STATUS_ACTIVE = 1;
export const STATUS_INACTIVE = 0;

export const STATUS_PENDING = 0; // Pendiente
export const STATUS_ON_HOLD = 1; // En Espera
export const STATUS_IN_PROGRESS = 2; // En Progreso
export const STATUS_FINISHED = 3; // Finalizada

export const STEP_FIRST = 1;
export const STEP_SECOND = 2;

export const TIMELINE_UPDATE = 0;
export const TIMELINE_DATE = 1;
export const TIMELINE_TIME = 2;
export const TIMELINE = 3;

export const SERVICELINE_INIT = 0;
export const SERVICELINE_AREA = 1;
export const SERVICELINE_SERVICE = 2;

export const DOCTOR_COACH = 1;

export const SELECT_LOCAL = 0;
export const SELECT_MUNDO = 1;

export const NATIONALITIES = [
  { value: "V", label: "V" },
  { value: "E", label: "E" },
  { value: "J", label: "J" },
];

export const CELLPHONE_CODE = [
  { value: "0412", label: "0412" },
  { value: "0416", label: "0416" },
  { value: "0426", label: "0426" },
  { value: "0414", label: "0414" },
  { value: "0424", label: "0424" },
];

export const GENDER = [
  { value: 1, label: "Femenino" },
  { value: 2, label: "Masculino" },
];

export const MARITAL_STATUS = [
  { value: 1, label: "Soltero/a" },
  { value: 2, label: "Comprometido/a" },
  { value: 3, label: "Casado/a" },
  { value: 4, label: "Divorciado/a" },
  { value: 5, label: "Viudo/a" },
  { value: 6, label: "Relación" },
];

export const BOOLEAN_STATUS = [
  { value: 1, label: "Si" },
  { value: 0, label: "No" },
];

export const STATUS_USER = [
  { value: 1, label: "Activo" },
  { value: 0, label: "Inactivo" },
];

export const BANNERS = [
  { value: 0, label: "Principal" },
  { value: 1, label: "General" },
];

export const TYPE_POST = [
  { name: "Noticia", value: "Noticia" },
  { name: "Post", value: "Post" },
];

export const TYPE_VENUES = [
  { value: 1, label: "Mundo" },
  { value: 0, label: "Local" },
];

export const PATH = ["/admin/stories/create"];
export const PATH_QUEUE = "/admin/queue/:id";

export const FIL_EXTENSION_PDF = ".pdf";
export const FIL_EXTENSION_EXCEL = ".xls";

export const FILE_TYPES = {
  excel: {
    type: "application/vnd.ms-excel",
    extension: FIL_EXTENSION_EXCEL,
  },
  pdf: {
    type: "application/pdf",
    extension: FIL_EXTENSION_PDF,
  },
};

export const FORM_BIOPHYSICS = 1;
export const FORM_BIOCHEMISTRY = 2;
export const FORM_ORTHOMOLECULAR = 3;
export const FORM_GENETICS = 4;

export const GENDER_FEMALE = 1;
export const GENDER_SPORTY_FEMALE = 2;
export const GENDER_MALE = 3;
export const GENDER_SPORTY_MALE = 4;

export const GENDERS = [
  {
    label: "Masculino",
    value: 3,
  },
  {
    label: "Masculino Deportivo",
    value: 4,
  },
  {
    label: "Femenino",
    value: 1,
  },
  {
    label: "Femenino Deportivo",
    value: 2,
  },
];

export const FORMS_TYPES = [
  {
    type: FORM_BIOPHYSICS,
    quantity: 0,
    remaining: 0,
  },
  {
    type: FORM_BIOCHEMISTRY,
    quantity: 0,
    remaining: 0,
  },
  {
    type: FORM_ORTHOMOLECULAR,
    quantity: 0,
    remaining: 0,
  },
  {
    type: FORM_GENETICS,
    quantity: 0,
    remaining: 0,
  },
];

export const FORM_METABOLIZER = [
  {
    value: 1,
    label: "Lentos",
  },
  {
    value: 2,
    label: "Medios",
  },
  {
    value: 3,
    label: "Rápidos",
  },
  {
    value: 4,
    label: "Ultra",
  },
];

export const MONTHS =
  "Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre";
