import HoldOn from "react-hold-on";
import Swal from "sweetalert2";
import moment from "moment";
import $ from "jquery";
import { Board } from "models";

class Globals {
  setLoading = () => {
    HoldOn.open({
      theme: "sk-bounce",
    });
  };

  quitLoading = () => {
    HoldOn.close();
  };

  hideTooltip = () => {
    $("button").blur();
  };

  showSuccess = (message: string) => {
    Swal.fire({
      title: "",
      text: message,
      type: "success",
      showCancelButton: false,
      confirmButtonColor: "#0FC6FB",
    });
  };

  showError = (message?: string) => {
    Swal.fire({
      title: "",
      text: message ? message : "Se ha producido un error",
      type: "error",
      showCancelButton: false,
      confirmButtonColor: "#f2692a",
    });
  };

  confirm = (message: string, callback: any) => {
    this.hideTooltip();
    Swal.fire({
      title: "",
      text: message,
      type: "warning",
      showCancelButton: true,
      reverseButtons: true,
      confirmButtonColor: "#0FC6FB",
      cancelButtonText: "Cancelar",
      confirmButtonText: "Aceptar",
      focusConfirm: false,
      focusCancel: false,
    }).then((confirm) => {
      if (confirm.value) {
        callback();
      }
    });
  };

  getDate = (
    date: string,
    to: string = "DD/MM/YYYY",
    from: string = "YYYY-MM-DD HH:mm:ss"
  ) => {
    return moment(date, from).format(to);
  };

  getRange = (start: number, end: number) => {
    return new Array(end - start + 1).fill(undefined).map((_, i) => i + start);
  };

  getReverse = (r: Board, value: number, reverse: boolean) => {
    return reverse
      ? value >= parseFloat(r.max) || parseFloat(r.min) <= value
      : parseFloat(r.min) >= value || value <= parseFloat(r.max);
  };

  logout = (message: string, callback: any) => {
    Swal.fire({
      title: message,
      showCancelButton: true,
      reverseButtons: true,
      confirmButtonColor: "#0FC6FB",
      cancelButtonText: "Cancelar",
      confirmButtonText: "Salir",
      focusConfirm: false,
      focusCancel: false,
    }).then((confirm) => {
      if (confirm.value) {
        callback();
      }
    });
  };
}

export default new Globals();
