import Swal from "sweetalert2";

export const INTERNAL_SERVER_ERROR = 500;
export const NOT_FOUND_ERROR = 404;
export const UNAUTHORIZED = 401;
export const UNPROCESSABLE_ENTITY = 422;
export const FORBIDDEN = 403;

export const handlerError = (error: any) => {
  const { message, response } = error;
  console.log(message, response, error, "ERROR");

  switch (response?.status) {
    case NOT_FOUND_ERROR:
      showError("No hemos encontrado lo que buscaba.");
      return;
    case INTERNAL_SERVER_ERROR:
      showError("Ha ocurrido un error interno.");
      return;
    case UNAUTHORIZED:
      showError("Usuario no autorizado.");
      return;
    case UNPROCESSABLE_ENTITY:
      if (response.data[0] === "El campo correo electrónico es obligatorio.") {
        showError("El correo electrónico es obligatorio.");
      } else if (response.data[0] === "El campo password es obligatorio.") {
        showError("La contraseña es obligatoria.");
      } else if (
        response.data[0] ===
        "El campo password debe contener al menos 6 caracteres."
      ) {
        showError("La contraseña debe tener al menos 6 carácteres.");
      } else if (
        response.data[0] ===
        "El campo correo electrónico debe ser una dirección de correo válida."
      ) {
        showError(
          "El correo electrónico debe ser una dirección de correo válida."
        );
      } else {
        showError(response.data[0]);
      }

      return;
    case FORBIDDEN:
      showError("Usuario desactivado.");
      return;
  }

  if (isNewtworkError(message)) {
    showError("Revise su conexión.");
    return;
  }
};

const showError = (text: string) => {
  Swal.fire({
    title: "",
    text: text,
    type: "error",
    showCancelButton: false,
    confirmButtonColor: "#ffe44f !important",
  });
};

const isNewtworkError = (message: string): boolean =>
  message.indexOf("Network Error") > -1;
