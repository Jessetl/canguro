import Swal from "sweetalert2";

export const Notification = (text: string, tipo: any = 'success') => {
    Swal.fire({
        title: "",
        text: text,
        type: tipo,
        showCancelButton: false,
        confirmButtonColor: "#f2692a"
    });
}
