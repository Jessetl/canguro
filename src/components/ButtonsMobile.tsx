import React, { ReactNode } from "react";
import { RouteComponentProps, withRouter } from "react-router";

import { initialState, State } from "../screens/web/FormState";

import playStore from "../assets/img/PlayStore.png";
import appStore from "../assets/img/AppStore.png";

import { RootState } from "reducers";
import { connect, ConnectedProps } from "react-redux";

const mapState = (state: RootState) => ({
  info: state.contact,
});

const connector = connect(mapState);

type Props = ConnectedProps<typeof connector> & RouteComponentProps;

class ButtonsMobile extends React.PureComponent<Props, State> {
  public state: State;

  constructor(props: Props) {
    super(props);
    this.state = initialState;
  }

  componentDidMount() {
    if (this.props.info) {
      const { info } = this.props;

      this.setState((prevState: any) => {
        return {
          ...prevState,
          link_app_android: info.link_app_android,
          link_app_ios: info.link_app_ios,
        };
      });
    }
  }

  render(): ReactNode {
    const { link_app_android, link_app_ios } = this.state;

    const appStoreButton = {
      backgroundImage: "url( " + appStore + ") ",
    };

    const playStoreButton = {
      backgroundImage: "url( " + playStore + ")",
    };

    return (
      <div className="text-center m-auto pb-2 pt-2 w-100 h-100">
        <a
          className="block"
          href={link_app_android}
          target="_blank"
          rel="noopener noreferrer"
        >
          <button
            type="submit"
            style={playStoreButton}
            className="buttonMobile"
          ></button>
        </a>
        <form className="block" target="_blank" action={link_app_ios}>
          <button
            type="submit"
            style={appStoreButton}
            className="buttonMobile"
          ></button>
        </form>
      </div>
    );
  }
}

export default withRouter(connector(ButtonsMobile));
