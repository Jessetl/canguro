import React, { useState, useEffect } from "react";
import axios from "axios";

import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

import { Brand } from "models";
import { Loading } from "components";
import { BrandService } from "services";
import { handlerError } from "utils";

import arrow from "../assets/icons/arrowCarousel.svg";

const BrandsInfo = () => {
  const responsive = {
    desktop: {
      breakpoint: { max: 9000, min: 0 },
      items: 1,
      slidesToSlide: 1,
    },
  };

  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [visible, setVisible] = React.useState(false);
  const [brands, setBrands] = useState<Brand[]>([]);
  const cancelToken = axios.CancelToken;

  useEffect(() => {
    const source = cancelToken.source();

    (async () => {
      try {
        const { data } = await BrandService.index(source);

        setBrands(data);
        setIsLoading(false);
      } catch (err) {
        console.log("Error");
        setIsLoading(false);
        handlerError(err);
      }

      return () => source.cancel("Cancel by user");
    })();
  }, [cancelToken]);

  const CustomDot = ({
    index,
    onClick,
    active,
  }: {
    onMove?: any;
    index?: any;
    onClick?: any;
    active?: any;
    visible?: any;
    setVisible?: any;
  }) => {
    let imagen, imagenGold;
    if (index === 0) {
      imagen = brands[0].logo_url;
      imagenGold = brands[0].logo_gold_url;
    } else if (index === 1) {
      imagen = brands[1].logo_url;
      imagenGold = brands[1].logo_gold_url;
    } else if (index === 2) {
      imagen = brands[2].logo_url;
      imagenGold = brands[2].logo_gold_url;
    } else {
      imagen = brands[3].logo_url;
      imagenGold = brands[3].logo_gold_url;
    }

    return (
      <div className="m-0 p-0 h-50 w-50 mt-5">
        <li
          className={active ? "active" : "inactive"}
          onClick={() => onClick()}
        >
          {active ? (
            <img
              onClick={() => setVisible(!visible)}
              src={imagenGold}
              alt="logo gold"
              className="h-25 w-25 marginMarks"
            />
          ) : (
            <img src={imagen} alt="logo" className="h-25 w-25 marginMarks" />
          )}
        </li>
      </div>
    );
  };

  const CustomRight = ({ onClick }: { onClick: any }) => (
    <button className="arrow right" onClick={onClick}>
      <img src={arrow} alt="arrow right" />
    </button>
  );

  const CustomLeft = ({ onClick }: { onClick: any }) => (
    <button className="arrow left" onClick={onClick}>
      <img src={arrow} alt="arrow left" />
    </button>
  );

  const MyCarousel = ({ items }: { items: Brand[] }) => (
    <div className="carousel-brand">
      <Carousel
        swipeable={true}
        draggable={true}
        showDots={true}
        responsive={responsive}
        infinite={true}
        keyBoardControl={true}
        customTransition="all 0.5"
        transitionDuration={500}
        containerClass="carouselContainerMarks"
        autoPlay={false}
        itemClass="marginCarouselItemsMarks"
        customDot={<CustomDot />}
        customRightArrow={<CustomRight onClick />}
        customLeftArrow={<CustomLeft onClick />}
      >
        {items.map((item: Brand, i: number) => (
          <div className="row w-75 h-100 m-auto fx-x" key={i.toString()}>
            {visible && (
              <React.Fragment>
                <div className="col-10 col-md-6 m-auto p-0 w-100 carouselBrandImage">
                  <img
                    src={item.image_url_full}
                    alt={item.name}
                    title={item.name}
                    className="w-100"
                  />
                </div>
                <div className="col-12 col-md-6 w-100">
                  <div className="marc-x">
                    <p className="text-left poppins-Medium text-white carouselBrandTitle">
                      {item.name}
                    </p>
                    <p className="text-left text-white carouselBrandDescription">
                      {item.description}
                    </p>
                    <a
                      href={item.link}
                      target="_blank"
                      rel="noopener noreferrer"
                      className="text-left carouselBrandDescription carouselBrandLink"
                    >
                      {item.link}
                    </a>
                  </div>
                </div>
              </React.Fragment>
            )}
          </div>
        ))}
      </Carousel>
    </div>
  );

  if (isLoading) {
    return <Loading />;
  }

  return (
    <div className="marginCarousel">
      <MyCarousel items={brands} />
    </div>
  );
};

export default BrandsInfo;
