import React from "react";
import { useHistory } from "react-router-dom";
import { Link } from "react-router-dom";

import { BlogModel } from "models";
import { Banner } from "components";

// Wrapper
import dafaultImage from "assets/img/default-image.png";

export const Posts = ({ posts }: { posts: BlogModel[] }) => {
  const history = useHistory();

  const goToBlog = (blogId: number, posts: any) =>
    history.push({
      pathname: `/blog/${blogId}`,
      state: { posts: posts },
    });

  return (
    <div className="container">
      <div className="row">
        {posts.map((post: BlogModel) => (
          <div key={post.id} className="list-group-item w-100 mt-3 col-md-6">
            <div className="card-canguro ziru">
              <img
                className="card-img-top img-fluid"
                src={post.image}
                alt="..."
              />
              <div className="card-body border-none mx-3">
                <div className="cont-p">
                  <h5 className="postDate poppins-regular my-3">
                    {post.date_es}
                  </h5>
                  <h5 className="postTitle poppins-semibold">{post.title}</h5>
                  <p className="postDescription poppins-regular text-justify mt-3">
                    <span
                      dangerouslySetInnerHTML={{ __html: post.description }}
                    ></span>
                  </p>
                  <button
                    className="btnPosts"
                    onClick={() => goToBlog(post.id, posts)}
                  >
                    Aprende más →
                  </button>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export const Post = ({
  match,
  location,
  posts,
}: {
  match: any;
  location: any;
  posts: BlogModel[];
}) => {
  const {
    params: { id },
  } = match;

  const now = new Date();

  posts = location.state.posts;
  const post = posts.find((post: BlogModel) => post.id === parseInt(id));
  const blogs = localStorage.getItem("posts");
  const image_url = getImageUrl(post);

  if (!blogs) {
    localStorage.setItem(
      "posts",
      JSON.stringify([
        {
          ...post,
          expire_in: now.getTime() + 5000,
        },
      ])
    );
  }

  if (!!blogs && !!post) {
    let storagePosts = JSON.parse(blogs);

    localStorage.setItem(
      "posts",
      JSON.stringify(
        storagePosts.filter(
          ({ expire_in }: { expire_in: number }) =>
            new Date().getTime() > expire_in
        )
      )
    );

    if (storagePosts.length < 3) {
      const blog = storagePosts.find(
        ({ id }: { id: number }) => id === post.id
      );

      if (!blog) {
        localStorage.setItem(
          "posts",
          JSON.stringify([
            ...storagePosts,
            {
              ...post,
              expire_in: now.getTime() + 5000,
            },
          ])
        );
      }
    }
  }

  return (
    <React.Fragment>
      <Banner title="Blog" />
      <div className="container us-b ">
        <div className="row">
          <div className="col-md-12">
            <Link to="/blog" className="poppins-regular usButton">
              Regresar
            </Link>
          </div>
        </div>
      </div>

      <ul className=" col-8 ml-auto mr-auto list-group postIndividual">
        <li className="list-group-item rounded w-100">
          <div className="card-canguro">
            {!!post && (
              <React.Fragment>
                <img
                  src={image_url}
                  className="card-img-top img-fluid"
                  alt="Blog Imagen"
                />
                <div className="card-body">
                  <h3 className="card-text poppins-regular">{post.date_es}</h3>
                  <h5 className="card-title poppins-semibold">{post.title}</h5>
                  <p className="card-text poppins-regular">
                    <span
                      dangerouslySetInnerHTML={{ __html: post.description }}
                    ></span>
                  </p>
                </div>
              </React.Fragment>
            )}
          </div>
        </li>
      </ul>
    </React.Fragment>
  );
};

function getImageUrl(post: BlogModel | undefined) {
  if (!!post) {
    return post.image || dafaultImage;
  }
}
