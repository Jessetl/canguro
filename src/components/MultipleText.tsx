import React, { useState } from "react";

import { Input } from "components";

interface MultipleTextProps {
  required?: boolean;
  labelClass?: string | undefined;
  label?: string | undefined;
  name?: string;
  type?: "text" | "number" | "password" | "email";
  color?: string | "";
  placeholder?: string | "";
  value?: string[] | "";
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onBlur?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onKeyPress?: (event: React.KeyboardEvent<HTMLInputElement>) => void;
  className?: string;
  disabled?: boolean;
  defaultValue?: string | number | "";
  addButton?: string | "";
}

const MultipleText: React.FC<MultipleTextProps> = ({
  required = false,
  labelClass,
  label,
  name,
  type = "text",
  color,
  placeholder,
  value,
  onBlur,
  onKeyPress,
  className,
  disabled = false,
  defaultValue,
  addButton = "Add",
}) => {
  const [inputList, setInputList] = useState([{ value: "" }]);

  const handleInputChange = (index: number) => {
    return (e: React.ChangeEvent<HTMLInputElement>) => {
      const value = e.target.value;
      const list = [...inputList];
      list[index] = { value };
      setInputList(list);
    };
  };

  const handleAddClick = () => {
    setInputList([...inputList, { value: "" }]);
  };

  return (
    <div className={`form-group ${required && "is-required"}`}>
      {label && (
        <label className={`${labelClass ? labelClass : ""}`} htmlFor={name}>
          {label}
        </label>
      )}

      {inputList.map((x, i) => {
        return (
          <div>
            <Input
              placeholder={placeholder}
              type={type}
              className={`form-control ${
                color ? `input-${color}` : "input-white"
              } ${className ? className : ""}`}
              name={name}
              onChange={handleInputChange(i)}
              onBlur={onBlur}
              onKeyPress={onKeyPress}
              defaultValue={defaultValue}
              disabled={disabled}
            />
          </div>
        );
      })}

      {
        <label
          className="button-rounded border border-blue p-2"
          onClick={handleAddClick}
        >
          {addButton}
        </label>
      }
    </div>
  );
};

export default MultipleText;
