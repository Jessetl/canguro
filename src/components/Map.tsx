import React, { useState } from "react";

import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";

import {
  ComposableMap,
  Geographies,
  Geography,
  Marker,
} from "react-simple-maps";

import { mapchart } from "assets/img";

const markers = [
  {
    markerOffset: 17,
    name: "Puerto Ordaz",
    address: "Dirección:",
    coordinates: [-43.9036, 0.5806],
    description:
      "-Dirección: Centro comercial Makrocentro 1, piso 1, puerta 4, local 123.\n Delivery: 0424-9716894 / 0424-9495754",
  },
  {
    markerOffset: 17,
    name: "Puerto la Cruz",
    address: "Dirección:",
    coordinates: [-53.9036, 8.9806],
    description:
      "-Casco central, calle libertad con calle Buenos Aires, #48. Punto de referencia: frente tiendas Wrangler. \n Delivery: 0424-8697550 ",
  },
  {
    markerOffset: 17,
    name: "Barquisimeto",
    address: "Dirección:",
    coordinates: [-77.9936, 9.4806],
    description:
      "-Cosmos: Esquina de la carrera 22 con calle 26. Este: Nueva Segovia, Ciudad Llanero, carrera 1 entre calle 3 y 4 Local D11 \n Delivery: 0424-2552054 / 0424-2235389 / 0424-2256726",
  },
  {
    markerOffset: 17,
    name: "Caracas",
    address: "Direcciones:",
    coordinates: [-66.827814, 9.481708],
    description:
      "-Cortijos: Av principal de los cortijos, diagonal a FEMSA(Coca-Cola) edif. los hermanos piso 1. \n -Sabana City: Boulevard de Sabana grande, frente a Heladería La Poma. \n -Centro Comercial City Market: City 1 planta baja local 114 y City 2 piso 3 local 329. \n -Urdaneta detal y Fix: Av Urdaneta , esquina la pelota, centro profesional Urdaneta. \n -Urdaneta Mayor: Av Urdaneta diagonal al CICPC, edificio residencias 26 piso 1. \n -Sabana grande: Boulevard de sabana grande, diagonal al Banco Venezuela, por la acera del Farmatodo. \n  Delivery: 0424-2836078 / 0424-2836042 / 0424-2836037",
  },
  {
    markerOffset: 17,
    name: "Valencia",
    address: "Direcciones:",
    coordinates: [-69.9036, 13.4806],
    description:
      "-Calle Comercio entre Avenida Urdaneta y Boyaca, edificio Mendoza piso PB Local 98-68 Urbanización Centro. Avenida Bolívar con Cedeño. Centro comercial Cedeño, planta baja. Frente al metro de la Cedeño. \n Delivery: 0424-4345544",
  },
  {
    markerOffset: 17,
    name: "Acarigua",
    address: "Dirección:",
    coordinates: [-67.205811, 4.557327],
    description:
      "-Calle 30 con av.33 c.c latín Center p.b local 8b. \n Delivery: 0412-1533246",
  },
  {
    markerOffset: 17,
    name: "Maracaibo",
    address: "Dirección:",
    coordinates: [-87.61245, 11.66663],
    description:
      "-Avenida Las Delicias con calle 69, por la clínica Izot. Locales ML-446 y ML-447. \n Delivery: 0412-6915148 / 0412-1224034",
  },
  {
    markerOffset: 17,
    name: "Maturin",
    address: "Dirección:",
    coordinates: [-62.194261, 9.737635],
    description:
      "-Calle Arriojas, edificio Samuel, piso 2, sector centro. \n Delivery: 0424-9069796 / 0424-9023386",
  },
];

const MapChart = () => {
  const [name, setName] = useState("");
  const [address, setaddress] = useState("");
  const [description, setDescription] = useState("");
  const [exist, setExist] = useState(false);

  const handleClose = () => setExist(false);

  const handleModal = (name: string, address: string, description: string) => {
    return (event: React.MouseEvent<SVGPathElement, MouseEvent>) => {
      setName(name);
      setaddress(address);
      setDescription(description);

      setExist(true);
    };
  };

  return (
    <div className="w-100 h-100">
      <ComposableMap
        projection="geoAzimuthalEqualArea"
        projectionConfig={{
          rotate: [58, 20, 0],
          scale: 400,
        }}
        className="position-absolute w-75"
      >
        <Geographies geography={mapchart}>
          {({ geographies }) =>
            geographies.map((geo) => (
              <Geography
                key={geo.rsmKey}
                geography={geo}
                fill="#EAEAEC"
                stroke="#D6D6DA"
              />
            ))
          }
        </Geographies>
        {markers.map(
          ({
            name,
            address,
            coordinates,
            markerOffset,
            description,
          }: {
            name: any;
            address: any;
            coordinates: any;
            markerOffset: any;
            description: any;
          }) => (
            <Marker
              key={name}
              coordinates={coordinates}
              className="w-25 h-25"
              onClick={handleModal(name, address, description)}
            >
              <circle
                r={6}
                fill="#ffe44f"
                stroke="#fff"
                strokeWidth={2}
                className="handMap"
              />
              <text
                textAnchor="middle"
                y={markerOffset}
                style={{ fontFamily: "system-ui", fill: "#fff" }}
                className="fontMap"
              >
                {name}
              </text>
            </Marker>
          )
        )}
      </ComposableMap>
      <img src={mapchart} alt="map" className="h-100 w-100 m-auto" />

      <Modal
        show={exist}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        className="modalMap"
      >
        <Modal.Body className="modalButton text-black">
          <h3>{name}</h3>
        </Modal.Body>
        <Modal.Body className="rounded-0 bg-black next-l text-white">
        <p>{address}</p>
         {description}
        </Modal.Body>
        <Modal.Footer>
          <Button className="modalButton" onClick={handleClose}>
            Cerrar
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default MapChart;
