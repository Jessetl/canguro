import React, { useState, useEffect } from "react";
import axios from "axios";

import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

import { IdentityModel } from "models";
import { handlerError } from "utils";
import { Loading } from "components";
import { IdentityService } from "services";

import { whitePoint, yellowPoint } from "assets/img";

const Identity = () => {
  const responsive = {
    desktop: {
      breakpoint: { max: 9000, min: 1024 },
      items: 1,
      slidesToSlide: 1,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 1,
      slidesToSlide: 1,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
      slidesToSlide: 1,
    },
  };

  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [identities, setIdentities] = useState<IdentityModel[]>([]);
  const cancelToken = axios.CancelToken;

  useEffect(() => {
    const source = cancelToken.source();

    (async () => {
      try {
        const data = await IdentityService.get(source);
        setIsLoading(false);
        setIdentities(data);
      } catch (err) {
        console.log("Error");
        handlerError(err);
      }

      return () => source.cancel("Cancel by user");
    })();
  }, [cancelToken]);

  const Identities = identities;

  const CustomDot = ({
    onMove,
    index,
    onClick,
    active,
  }: {
    onMove?: any;
    index?: any;
    onClick?: any;
    active?: any;
  }) => {
    let imagen, imagenGold;

    imagen = whitePoint;
    imagenGold = yellowPoint;

    return (
      <div className="marginPoints">
        <li
          className={active ? "active" : "inactive"}
          onClick={() => onClick()}
        >
          {active ? (
            <img src={imagenGold} alt="point gold" className="pointsCarousel" />
          ) : (
            <img src={imagen} alt="point" className="pointsCarousel" />
          )}
        </li>
      </div>
    );
  };

  const MyCarousel2 = ({ items }: { items: any }) => (
    <div className="row w-100 h-100 p-0 marginCarouselIdentity m-0">
      <div className="m-0 col-10 col-sm-6 carouselComponentIdentity w-100 h-100 mt-auto mb-auto">
        {items.map((item: any, i: number) => (
          <div className="w-100 h-100" key={i}>
            <div className="p-0 w-100 h-100">
              <p className="poppins-regular text-white">
                <span className="poppins-bold">{item.name}:</span>{" "}
                {item.description}
              </p>
            </div>
          </div>
        ))}
      </div>
      <div className="m-0 col-10 col-sm-6 carouselComponent m-auto w-100 h-100">
        <Carousel
          swipeable={true}
          draggable={true}
          showDots={true}
          responsive={responsive}
          infinite={true}
          keyBoardControl={true}
          customTransition="all 0.5"
          transitionDuration={500}
          removeArrowOnDeviceType={["desktop", "tablet", "mobile"]}
          containerClass="carouselContainer"
          autoPlay={true}
          itemClass="marginCarouselItemsIdentity marginCarouselItems"
          customDot={<CustomDot />}
        >
          {items.map((item: IdentityModel, i: number) => (
            <div className="row bg-black carouselIdentity text-center" key={i}>
              <div className="p-0 w-100 h-100 m-auto text-center">
                <img
                  src={item.image_url}
                  alt={item.name}
                  title={item.name}
                  className="w-100 h-100 m-auto"
                />
              </div>
            </div>
          ))}
        </Carousel>
      </div>
    </div>
  );

  if (isLoading) {
    return <Loading />;
  }

  return (
    <div className="marginCarousel w-100 h-100">
      <MyCarousel2 items={Identities} />
    </div>
  );
};

export default Identity;
