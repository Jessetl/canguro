import React, { useState, useEffect } from "react";
import instagram from "../../src/assets/icons/instagram2.svg"; //"../../assets/icons/instagram2.svg";

import { handlerError } from "utils";
import axios from "axios";

import { VenueModel } from "models";
import { VenueService } from "services";
import { Loading } from "components";

const CanguroMundo = () => {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [venues, setVenues] = useState<VenueModel[]>([]);
  const cancelToken = axios.CancelToken;

  useEffect(() => {
    const source = cancelToken.source();

    (async () => {
      try {
        const data = await VenueService.getCanguroMundo(source);

        setIsLoading(false);
        setVenues(data);
      } catch (err) {
        console.log("Error");
        handlerError(err);
      }

      return () => source.cancel("Cancel by user");
    })();
  }, [cancelToken]);

  let address1,
    address2,
    name1,
    name2,
    image1,
    image2,
    web_url1,
    web_url2,
    ig_url1,
    ig_url2,
    country1,
    country2;

  for (let i = 0; i < venues.length; i++) {
    if (i == 0) {
      address1 = venues[i].address;
      name1 = venues[i].name;
      image1 = venues[i].image_url;
      web_url1 = venues[i].web_url;
      ig_url1 = venues[i].ig_url;
      country1 = venues[i].name || "Ecuador";
    } else {
      address2 = venues[i].address;
      name2 = venues[i].name;
      image2 = venues[i].image_url;
      web_url2 = venues[i].web_url;
      ig_url2 = venues[i].ig_url;
      country2 = venues[i].name || "Ecuador";
    }
  }

  if (isLoading) {
    return <Loading />;
  }

  return (
    <div className="row w-100 h-100  m-auto ">
      <div className="row col-12 col-sm-6 w-100 m-auto canguroWorldText p-0">
        <div className="bor-world">
        <div className="col-5 m-0 w-100 p-0">
          <img src={image1} alt={name1} className="w-100" />
        </div>
        <div className="col-7 m-0 mt-auto mr-auto mb-auto">
          <h3 className="text-left poppins-semibold text-white">{country1}</h3>
          <p className="text-left popins-regular text-white">
            <span className="poppins-regular bold">Dirección:</span> {address1}
          </p>
          <p className="text-left popins-regular text-white truncate">
            Página:{" "}
            <a
              href={web_url1}
              target="_blank"
              rel="noopener noreferrer"
              className="linkWeb"
            >
              {web_url1}
            </a>
          </p>
          <a className=" m-0 p-0" href={ig_url1} target="_blank">
            <img
              className="canguroWorldButton"
              alt="instagram"
              src={instagram}
            />
          </a>
        </div>
      </div>
      </div>
      <div className="row col-12 col-sm-6 w-100 m-auto canguroWorldText p-0">
        <div className="bor-world">
        <div className="col-5 m-0 w-100 p-0">
          <img src={image2} alt={name2} className="w-100" />
        </div>
        <div className="col-7 m-0 mt-auto mr-auto mb-auto">
          <h3 className="text-left poppins-semibold text-white">{country2}</h3>
          <p className="text-left popins-regular text-white">
            <span className="poppins-regular bold">Dirección:</span> {address2}
          </p>
          <p className="text-left popins-regular text-white truncate">
            Página:{" "}
            <a
              href={web_url2}
              target="_blank"
              rel="noopener noreferrer"
              className="linkWeb"
            >
              {web_url2}
            </a>
          </p>
          <a className=" m-0 p-0" href={ig_url2} target="_blank">
            <img
              className="canguroWorldButton"
              alt="instagram"
              src={instagram}
            />
          </a>
        </div>
        </div>
      </div>
    </div>
  );
};

export default CanguroMundo;
