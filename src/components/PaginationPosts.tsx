import React from "react";

const Pagination = ({
  postsPerPage,
  totalPosts,
  paginate,
  currentPage,
}: {
  postsPerPage: number;
  totalPosts: number;
  paginate: (page: number) => void;
  currentPage: number;
}) => {
  const pageNumber = [];

  for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
    pageNumber.push(i);
  }

  return (
    <div className="mt-2">
      <nav>
        <ul className="pagination">
          {pageNumber.map((number) => {
            if (number === currentPage) {
              return (
                <li key={number} className="page-item align-middle">
                  <a
                    href="#!"
                    onClick={() => paginate(number)}
                    className="page-link activePaginationBG"
                  >
                    <p className="activePagination p-0 m-0">{number}</p>
                  </a>
                </li>
              );
            } else {
              return (
                <li key={number} className="page-item">
                  <a
                    href="#!"
                    onClick={() => paginate(number)}
                    className="page-link BgGray"
                  >
                    <p className="text-white p-0 m-0">{number}</p>
                  </a>
                </li>
              );
            }
          })}
        </ul>
      </nav>
    </div>
  );
};

export default Pagination;
