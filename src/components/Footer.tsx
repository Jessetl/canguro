import React, { ReactNode } from "react";
import { RouteComponentProps, withRouter } from "react-router";
import { Link } from "react-router-dom";
import { logo } from "assets/img";

import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import UsService from "services/modules/usService";
import { handlerError } from "utils";

import Instagram from "assets/icons/instagram.svg";
import Facebook from "assets/icons/facebook.svg";
import Twitter from "assets/icons/twitter.svg";
import Phone from "assets/icons/phone.svg";
import mar from "assets/icons/map-info.svg";

import ButtonsMobile from "./ButtonsMobile";

import { RootState } from "reducers";
import { connect, ConnectedProps } from "react-redux";

const mapState = (state: RootState) => ({
  info: state.contact,
});

const connector = connect(mapState);

type Props = ConnectedProps<typeof connector> & RouteComponentProps;

type State = {
  email: string;
  twitter: string;
  instagram: string;
  facebook: string;
  tlf: string;
  mision: string;
  vision: string;
  address: string;
  moral: string;
};

const initialState: State = {
  twitter: "",
  email: "",
  instagram: "",
  facebook: "",
  tlf: "",
  mision: "",
  vision: "",
  moral: "",
  address: "",
};

class Footer extends React.PureComponent<Props, State> {
  public state: State;

  constructor(props: Props) {
    super(props);

    this.state = initialState;
  }

  componentDidMount() {
    if (this.props.info) {
      const { info } = this.props;

      this.setState((prevState: any) => {
        return {
          ...prevState,
          twitter: info.twitter,
          instagram: info.instagram,
          facebook: info.facebook,
          email: info.email,
          tlf: info.tlf,
          address: info.address,
        };
      });
    }

    UsService.getAll()
      .then((us) => {
        if (us.length > 0) {
          this.setState((prevState: any) => {
            return {
              ...prevState,
              moral: us[0].moral,
              mision: us[0].mision,
              vision: us[0].vision,
            };
          });
        }
      })
      .catch((err) => handlerError(err));
  }

  render(): ReactNode {
    const { info } = this.props;
    const { twitter, instagram, facebook, tlf, moral, address } = this.state;

    const replaceText = address ? address.replace("\n", "<br/>") : "";

    return (
      <div className="footer" id="footer">
        <Navbar expand="lg" className="shadow-sm size p-0">
          <Row className="w-100 p-0 m-auto">
            <Col md={3} className="w-100 p-0 m-0">
              <div className="col1Footer text-center">
                <Link to="/home">
                  <img src={logo} className="imgLogoFooter" alt="Logo" />
                </Link>
                <p className="text-white fontFooter pt-0">{moral}</p>
              </div>
            </Col>
            <Col md={2} className="w-100">
              <h6 className="poppins-regular text-white">Categorías</h6>
              <ul className="text-white text-left list-unstyled">
                <li className="listFooter">
                  <Link to="/home">Inicio</Link>
                </li>
                <li className="listFooter">
                  <Link to="/us">Nosotros</Link>
                </li>
                <li className="listFooter">
                  <a
                    href="http://142.93.197.163/ecommerce/public/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    Tienda
                  </a>
                </li>
                <li className="listFooter">
                  <Link to="/blog">Blog</Link>
                </li>
                <li className="listFooter">
                  <Link to="/courses">Cursos</Link>
                </li>
                <li className="listFooter">
                  <a href="#brands">Nuestras Marcas</a>
                </li>
                <li className="listFooter">
                  <a href="#footer">Contáctanos</a>
                </li>
              </ul>
            </Col>
            <Col md={5}>
              <Row className="w-100 p-0 m-auto">
                <Col md={6}>
                  <h6 className="poppins-regular text-white">Redes Sociales</h6>
                  <div className="redes-footer">
                    <Navbar.Text className="text-white fontSubFooter p-0 m-0">
                      <Navbar.Brand
                        target="_blank"
                        href={twitter}
                        className="d-inline-block p-0 m-0"
                      >
                        <img
                          src={Twitter}
                          alt="Logo"
                          className="imgMenuFooter p-0"
                        />
                      </Navbar.Brand>
                      <Navbar.Brand
                        target="_blank"
                        href={facebook}
                        className="d-inline-block p-0 m-0"
                      >
                        <img
                          src={Facebook}
                          alt="Logo"
                          className="imgMenuFooter p-0"
                        />
                      </Navbar.Brand>
                      <Navbar.Brand
                        target="_blank"
                        href={instagram}
                        className="d-inline-block p-0 m-0"
                      >
                        <img
                          src={Instagram}
                          alt="Logo"
                          className="imgMenuFooter p-0"
                        />
                      </Navbar.Brand>
                    </Navbar.Text>

                    <p className="text-white fontQuestion">
                      <a
                        className="text-white"
                        href={`tel:${tlf}`}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        <img src={Phone} alt="Logo" className="imgMenuFooter" />
                      </a>
                      ¿Tienes preguntas?
                    </p>

                    <p className="text-white fontSubMenu fontSubFooter p-0 m-0">
                      <a
                        className="text-white"
                        href={`tel:${tlf}`}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        {tlf}
                      </a>
                    </p>
                  </div>
                </Col>
                <Col md={6}>
                  <div className="address-footer">
                    <p className="text-white">
                      <img src={mar} alt="Logo" className="imgMenuFooter" />
                      Dirección
                    </p>
                    {address && (
                      <p
                        className="text-white fontSubMenu fontSubFooter p-0 m-0"
                        dangerouslySetInnerHTML={{ __html: replaceText }}
                      ></p>
                    )}
                  </div>
                </Col>
              </Row>
            </Col>
            <Col md={2} className="buttonMobileFooter">
              <h6 className="poppins-regular text-white">App</h6>
              {!!info && <ButtonsMobile />}
            </Col>
          </Row>
        </Navbar>
        <Navbar expand="lg" className="subHeaderFooter">
          <Nav className="m-auto">
            <Navbar.Text className="fontSubFooter">
              © 2020 Designed by{" "}
              <a
                target="_blank"
                href="https://limonbyte.com/"
                rel="noopener noreferrer"
              >
                LimónByte
              </a>{" "}
              | All rights reserved
            </Navbar.Text>
          </Nav>
        </Navbar>
      </div>
    );
  }
}

export default withRouter(connector(Footer));
