import React from 'react';

const Textarea = (props: any) => (
	<div className="form-group">
		{ props.label && <label htmlFor={ props.name } className="poppins-regular">{ props.label }</label> }
		<textarea 
			{ ...props }
			rows={ props.rows ? props.rows : 4 }
			type={ props.type ? props.type : 'text' }
			className="form-control poppins-regular"
			name={ props.name }
			value = {props.value}
			onChange = {props.onChange}>
		</textarea>
	</div>
)

export default Textarea;