import React from "react";
import Card from "react-bootstrap/Card";

import { bannerGeneral } from "assets/img";

const Banner = ({
  title,
  message,
  principal = false,
  banner_image = bannerGeneral,
}: {
  title: string;
  message?: string;
  principal?: boolean;
  banner_image?: string;
}) => {
  let view;
  banner_image = banner_image;

  if (principal) {
    view = (
      <Card
        className="m-auto p-0 rounded-0 jumbotronPrincipal"
        style={{ backgroundImage: "url( " + banner_image + ")" }}
      >
        <div className="mb-auto mr-auto h-100 w-100">
          <p className="titleBanner titleBannerPrincipal alignBannerPrincipal poppins-bold">
            {title}
          </p>
          <p className="textBanner poppins-regular">{message}</p>
        </div>
      </Card>
    );
  } else {
    view = (
      <Card
        className="m-auto p-0 rounded-0 jumbotronGeneral poppins-regular"
        style={{ backgroundImage: "url(" + banner_image + ")" }}
      >
        <div className="mt-auto mb-auto mr-auto bannerGeneralHeight">
          <p className="titleBannerGeneral my-0 poppins-medium">{title}</p>
        </div>
      </Card>
    );
  }

  return <div className="w-100 h-100">{view}</div>;
};

export default Banner;
