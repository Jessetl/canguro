import React, { useState, useEffect } from "react";
import { handlerError } from "utils";
import axios from "axios";
import BlogService from "services/modules/blogService";

import CardDeck from "react-bootstrap/CardDeck";
import Card from "react-bootstrap/Card";

import { Link } from "react-router-dom";
import { puntos } from "assets/img";
import { BlogModel } from "models";
import { Loading } from "components";

const BlogHome = () => {
  const [isLoading, setIsLoading] = useState<boolean>(true);

  const [blogs, setBlogs] = useState<BlogModel[]>([]);

  const cancelToken = axios.CancelToken;

  useEffect(() => {
    const source = cancelToken.source();

    (async () => {
      try {
        const { data } = await BlogService.getLast3(source);
        setIsLoading(false);
        setBlogs(data);
      } catch (err) {
        handlerError(err);
      }

      return () => source.cancel("Cancel by user");
    })();
  }, [cancelToken]);

  let title1,
    title2,
    title3,
    created_at1,
    created_at2,
    created_at3,
    image1,
    image2,
    image3,
    id1 = 0,
    id2 = 0,
    id3 = 0;
  if (blogs.length > 0) {
    for (let i = 0; i < blogs.length; i++) {
      if (i === 0) {
        title1 = blogs[i].title;
        created_at1 = blogs[i].created_at;
        image1 = blogs[i].image;
        id1 = blogs[i].id;
      }
      if (i === 1) {
        title2 = blogs[i].title;
        created_at2 = blogs[i].created_at;
        image2 = blogs[i].image;
        id2 = blogs[i].id;
      }
      if (i === 2) {
        title3 = blogs[i].title;
        created_at3 = blogs[i].created_at;
        image3 = blogs[i].image;
        id3 = blogs[i].id;
      }
    }
  } else {
    title1 = "";
    title2 = "";
    title3 = "";
    created_at1 = null;
    created_at2 = null;
    created_at3 = null;
    image1 = "";
    image2 = "";
    image3 = "";
  }

  if (isLoading) {
    return <Loading />;
  }

  return (
    <div className="w-100 p-0 m-0 bgBackground">
      <div className="w-100 p-0 m-0 bgBackground">
        <div className="w-100 position-absolute overflow-hidden">
          <div className="w-100 d-flex flex-row">
            <img
              src={puntos}
              alt="puntos"
              className=" pointsImageTopHomeBlog"
            />
          </div>
          <div className="w-100 d-flex flex-row-reverse">
            <img src={puntos} alt="puntos" className="pointsImageHomeBlog" />
          </div>
        </div>

        <div className="w-100 col-11 m-auto">
          <div className="row marginTitleBlogHome">
            <div className="col-6">
              <h3 className="subtitleBlogHome poppins-regular">
                Noticias Recientes
              </h3>

              <h5 className="titleBlogHome poppins-semibold">Blog</h5>
            </div>
            <div className="col-6 mt-auto mb-auto">
              <Link
                to={`/blog`}
                className="btnHomeBlog poppins-regular text-center float-right"
              >
                Todas las noticias
              </Link>
            </div>
          </div>
        </div>
        <CardDeck className="w-100 cardDeck col-11 m-auto">
          {title1 && (
            <Card className="firstCard overflow-hidden">
              <Card.Img variant="top" src={image1} />
              <Card.Body>
                <Card.Text className="poppins-regular">{created_at1}</Card.Text>
                <div className="w-100 d-flex align-content-center flex-wrap">
                  <Card.Title className="poppins-semibold">{title1}</Card.Title>
                </div>
              </Card.Body>
              <div className="w-100">
                <Link
                  to={{
                    pathname: `/blog/${id1}`,
                    state: { posts: blogs },
                  }}
                  className="btnHomeBlog poppins-regular align-items-center text-center"
                >
                  Aprende más →
                </Link>
              </div>
            </Card>
          )}
          {title2 && (
            <Card className="secondCard overflow-hidden">
              <Card.Img variant="top" src={image2} />
              <Card.Body>
                <Card.Text className="poppins-regular">{created_at2}</Card.Text>
                <div className="w-100 h-75  d-flex align-content-center flex-wrap">
                  <Card.Title className="poppins-semibold align-middle">
                    {title2}
                  </Card.Title>
                </div>
              </Card.Body>
              <div className="w-100">
                <Link
                  to={{
                    pathname: `/blog/${id2}`,
                    state: { posts: blogs },
                  }}
                  className="btnHomeBlog poppins-regular align-items-center text-center"
                >
                  Aprende más →
                </Link>
              </div>
            </Card>
          )}
          {title3 && (
            <Card className="thirdCard overflow-hidden">
              <Card.Img variant="top" src={image3} />
              <Card.Body>
                <Card.Text className="poppins-regular">{created_at3}</Card.Text>

                <div className="w-100 h-75  d-flex align-content-center flex-wrap">
                  <Card.Title className="poppins-semibold align-middle">
                    {title3}
                  </Card.Title>
                </div>
              </Card.Body>
              <div className="w-100">
                <Link
                  to={{
                    pathname: `/blog/${id3}`,
                    state: { posts: blogs },
                  }}
                  className="btnHomeBlog poppins-regular align-items-center text-center"
                >
                  Aprende más →
                </Link>
              </div>
            </Card>
          )}
        </CardDeck>
      </div>
    </div>
  );
};

export default BlogHome;
