import React from "react";

import { puntos, usImage } from "assets/img";

import arrow from "../assets/icons/arrow.svg";
import { Link } from "react-router-dom";

const UsArea = ({
  title,
  message,
  image,
  principal = false,
}: {
  title: string;
  image: string;
  message: string;
  principal?: boolean;
}) => {
  let view;
  const replaceText = message ? message.replace("\n", "<br/>") : "";

  if (principal) {
    view = (
      <div className="row w-100 h-100 usBackground usText">
        <div className="col-10 col-sm-5 h-100 m-auto usZoneImage">
          <img src={image || usImage} alt="Nosotros" className="usImage" />
        </div>
        <div className="col-10 col-sm-5 m-auto">
          <h2 className="usTitleArea poppins-semibold">{title}</h2>
          <p className="text-left Poppins Medium text-white usDescription">
            {message}
          </p>
        </div>
      </div>
    );
  } else {
    view = (
      <div className="row w-100 h-100 usBackground usText">
        <div className="w-100 position-absolute overflow-hidden">
          <div className="w-100 d-flex flex-row">
            <img src={puntos} alt="puntos" className=" pointsImageTopUs" />
          </div>
          <div className="w-100 d-flex flex-row-reverse">
            <img src={puntos} alt="puntos" className="pointsImageUs" />
          </div>
        </div>

        <div className="col-10 col-sm-5 h-100 m-auto usZoneImage">
          <div className="efect-ix">
            <figure className="effect-layla">
              <img src={image || usImage} alt="Nosotros" className="usImage" />
              <figcaption></figcaption>
            </figure>
          </div>
        </div>
        <div className="col-11 col-sm-5 m-auto">
          <h2 className="usTitleArea poppins-semibold">{title}</h2>
          <p
            className="text-left poppins-regular text-white usDescription"
            dangerouslySetInnerHTML={{ __html: replaceText }}
          ></p>
          <Link to="/us" className=" poppins-regular usButton">
            Acerca de la empresa{" "}
            <img src={arrow} alt="arrow" className="arrowButtonUs" />
          </Link>
        </div>
      </div>
    );
  }

  return <div className="w-100 h-100">{view}</div>;
};
export default UsArea;

/*import React, { ReactNode } from "react";
import { RouteComponentProps, withRouter } from 'react-router';
import { handlerError, showSuccess } from "utils";
import { initialState, State } from "../screens/web/FormState";
import UsService from "services/modules/usService";

type Props = RouteComponentProps;

class UsArea extends React.PureComponent<Props, State> { 

  public state: State;

  constructor(props: Props) {
    super(props);
    this.state = initialState;
  }

  componentDidMount() {
    this.getUs();
  }

  //obtener us
  getUs = () => {
    UsService.getAll()
      .then((us) => {
        if (us.length > 0) {
          this.setState((prevState: any) => {
            return {
              ...prevState,
              moral: us[0].moral,
              us_image: us[0].us_image,
            };
          });
        }
        
      })
      .catch((err) => handlerError(err));
  };

  render(): ReactNode {
    const { moral, us_image } = this.state;

    let view;
    let principal = true;
    
      if (principal) {
        view = (
          <div className="row w-100 h-100 usBackground usText">
            <div className="col-10 col-sm-5 h-100 m-auto usZoneImage">
              <img src={us_image} alt="Nosotros" className="usImage" />
            </div>
            <div className="col-10 col-sm-5 m-auto">
              <p className="mb-0 poppins-regular usSubtitle">
                Acerca de la empresa
              </p>
              <h3 className="usTitleArea poppins-semibold">Nosotros</h3>
              <p className="text-left popins-regular text-white usDescription">
                {moral}
              </p>
            </div>
          </div>
        );
      } else {
        view = (
          <div className="row w-100 h-100 usBackground usText">
            <div className="w-100 position-absolute overflow-hidden">
              <div className="w-100 d-flex flex-row">
                <img src={puntos} alt="puntos" className=" pointsImageTopUs" />
              </div>
              <div className="w-100 d-flex flex-row-reverse">
                <img src={puntos} alt="puntos" className="pointsImageUs" />
              </div>
            </div>
    
            <div className="col-10 col-sm-5 h-100 m-auto usZoneImage">
              <img src={us_image} alt="Nosotros" className="usImage" />
            </div>
            <div className="col-11 col-sm-5 m-auto">
              <p className="mb-0 poppins-regular usSubtitle">
                Acerca de la empresa
              </p>
              <h3 className="usTitleArea poppins-semibold">Nosotros</h3>
              <p className="text-left popins-regular text-white usDescription">
                {moral}
              </p>
              <p className="text-left popins-regular text-white usDescription">
                {moral}
              </p>
              <a className=" poppins-regular usButton" href="/us">
                Acerca de la empresa{" "}
                <img src={arrow} alt="arrow" className="arrowButtonUs" />
              </a>
            </div>
          </div>
        );
      }

    return(

      <div className="w-100 h-100">{view}</div>

    );

  }

}

export default withRouter(UsArea); */
