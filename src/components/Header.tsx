import React, { ReactNode } from "react";

import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";

import Mail from "assets/icons/mail.svg";
import Hour from "assets/icons/hour.svg";

import Instagram from "assets/icons/instagram.svg";
import Facebook from "assets/icons/facebook.svg";
import Twitter from "assets/icons/twitter.svg";

import { Link } from "react-router-dom";
import { Link as LinkWithScroll } from "react-scroll";

import { RouteComponentProps, withRouter } from "react-router";

import { logo } from "assets/img";

import { RootState } from "reducers";
import { connect, ConnectedProps } from "react-redux";

export const HeaderQueue = ({ area }: { area: string }) => {
  return (
    <Navbar bg="white" expand="lg" className="header shadow-drl">
      <Navbar.Text className="drl-title text-orange">{area}</Navbar.Text>
      <Navbar.Collapse className="py-3 justify-content-end">
        <Navbar.Brand href="#home">
          <img
            src={logo}
            width="225"
            height="75"
            alt="Logo"
            className="float-right mr-5"
          />
        </Navbar.Brand>
      </Navbar.Collapse>
    </Navbar>
  );
};

const mapState = (state: RootState) => ({
  info: state.contact,
});

const connector = connect(mapState);

type Props = ConnectedProps<typeof connector> & RouteComponentProps;

type State = {
  email: string;
  twitter: string;
  instagram: string;
  facebook: string;
  hours: string;
};

const initialState: State = {
  twitter: "",
  email: "",
  instagram: "",
  facebook: "",
  hours: "",
};

class Header extends React.PureComponent<Props, State> {
  public state: State;

  constructor(props: Props) {
    super(props);
    this.state = initialState;
  }

  componentDidMount() {
    if (this.props.info) {
      const { info } = this.props;

      this.setState((prevState: any) => {
        return {
          ...prevState,
          twitter: info.twitter,
          instagram: info.instagram,
          facebook: info.facebook,
          email: info.email,
          hours: info.horario,
        };
      });
    }
  }

  render(): ReactNode {
    const { twitter, instagram, email, facebook, hours } = this.state;

    const brands: string = "brands";

    return (
      <div className="sticky-top">
        <Navbar expand="lg" className="subHeader shadow-sm size">
          <Nav className="m-0 mr-auto p-0 h-100 ">
            <Navbar.Text className="d-inline-block text-white border-left border-right p-0 border-secondary">
              <a href={twitter} target="_blank" rel="noopener noreferrer">
                <img
                  src={Twitter}
                  alt="Logo"
                  className="socialMediaHeader imgMenu"
                />
              </a>
            </Navbar.Text>
            <Navbar.Text className="d-inline-block text-white border-left border-right p-0 border-secondary">
              <a href={facebook} target="_blank" rel="noopener noreferrer">
                <img
                  src={Facebook}
                  alt="Logo"
                  className="socialMediaHeader imgMenu"
                />
              </a>
            </Navbar.Text>
            <Navbar.Text className="d-inline-block text-white border-left border-right p-0 border-secondary">
              <a href={instagram} target="_blank" rel="noopener noreferrer">
                <img
                  src={Instagram}
                  alt="Logo"
                  className="socialMediaHeader imgMenu"
                />
              </a>
            </Navbar.Text>
          </Nav>
          <Nav className="ml-auto p-0">
            <Navbar.Text className="d-inline-block text-white border-left border-right p-0 pl-1 pr-2 border-secondary">
              <img
                src={Mail}
                alt="Logo"
                className="socialMedia imgMenuInfo p-0"
              />
              <p className="d-inline-block fontSubMenu m-0 pt-auto pb-auto poppins-regular">
                <a href={`mailto:${email}`}>{email}</a>
              </p>
            </Navbar.Text>
            <Navbar.Text className="d-inline-block text-white border-left border-right p-0 pl-1 pr-2 border-secondary">
              <img
                src={Hour}
                alt="Logo"
                className="socialMedia imgMenuInfo p-0"
              />
              <p className="d-inline-block fontSubMenu m-0 pt-auto pb-auto poppins-regular">
                {hours}
              </p>
            </Navbar.Text>
          </Nav>
        </Navbar>
        <Navbar
          collapseOnSelect
          expand="md"
          className="header shadow-sm pt-0 pb-0"
        >
          <Navbar.Brand href="/canguro-web/web" className="p-0 m-0">
            <img src={logo} className="imgLogo" alt="Logo" />
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse
            id="responsive-navbar-nav"
            className="justify-content-center h-100 mt-auto mb-auto toggleNav"
          >
            <Link to="/home" className="fontMenu poppins-regular h-100">
              Inicio
            </Link>
            <Link to="/us" className="fontMenu poppins-regular h-100">
              Nosotros
            </Link>
            <a
              className="fontMenu poppins-regular h-100"
              href="http://142.93.197.163/ecommerce/public/"
              target="_blank"
              rel="noopener noreferrer"
            >
              Tienda
            </a>
            <Link to="/blog" className="fontMenu poppins-regular h-100">
              Blog
            </Link>
            <Link to="/courses" className=" fontMenu poppins-regular h-100">
              Cursos
            </Link>
            <Link
              to={`/home/${brands}`}
              className=" fontMenu poppins-regular h-100"
            >
              Nuestras Marcas
            </Link>
            <LinkWithScroll
              to="footer"
              smooth={true}
              className=" fontMenu poppins-regular h-100"
            >
              Contáctanos
            </LinkWithScroll>
          </Navbar.Collapse>
        </Navbar>
      </div>
    );
  }
}

export default withRouter(connector(Header));
