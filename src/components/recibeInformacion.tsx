import React, { ReactNode } from "react";
import { RouteComponentProps, withRouter } from "react-router-dom";
import axios, { CancelTokenSource } from "axios";
import { handlerError, showSuccess } from "utils";

import SubscribeReceiveInfoService from "services/modules/subscribeReceiveInfoService";
import { SubscribeReceiveInfoModel } from "models";
import { Button, Input } from "components";

type Props = RouteComponentProps;
type State = { isSubmit: boolean; email: String };
const initialState: State = { isSubmit: false, email: "" };

const onSubmit = (data: any, e: any) => {
    e.target.reset();
  };

class RecibeInformacion extends React.PureComponent<Props, State> {
  public state: State;
  public source: CancelTokenSource;

  constructor(props: Props) {
    super(props);

    this.state = initialState;

    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();
  }

  handleChange = (key: string) => {
    return (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value } = event.currentTarget;

      this.setState((prevState: any) => {
        return {
          ...prevState,
          [key]: value,
        };
      });
    };
  };

  handleSubmit = async (
    event: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    event.preventDefault();
    const { email } = this.state;
    if (this.state.isSubmit) {
      return;
    }

    this.setState({
      isSubmit: true,
    });

    const trabajaconnosotros: SubscribeReceiveInfoModel = {
      email,
    };

    SubscribeReceiveInfoService.sendEmail(trabajaconnosotros, this.source)
      .then(() => {
        showSuccess();
      })
      .catch(handlerError)
      .finally(() => this.setState({ isSubmit: false }));
  };

  render(): ReactNode {
    const { isSubmit } = this.state;

    return (
      <form
        onSubmit={this.handleSubmit}
        noValidate
        autoComplete="off"
        className="w-100 h-100 form-group"
      >
        <h3 className="poppins-semibold m-0">Recibe Información</h3>
        <p className="text-white poppins-regular">
          Suscribete y recibe información en tu correo
        </p>
        <div className="text-center">
          <Input
            name={"email"}
            color="white"
            type="text"
            placeholder="Ingresa tu correo"
            //value={email}
            onChange={this.handleChange("email")}
          />
        </div>

        <div className="pl-0 pr-0 col-3">
          <Button submitted={isSubmit} block type="submit">
            Enviar
          </Button>
        </div>
      </form>
    );
  }
}

export default withRouter(RecibeInformacion);
