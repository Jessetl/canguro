import React from "react";

interface InputProps {
  required?: boolean;
  labelClass?: string | undefined;
  label?: string | undefined;
  name?: string;
  type?: "text" | "number" | "password" | "email";
  color?: string | "";
  placeholder?: string | "";
  maxlength?: number;
  value?: string | number | "";
  className?: string;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onBlur?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onKeyPress?: (event: React.KeyboardEvent<HTMLInputElement>) => void;
  disabled?: boolean;
  defaultValue?: string | number | "";
  step?: string;
}

const Input: React.FC<InputProps> = ({
  required = false,
  labelClass,
  label,
  name,
  type = "text",
  color,
  placeholder,
  value,
  maxlength,
  onChange,
  onBlur,
  onKeyPress,
  className,
  disabled = false,
  defaultValue,
  step,
}) => {
  return (
    <div className={`form-group ${required && "is-required"}`}>
      {label && (
        <label
          className={`${labelClass ? labelClass : ""} poppins-regular`}
          htmlFor={name}
        >
          {label}
        </label>
      )}
      <input
        step={step}
        maxLength={maxlength}
        placeholder={placeholder}
        type={type}
        className={`form-control ${
          color ? `input-${color}` : "input-white poppins-regular"
        } ${className ? className : ""}`}
        name={name}
        onChange={onChange}
        onBlur={onBlur}
        onKeyPress={onKeyPress}
        value={value}
        defaultValue={defaultValue}
        disabled={disabled}
      />
    </div>
  );
};

export default Input;

// const Input	= (props: any) => (
// 	<div className="form-group">
// 		{ props.label && <label htmlFor={ props.name }>{ props.label }</label> }
// 		<input
// 			{ ...props }
// 			type={ props.type ? props.type : 'text' }
// 			className={ `form-control ${ props.color ? 'input-'+props.color : 'input-white' }` }
// 			name={ props.name } />
// 	</div>
// )
