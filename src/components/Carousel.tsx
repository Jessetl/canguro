import React, { useState, useEffect } from "react";
import { handlerError } from "utils";
import axios from "axios";
import ProductService from "services/modules/productsService";
import { Product } from "models";

import { whitePoint, yellowPoint } from "assets/img";

import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { Loading } from "components";

const responsive = {
  desktop: {
    breakpoint: { max: 9000, min: 1024 },
    items: 3,
    slidesToSlide: 3,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
    slidesToSlide: 2,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
    slidesToSlide: 2,
  },
};

const CarouselItems = () => {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [products, setProducts] = useState<Product[]>([]);
  const cancelToken = axios.CancelToken;

  useEffect(() => {
    const source = cancelToken.source();

    (async () => {
      try {
        const { data } = await ProductService.get12products(source);
        setIsLoading(false);
        setProducts(data);
      } catch (err) {
        console.log("Error");
        setIsLoading(false);
        handlerError(err);
      }

      return () => source.cancel("Cancel by user");
    })();
  }, [cancelToken]);

  const Products = products;

  const CustomDot = ({
    onMove,
    index,
    onClick,
    active,
  }: {
    onMove?: any;
    index?: any;
    onClick?: any;
    active?: any;
  }) => {
    let imagen, imagenGold;

    imagen = whitePoint;
    imagenGold = yellowPoint;

    return (
      <div className="marginPoints">
        <li
          className={active ? "active" : "inactive"}
          onClick={() => onClick()}
        >
          {active ? (
            <img
              src={imagenGold}
              alt="point selected"
              className="pointsCarousel"
            />
          ) : (
            <img src={imagen} alt="point" className="pointsCarousel" />
          )}
        </li>
      </div>
    );
  };

  const MyCarousel2 = ({ items }: { items: any }) => (
    <div className="m-0 carouselComponent w-100 h-100">
      <Carousel
        swipeable={true}
        draggable={true}
        showDots={true}
        responsive={responsive}
        infinite={true}
        keyBoardControl={true}
        customTransition="all 0.9"
        transitionDuration={500}
        //removeArrowOnDeviceType={["desktop", "tablet", "mobile"]}
        containerClass="carouselContainer"
        autoPlay={false}
        itemClass="marginCarouselItems"
        customDot={<CustomDot />}
      >
        {items.map((item: any, i: number) => (
          <div className="row bg-black carouselCard" key={i}>
            <div className="carouselImage col-5 px-0 w-100 h-100">
              <img
                src={item.image_url}
                alt={item.name}
                title={item.name}
                className="w-100"
              />
            </div>
            <div className="col-7 bgFooter">
              <p className="text-left poppins-semibold text-white carouselTitle">
                {item.name}
              </p>
              <p className="text-left text-white carouselDescription">
                {item.description}
              </p>
            </div>
          </div>
        ))}
      </Carousel>
    </div>
  );

  if (isLoading) {
    return <Loading />;
  }

  return (
    <div className="marginCarousel w-100 h-100">
      <MyCarousel2 items={Products} />
    </div>
  );
};

export default CarouselItems;
