import React, { useState, useEffect } from "react";
import {
  fondoValores,
  fondoEvolucion,
  fondoFamiliaridad,
  fondoEspiritualidad,
  fondoCompromiso,
  fondoEquipo,
  fondoOportunidad,
} from "assets/img";

import { handlerError } from "utils";
import axios from "axios";
import ValueService from "services/modules/valueService";
import { ValueModel } from "models";

const Accordion = () => {
  const [fond, setFond]: any = useState();
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [values, setValues] = useState<ValueModel[]>([]);
  const cancelToken = axios.CancelToken;

  useEffect(() => {
    const source = cancelToken.source();

    (async () => {
      try {
        const { data } = await ValueService.getAll(source);
        setIsLoading(false);
        setValues(data);
      } catch (err) {
        console.log("Error");
        handlerError(err);
      }

      return () => source.cancel("Cancel by user");
    })();
  }, [cancelToken]);

  if (!fond) {
    setFond(fondoValores);
  }

  const changeBackground = (index: number) => {
    return (e: React.MouseEvent<HTMLLIElement>) => {
      if (index === 1) {
        setFond(fondoEvolucion);
      } else if (index === 2) {
        setFond(fondoFamiliaridad);
      } else if (index === 3) {
        setFond(fondoEspiritualidad);
      } else if (index === 4) {
        setFond(fondoCompromiso);
      } else if (index === 5) {
        setFond(fondoEquipo);
      } else if (index === 6) {
        setFond(fondoOportunidad);
      } else {
        setFond(fondoValores);
      }
    };
  };

  return (
    <div className="w-100 m-auto p-0 text-center">
      <div id="preload">
        <img src={fondoValores} />
        <img src={fondoOportunidad} />
        <img src={fondoEquipo} />
        <img src={fondoCompromiso} />
        <img src={fondoEspiritualidad} />
        <img src={fondoFamiliaridad} />
        <img src={fondoEvolucion} />
      </div>
      <div className="accordian w-100 p-0 text-center">
        <ul
          className="backgroundAccordion p-0"
          style={{ backgroundImage: "url( " + fond + ")" }}
        >
          <li className="item1Accordion p-0" onMouseOver={changeBackground(1)}>
            <h2 className="poppins-medium titleAccordion">
              {values.length > 0 && values[0].name}
            </h2>
            <p className="poppins-regular">
              {values.length > 0 && values[0].description}
            </p>
          </li>
          <li className="item2Accordion p-0" onMouseOver={changeBackground(2)}>
            <h2 className="poppins-medium titleAccordion">
              {values.length > 0 && values[1].name}
            </h2>
            <p className="poppins-regular">
              {values.length > 0 && values[1].description}
            </p>
          </li>
          <li className="item3Accordion p-0" onMouseOver={changeBackground(3)}>
            <h2 className="poppins-medium titleAccordion">
              {values.length > 0 && values[2].name}
            </h2>
            <p className="poppins-regular">
              {values.length > 0 && values[2].description}
            </p>
          </li>
          <li className="item4Accordion p-0" onMouseOver={changeBackground(4)}>
            <h2 className="poppins-medium titleAccordion">
              {values.length > 0 && values[3].name}
            </h2>
            <p className="poppins-regular">
              {values.length > 0 && values[3].description}
            </p>
          </li>
          <li className="item5Accordion p-0" onMouseOver={changeBackground(5)}>
            <h2 className="poppins-medium titleAccordion">
              {values.length > 0 && values[4].name}
            </h2>
            <p className="poppins-regular">
              {values.length > 0 && values[4].description}
            </p>
          </li>
          <li className="item6Accordion p-0" onMouseOver={changeBackground(6)}>
            <h2 className="poppins-medium titleAccordion">
              {values.length > 0 && values[5].name}
            </h2>
            <p className="poppins-regular">
              {values.length > 0 && values[5].description}
            </p>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Accordion;
