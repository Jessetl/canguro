import PrivateRoute from "./route";
import Button from "./button";
import CardTitle from "./card-title";
import Card from "./card";
import DatePicker from "./datepicker";
import TimePicker from "./timepicker";
import File from "./file";
import Icon from "./icon";
import Input from "./input";
import List from "./list";
import Modal from "./modal";
import Pagination from "./pagination";
import Select from "./select";
import Table from "./table";
import Textarea from "./textarea";
import Tabs from "./tabs";
import Checkbox from "./checkbox";
import PaperClip from "./paperclip";
import InputNumber from "./input-number";
import PageTitle from "./PageTitle";
import MultipleText from "./MultipleText";
import Footer from "./Footer";
import ButtonsMobile from "./ButtonsMobile";
import Accordion from "./Accordion";
import CarouselItems from "./Carousel";
import Banner from "./Banner";
import UsArea from "./UsArea";
import InstaSlider from "./InstaSlider";
import PaginationPosts from "./PaginationPosts";
import BrandsInfo from "./BrandsInfo";
import Identity from "./Identity";
import Identidad from "./Identidad";
import MapChart from "./Map";
import CanguroMundo from "./CanguroMundo";
import Header from "./Header";

import Image from "./Image";
import ImageUpload from "./ImageUpload";

export * from "./Title";
export * from "./Header";
//export * from "./Title";
//export * from "./Header";
export * from "./Sidebar";
export * from "./Breadcrumb";
export * from "./TitleModal";
export * from "./Empty";
export * from "./Loading";
export * from "./CardCustom";
export * from "./Post";

export {
  Header,
  PrivateRoute,
  InputNumber,
  Button,
  CardTitle,
  Card,
  DatePicker,
  TimePicker,
  File,
  Icon,
  Input,
  List,
  Modal,
  Pagination,
  Select,
  Table,
  Textarea,
  Tabs,
  Checkbox,
  PaperClip,
  PageTitle,
  MultipleText,
  Footer,
  ButtonsMobile,
  Accordion,
  CarouselItems,
  UsArea,
  Banner,
  PaginationPosts,
  BrandsInfo,
  MapChart,
  Identity,
  Identidad,
  ImageUpload,
  Image,
  CanguroMundo,
  InstaSlider,
};
 