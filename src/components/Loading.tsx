import React from "react";

export const Loading = () => (
  <>
    <div className="row fluid-vh align-items-center justify-content-center">
      <div className="spinner-border text-orange" role="status">
        <span className="sr-only">Loading...</span>
      </div>
    </div>
  </>
);

export const Submitted = () => (
  <>
    <div className="row my-4">
      <div className="col text-center">
        <div className="spinner-border text-orange" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      </div>
    </div>
  </>
);
