import React from "react";

export type Options = {
  value?: string | number;
  label?: string;
};
interface SelectProps {
  required?: boolean;
  label?: string | undefined;
  name?: string;
  color?: string | "";
  placeholder?: string | "";
  maxlength?: number;
  options: Options[];
  onChange?: (event: React.ChangeEvent<HTMLSelectElement>) => void;
  valueSelect?: any;
  defaulText?: string;
}

const Select: React.FC<SelectProps> = ({
  required = false,
  label,
  name,
  color,
  options,
  onChange,
  valueSelect,
  defaulText = "Seleccione",
}) => {
  return (
    <div className={`form-group ${required && "is-required"}`}>
      {label && <label htmlFor={name}>{label}</label>}
      <select
        name={name}
        value={valueSelect}
        onChange={onChange}
        className={`form-control ${color ? color : ""}`}
      >
        <option value="">{defaulText}</option>
        {options.map(({ value, label }) => {
          return (
            <option key={value} value={value}>
              {label}
            </option>
          );
        })}
      </select>
    </div>
  );
};

export default Select;
