import React, { ReactNode } from "react";
import { RouteComponentProps, withRouter } from "react-router-dom";
import axios, { CancelTokenSource } from "axios";
import { handlerError, showSuccess } from "utils";

import TrabajaConNosotrosService from "services/modules/trabajaConNosotrosService";
import { TrabajaConNosotrosModel } from "models";
import { Button } from "components";

type Props = RouteComponentProps;
type State = { isSubmit: boolean; file: String; name: String; type: String };
const initialState: State = { isSubmit: false, file: "", name: "", type: "" };

class TrabajaConNosotros extends React.PureComponent<Props, State> {
  public state: State;
  public source: CancelTokenSource;

  constructor(props: Props) {
    super(props);

    this.state = initialState;

    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();
  }

  uploadFile = (key: string) => {
    return async (e: React.ChangeEvent<HTMLInputElement>) => {
      if (!!e.currentTarget.files?.length) {
        const file = e.currentTarget.files[0];
        const base64 = await this.convertBase64(file);
        const filename = file.name;
        const filetype = file.type;

        this.setState((prevState: any) => {
          return {
            ...prevState,
            [key]: base64,
            name: filename,
            type: filetype,
          };
        });
      }
    };
  };

  convertBase64 = (file: any) => {
    return new Promise((resolve, reject) => {
      const fileReader: any = new FileReader();

      fileReader.onload = () => {
        resolve(fileReader.result);
      };

      fileReader.onerror = (error: string) => {
        reject(error);
      };

      fileReader.readAsDataURL(file);
    });
  };

  handleSubmit = async (
    event: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    event.preventDefault();
    const { file, name, type } = this.state;
    if (!!!this.state.isSubmit) {
      this.setState({
        isSubmit: true,
      });

      const trabajaconnosotros: TrabajaConNosotrosModel = {
        file,
        name,
        type,
      };

      TrabajaConNosotrosService.sendEmail(trabajaconnosotros, this.source)
        .then(() => {
          showSuccess();
        })
        .catch(handlerError)
        .finally(() => this.setState({ isSubmit: false }));
    }
  };

  render(): ReactNode {
    const { isSubmit } = this.state;

    return (
      <form
        onSubmit={this.handleSubmit}
        noValidate
        autoComplete="off"
        className="w-100 h-100 form-group p-0"
      >
        <h3 className="poppins-semibold m-0">
          ¿Quieres ser parte de nuestro equipo?
        </h3>
        <p className="text-white poppins-regular">
          Envía tu CV a
          <a
            href="mailto:factorhumano@cangurovenezuela.com"
            className="linkWeb mx-2"
          >
            factorhumano@cangurovenezuela.com
          </a>
          te contactaremos
        </p>
        <div className="text-center marginFileCV w-75 h-75">
          <label className="upload">
            <input
              type="file"
              onChange={this.uploadFile("file")}
              accept=".pdf, .doc, .docx"
            />
          </label>
        </div>

        <div className="pl-0 pr-0 col-2 m-auto">
          <Button
            className="font-bold buttonCV"
            submitted={isSubmit}
            block
            type="submit"
          >
            Enviar
          </Button>
        </div>
      </form>
    );
  }
}

export default withRouter(TrabajaConNosotros);
