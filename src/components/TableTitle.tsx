import React from 'react';

const ModalTitle = (props: any) => (
	<h2 className="title-component">
		{ props.children }
		{ props.right && <div className="title-component-right">
			{ props.right }
		</div> }
	</h2>
)

export default ModalTitle;