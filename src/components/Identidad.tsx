import React, { useState, useEffect } from "react";
import axios from "axios";

import "react-multi-carousel/lib/styles.css";

import { IdentityModel } from "models";
import { handlerError } from "utils";
import { Loading } from "components";
import { IdentityService } from "services";

const Identity = () => {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [identities, setIdentities] = useState<IdentityModel[]>([]);
  const cancelToken = axios.CancelToken;

  useEffect(() => {
    const source = cancelToken.source();

    (async () => {
      try {
        const data = await IdentityService.get(source);
        setIsLoading(false);
        setIdentities(data);
      } catch (err) {
        console.log("Error");
        handlerError(err);
      }

      return () => source.cancel("Cancel by user");
    })();
  }, [cancelToken]);

  const Identities = identities;

  const MyCarousel2 = ({ items }: { items: IdentityModel[] }) => (
    <div className="w-100 h-100 p-0 marginCarouselIdentity m-0">
      <div className="container carouselComponent iden-x m-auto w-100 h-100">
        <div className="row">
          {items.map((item: IdentityModel, key: number) => (
            <div className="col-md-6 text-center" key={key.toString()}>
              <figure className="img-hover-text">
                <img
                  src={item.image_url}
                  alt={item.name}
                  title={item.name}
                  className="w-100 h-100 m-auto"
                />
                <figcaption>
                  <h5 className="poppins-regular text-white">
                    <span className="poppins-bold">{item.name}</span>
                  </h5>
                  <p> {item.description} </p>
                </figcaption>
              </figure>
            </div>
          ))}
        </div>
      </div>
    </div>
  );

  if (isLoading) {
    return <Loading />;
  }

  return (
    <div className="marginCarousel w-100 h-100">
      <MyCarousel2 items={Identities} />
    </div>
  );
};

export default Identity;
