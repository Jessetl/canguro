import React from "react";
import { RouteComponentProps, withRouter } from "react-router";

import { RootState } from "reducers";
import { connect, ConnectedProps } from "react-redux";

import instagram from "../../src/assets/icons/insta-ico.svg";
import { InstagramService } from "services";

const mapState = (state: RootState) => ({
  info: state.contact,
});

const connector = connect(mapState);

type State = {
  linkInstagram: string;
};

const initialState: State = {
  linkInstagram: "",
};

type Props = ConnectedProps<typeof connector> & RouteComponentProps;
class InstaSlider extends React.Component<Props, State> {
  public state: State;

  constructor(props: Props) {
    super(props);
    this.state = initialState;
  }

  componentDidMount() {
    InstagramService.getAll().then((res) => {
      console.log(res);
    });

    if (this.props.info) {
      const { info } = this.props;

      this.setState((prevState: any) => {
        return {
          ...prevState,
          linkInstagram: info.instagram,
        };
      });
    }
  }

  render() {
    const { linkInstagram } = this.state;

    return (
      <section className="insta">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <p>Sigue Nuestra Cuenta</p>
              <a href={linkInstagram} target="_blank" rel="noopener noreferrer">
                <h2 className="poppins-semibold">
                  <img
                    className="insta-ico mr-1"
                    alt="instagram"
                    src={instagram}
                  />
                  Instagram
                </h2>
              </a>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default withRouter(connector(InstaSlider));
