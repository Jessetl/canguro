import React from "react";
import { registerLocale, setDefaultLocale } from "react-datepicker";
import DatePicker from "react-datepicker";
import { CustomProps } from "models";
import Icon from "./icon";

import es from "date-fns/locale/es";

registerLocale("es", es);
setDefaultLocale("es");

interface DateProps {
  required?: boolean;
  label?: string;
  color?: string | "";
  value?: Date;
  name: string;
  minDate?: Date;
  maxDate?: Date;
  onChange(
    date: Date | null,
    event: React.SyntheticEvent<any> | undefined
  ): void;
}

const CustomInput = React.forwardRef<HTMLDivElement, CustomProps>(
  ({ color, value, onClick }, ref) => (
    <div
      ref={ref}
      className={`container-datepicker ${color ? color : ""}`}
      onClick={onClick}
    >
      <p>{value}</p>
      <Icon name="calendar" />
    </div>
  )
);

const _DatePicker: React.FC<DateProps> = ({
  label,
  required = false,
  color,
  minDate = null,
  maxDate = null,
  value,
  name,
  onChange,
}) => (
  <div className="form-group">
    {label && (
      <label className="label-datepicker">
        {label} {required ? <span className="text-danger">(*)</span> : ""}
      </label>
    )}
    <DatePicker
      name={name}
      showMonthDropdown
      showYearDropdown
      minDate={minDate}
      maxDate={maxDate}
      selected={value}
      onChange={onChange}
      dateFormat="dd/MM/yyyy"
      customInput={<CustomInput value={value} color={color} />}
      popperPlacement="botom-start"
    />
  </div>
);

export default _DatePicker;
