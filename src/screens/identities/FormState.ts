import { IdentityForm, IdentityModel } from "models";

export type State = {
  isSubmitted: boolean;
  error: string;
  form: IdentityForm;
  uploaded: boolean;
};

export const initialState: State = {
  isSubmitted: false,
  error: "",
  form: {
    name: "",
    description: "",
    image: "",
  },
  uploaded: false,
};

export type IdentityProps = {
  identity: IdentityModel;
  onEdit: (id: number) => void;
  onDelete: (id: number) => void;
  onToggleViewImage: (url: string) => void;
};
