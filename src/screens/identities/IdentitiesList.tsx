import React, { FunctionComponent, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";

import { IdentityProps } from "./FormState";
import { IdentityModel } from "models";
import { Button, Loading, Empty, Pagination, Modal, Icon } from "components";
import { IdentityService } from "services";
import { confirm, handlerError, showSuccess } from "utils";

import editSvg from "assets/icons/edit.svg";
import deleteSvg from "assets/icons/delete.svg";

export default function IdentitiesList() {
  const history = useHistory();
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [identities, setIdentities] = useState<IdentityModel[]>([]);
  const [showModal, setShowModal] = useState<boolean>(false);
  const [page, setPage] = useState<number>(1);
  const [lastPage, setLastPage] = useState<number>(1);
  const [imgurl, setImgurl] = useState<string>("");

  const cancelToken = axios.CancelToken;

  useEffect(() => {
    const source = cancelToken.source();

    (async () => {
      try {
        const { data, last_page } = await IdentityService.getAll(page, source);

        setIsLoading(false);
        setLastPage(last_page);
        setIdentities(data);
      } catch (err) {
        handlerError(err);
      }

      return () => source.cancel("Cancel by user");
    })();
  }, [page, cancelToken]);

  const onDelete = async (identityId: number) => {
    const hasDelete = await confirm({
      text: "¿Desea eliminar esta identidad?",
    });

    if (!hasDelete) {
      return;
    }

    try {
      await IdentityService.delete(identityId);
      setIdentities(identities.filter(({ id }) => id !== identityId));
      showSuccess();
    } catch (error) {
      handlerError(error);
    }
  };

  const onChangePage = async (page: number) => {
    setIsLoading(true);

    const source = cancelToken.source();

    try {
      const { data, last_page } = await IdentityService.getAll(page, source);

      setIsLoading(false);
      setPage(page);
      setLastPage(last_page);
      setIdentities(data);
    } catch (err) {
      handlerError(err);
    }
  };

  const onToggleViewImage = (url: string) => {
    setImgurl(url);
    setShowModal(true);
  };

  const toCreate = () => history.push("/admin/identities/create");
  const toEdit = (id: number) => history.push(`/admin/identities/${id}/edit`);

  if (isLoading) {
    return <Loading />;
  }

  return (
    <div className="container">
      {showModal && (
        <Modal
          className="d-flex align-items-center"
          onClose={() => setShowModal(false)}
          visible
          header={false}
        >
          {!!imgurl && (
            <div style={{ maxHeight: 300, overflowY: "auto" }}>
              <img src={imgurl} alt="Voucher" className="img-fluid" />
            </div>
          )}
          {!imgurl && <label>No se ha cargado ninguna imagen</label>}
        </Modal>
      )}

      <div className="row mb-3">
        <div className="col col-sm-12 col-md-12 col-lg-12 col-xl-12">
          <h2 className="text-black my-3">Identidades canguro</h2>
        </div>
      </div>

      <div className="row mb-3">
        <div className="col col-sm-12 col-md-12 col-lg-12 col-xl-12">
          <Button
            color="orange"
            title="Crear Sede"
            className="float-right text-dark"
            onClick={toCreate}
          >
            Crear Nuevo
          </Button>
        </div>
      </div>

      <div className="row mx-0 rounded-lg shadow-sm bg-white mb-3">
        <div className="col-md-4 px-2 py-2 rounded-left font-bold border border-right-0 d-flex align-items-center">
          Nombre
        </div>
        <div className="col-md-4 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Descripción
        </div>
        <div className="col-md-4 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Acciones
        </div>
      </div>

      {identities.map((row: IdentityModel) => (
        <RowVenues
          key={row.id}
          identity={row}
          onEdit={(id: number) => toEdit(id)}
          onDelete={onDelete}
          onToggleViewImage={onToggleViewImage}
        />
      ))}

      {identities.length === 0 && <Empty />}

      <div className="row my-3">
        <div className="col-md">
          <Pagination pages={lastPage} active={page} onChange={onChangePage} />
        </div>
      </div>
    </div>
  );
}

const RowVenues: FunctionComponent<IdentityProps> = (props) => {
  const { identity, onEdit, onDelete, onToggleViewImage } = props;

  return (
    <div className="row mx-0 rounded-lg shadow-sm bg-white mb-3">
      <div className="col-md-4 px-2 py-2 bg-gray rounded-left font-bold d-flex align-items-center">
        {identity.name}
      </div>
      <div className="col-md-4 px-2 py-2 o-x bg-gray font-bold d-flex align-items-center">
        <p>{identity.description}</p>
      </div>
      <div className="col-md-4 px-2 py-2 text-center border border-right-0 d-flex align-items-center justify-content-center">
        <Button
          color="dark"
          title="Ver imagen"
          small
          className="rounded rounded-circle"
          onClick={(e: React.FormEvent<HTMLButtonElement>): void => {
            onToggleViewImage(identity.image_url);
          }}
        >
          <Icon name="eye" color="white" />
        </Button>
        <Button
          color="dark"
          title="Editar"
          small
          className="rounded rounded-circle"
          onClick={(_: React.FormEvent<HTMLButtonElement>): void =>
            onEdit(identity.id)
          }
        >
          <img src={editSvg} alt="Editar" />
        </Button>
        <Button
          color="dark"
          title="Eliminar"
          small
          className="rounded rounded-circle"
          onClick={(_: React.FormEvent<HTMLButtonElement>): void =>
            onDelete(identity.id)
          }
        >
          <img src={deleteSvg} alt="Eliminar" />
        </Button>
      </div>
    </div>
  );
};
