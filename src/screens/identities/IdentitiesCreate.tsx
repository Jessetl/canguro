import React from "react";
import { RouteComponentProps } from "react-router-dom";
import axios, { CancelTokenSource } from "axios";

import { initialState, State } from "./FormState";
import { IdentityService } from "services";
import { Button, PageTitle, Submitted, Image, Icon } from "components";
import { Input, Textarea } from "components";
import { handlerError, showSuccess } from "utils";

type Props = RouteComponentProps;

class IdentitiesCreate extends React.Component<Props, State> {
  public state: State;
  public source: CancelTokenSource;

  constructor(props: Props) {
    super(props);

    this.state = initialState;

    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();
  }

  componentWillUnmount() {
    this.source.cancel("Cancel by user");
  }

  handleSubmit = async (
    event: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    const { isSubmitted, form } = this.state;

    event.preventDefault();

    if (!!!isSubmitted) {
      this.setState({ isSubmitted: true });

      try {
        await IdentityService.store(form);

        this.setState({ isSubmitted: false });
        showSuccess();

        this.props.history.push("/admin/identities");
      } catch (error) {
        this.setState({ isSubmitted: false });
        handlerError(error);
      }
    }
  };

  handleChangeSelect = (key: string) => {
    return (event: React.ChangeEvent<HTMLSelectElement>): void => {
      const { value, name } = event.currentTarget;
      console.log("handleChangeSelect: key:", key, name);

      this.setState((prevState: any) => {
        return {
          ...prevState,
          [key]: {
            ...prevState[key],
            [name]: value,
          },
        };
      });
    };
  };

  handleChange = (key: string) => {
    return (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value, name } = event.currentTarget;
      console.log("handleChange: key:", key, name);

      this.setState((prevState: any) => {
        return {
          ...prevState,
          [key]: {
            ...prevState[key],
            [name]: value,
          },
        };
      });
    };
  };

  handleChangeFile = (key: string) => {
    return async (event: React.ChangeEvent<HTMLInputElement>) => {
      const { name } = event.target;

      if (event.currentTarget.files) {
        const base64 = await convertBase64(event.currentTarget.files[0]);

        this.setState((prevState: any) => {
          return {
            ...prevState,
            [key]: {
              ...prevState[key],
              [name]: base64,
            },
            uploaded: true,
          };
        });
      }
    };
  };

  render() {
    const { isSubmitted, form, uploaded } = this.state;

    return (
      <div className="container">
        <PageTitle backUrl={`/admin/identities`}>
          Registro de identidad
        </PageTitle>
        <div className="row justify-content-center mt-5">
          <div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
            <div className="card">
              <div className="card-body">
                <form
                  onSubmit={this.handleSubmit}
                  noValidate
                  autoComplete="off"
                >
                  <Input
                    label="Nombre"
                    name="name"
                    color="grey"
                    onChange={this.handleChange("form")}
                    value={form.name}
                  />
                  <Textarea
                    name="description"
                    label="Descripción"
                    color="grey"
                    onChange={this.handleChange("form")}
                    value={form.description}
                  />

                  <div className="row zoneMVCreate text-center my-2">
                    {!uploaded && (
                      <div className="col-12 col-sm-12 col-md-12">
                        <div className="text-center">
                          <label className="upload">
                            <input
                              type="file"
                              onChange={this.handleChangeFile("form")}
                              accept="image/png, image/jpeg"
                              name="image"
                            />
                          </label>
                        </div>
                      </div>
                    )}

                    {uploaded && (
                      <div className="col-12 col-sm-12 col-md-12 mb-3 text-center">
                        <Image
                          name="file"
                          source={form.image}
                          width="200"
                          height="150"
                          alt="img"
                          style={{ marginBottom: 7 }}
                        />
                        <Button
                          type="button"
                          color="orange"
                          title="Quitar"
                          small
                          className="rounded rounded-circle mt-2 mr-1"
                          onClick={() =>
                            this.setState((prevState: any) => {
                              return {
                                form: {
                                  ...prevState.form,
                                  url: "",
                                },
                                uploaded: false,
                              };
                            })
                          }
                        >
                          <Icon name="remove" color="white" />
                        </Button>
                      </div>
                    )}
                  </div>
                  <div className="row">
                    <div className="col my-2">
                      <p className="text-danger">
                        Se recomienda que las imágenes tengan un tamaño de
                        625x625
                      </p>
                    </div>
                  </div>

                  {isSubmitted ? (
                    <Submitted />
                  ) : (
                    <Button color="orange" type="submit" block>
                      Guardar
                    </Button>
                  )}
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const convertBase64 = (file: any) => {
  return new Promise((resolve, reject) => {
    const fileReader: any = new FileReader();

    fileReader.onload = () => {
      resolve(fileReader.result);
    };

    fileReader.onerror = (error: string) => {
      reject(error);
    };

    fileReader.readAsDataURL(file);
  });
};

export default IdentitiesCreate;
