import React, { ReactNode } from "react";
import { RouteComponentProps } from "react-router-dom";
import axios, { CancelTokenSource } from "axios";

import { ContactModel } from "models";

import { initialState, State } from "./FormState";
import { handlerError, showSuccess } from "utils";
import ContactService from "services/modules/contactService";
// import { TitleModal } from "components";
import { Button, Input, Textarea } from "components";

type Props = RouteComponentProps;

class ContactCreate extends React.PureComponent<Props, State> {
  public state: State;
  public source: CancelTokenSource;

  constructor(props: Props) {
    super(props);

    this.state = initialState;

    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();
  }

  componentDidMount() {
    this.getContact();
  }

  getContact = () => {
    ContactService.getAll()
      .then((contact) => {
        if (contact.length > 0) {
          this.setState((prevState: any) => {
            return {
              ...prevState,
              id: contact[0].id,
              email: contact[0].email,
              twitter: contact[0].twitter,
              instagram: contact[0].instagram,
              youtube: contact[0].youtube,
              facebook: contact[0].facebook,
              whatsapp: contact[0].whatsapp,
              link_app_android: contact[0].link_app_android,
              link_app_ios: contact[0].link_app_ios,
              schedule: contact[0].horario,
              tlf: contact[0].tlf,
              deliveries_phones: contact[0].deliveries_phones,
              address: contact[0].address,
            };
          });
        }
        console.log(this.state);
      })
      .catch((err) => handlerError(err));
  };

  handleChange = (key: string) => {
    return (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value } = event.currentTarget;
      this.setState((prevState: any) => {
        return {
          ...prevState,
          [key]: value,
        };
      });
    };
  };

  handleChangePhone = (key: number, event: any) => {
    const { deliveries_phones } = this.state;
    let rows = deliveries_phones;

    const { value } = event.currentTarget;
    for (let i = 0; i < rows.length; i++) {
      if (i === key) {
        rows[i].name = value;
      }
    }
    this.setState((prevState: any) => {
      return {
        ...prevState,
        deliveries_phones: [...rows],
      };
    });
  };

  handleChangeSede = (key: number, event: any, attr: string) => {
    const { sedes } = this.state;
    let rows = sedes;

    const { value } = event.currentTarget;
    for (let i = 0; i < rows.length; i++) {
      if (i === key) {
        rows[i] = {
          ...rows[i],
          [attr]: value,
        };
      }
    }
    this.setState((prevState: any) => {
      return {
        ...prevState,
        sedes: [...rows],
      };
    });
    console.log(this.state);
  };

  onAddPhones = () => {
    const { deliveries_phones } = this.state;
    let rows = deliveries_phones;

    rows.push({
      id: 0,
      name: "",
      contact_id: 0,
    });

    this.setState((prevState: any) => {
      return {
        ...prevState,
        deliveries_phones: [...rows],
      };
    });
  };

  deletePhones = (key: number) => {
    const { deliveries_phones } = this.state;
    let rows = deliveries_phones;
    rows = rows.splice(key, 1);

    this.setState((prevState: any) => {
      return {
        ...prevState,
        deliveries_phones: [...rows],
      };
    });
  };

  /*deleteSedes = (key: number) => {
    const { sedes } = this.state;
    let rows = sedes;
    rows = rows.splice(key, 1);

    this.setState((prevState: any) => {
      return {
        ...prevState,
        sedes: [...rows],
      };
    });
  }; */

  /*onAddSedes = () => {
    const { sedes } = this.state;
    let rows = sedes;
    rows.push({
      id: 0,
      name: "",
      address: "",
      image: "",
      phone: "",
      web_url: "",
      ig_url: "",
    }); 

    this.setState((prevState: any) => {
      return {
        ...prevState,
        sedes: [...rows],
      };
    });
  }; */

  handleSubmit = async (
    event: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    event.preventDefault();
    const { id, schedule, tlf } = this.state;

    if (!!!this.state.isSubmit) {
      this.setState({
        isSubmit: true,
      });

      const formContact: ContactModel = {
        ...this.state,
        horario: schedule,
        tlf: tlf,
      };

      if (!!id) {
        ContactService.update(id, formContact)
          .then((_) => {
            showSuccess();
          })
          .catch(handlerError)
          .finally(() => {
            this.setState({ isSubmit: false });
          });
      } else {
        ContactService.store(formContact, this.source)
          .then(() => {
            showSuccess();
          })
          .catch(handlerError)
          .finally(() => this.setState({ isSubmit: false }));
      }
    }
  };

  render(): ReactNode {
    const { isSubmit, email, twitter, instagram, youtube } = this.state;
    const { tlf, schedule, facebook, whatsapp, address } = this.state;
    const { link_app_android, link_app_ios } = this.state;

    return (
      <div className="container">
        <div className="row mb-3">
          <div className="col col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <h2 className="text-black text-center my-3">Contacto</h2>
          </div>
        </div>

        <div className="row justify-content-center">
          <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-10">
            <div className="card">
              <div className="card-body">
                <form
                  onSubmit={this.handleSubmit}
                  noValidate
                  autoComplete="off"
                >
                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        label="Tiktok"
                        name="Tiktok"
                        color="gray"
                        type="text"
                        value={twitter}
                        onChange={this.handleChange("twitter")}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        label={"Instagram"}
                        name={"instagram"}
                        color="gray"
                        type="text"
                        value={instagram}
                        onChange={this.handleChange("instagram")}
                      />
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        label={"Youtube"}
                        name={"youtube"}
                        color="gray"
                        type="text"
                        value={youtube}
                        onChange={this.handleChange("youtube")}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        label={"Facebook"}
                        name={"facebook"}
                        color="gray"
                        type="text"
                        value={facebook}
                        onChange={this.handleChange("facebook")}
                      />
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        label={"Whatsapp"}
                        name={"whatsapp"}
                        color="gray"
                        type="text"
                        value={whatsapp}
                        onChange={this.handleChange("whatsapp")}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        label={"Email"}
                        name={"email"}
                        color="gray"
                        type="text"
                        value={email}
                        onChange={this.handleChange("email")}
                      />
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        label={"Enlace de aplicación móvil android"}
                        name={"link_app_android"}
                        color="gray"
                        type="text"
                        value={link_app_android}
                        onChange={this.handleChange("link_app_android")}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        label={"Enlace de aplicación móvil ios"}
                        name={"link_app_ios"}
                        color="gray"
                        type="text"
                        value={link_app_ios}
                        onChange={this.handleChange("link_app_ios")}
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        label={"Teléfono"}
                        name={"tlf"}
                        color="gray"
                        type="number"
                        value={tlf}
                        onChange={this.handleChange("tlf")}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        label={"Horario"}
                        name={"schedule"}
                        color="gray"
                        type="text"
                        value={schedule}
                        onChange={this.handleChange("schedule")}
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <Textarea
                        label="Dirección"
                        name="address"
                        value={address}
                        onChange={this.handleChange("address")}
                        className="poppins-regular"
                      />
                    </div>
                  </div>
                  {/* <hr />
                  <div className="row text-center">
                    <div className="col col-sm">
                      <h4 className="poppins-regular">Número de delivery</h4>
                    </div>
                  </div>
                  {!!deliveries_phones && deliveries_phones.length > 0 && (
                    <React.Fragment>
                      {deliveries_phones.map((row, key) => {
                        return (
                          <div className="row" key={key.toString()}>
                            <div className="col-12 col-sm-12 col-md">
                              <Input
                                name={"phone"}
                                color="gray"
                                type="number"
                                value={row.name}
                                onChange={(value) =>
                                  this.handleChangePhone(key, value)
                                }
                              />
                            </div>
                            <div className="col col-sm">
                              <Button
                                type="button"
                                color="red"
                                className="btn-danger"
                                onClick={() => this.deletePhones(key)}
                              >
                                <Icon name="trash"></Icon>
                              </Button>
                            </div>
                          </div>
                        );
                      })}
                    </React.Fragment>
                  )} */}

                  {/* <div className="text-center row">
                    <div className="col col-sm">
                      <Button
                        small={true}
                        type="button"
                        className="btn-orange shadow text-black-important poppins-regular"
                        onClick={() => this.onAddPhones()}
                      >
                        Agregar número
                      </Button>
                    </div>
                  </div>
                  <hr /> */}

                  {/* <br />
                  <div className="row text-center">
                    <div className="col col-sm">
                      <h4>Sedes</h4>
                    </div>
                  </div> */}
                  {/* {!!sedes && sedes.length > 0 && (
                    <React.Fragment>
                      {sedes.map((row, key) => {
                        return (
                          <div className="row" key={key.toString()}>
                            <div className="col-12 col-sm-12 col-md">
                              <Input
                                label={"Sede"}
                                name={"sede"}
                                color="gray"
                                type="text"
                                value={row.name}
                                onChange={(value) =>
                                  this.handleChangeSede(key, value, "name")
                                }
                              />
                            </div>
                            <div className="col-12 col-sm-12 col-md">
                              <Input
                                label={"Dirección"}
                                name={"address"}
                                color="gray"
                                type="text"
                                value={row.address}
                                onChange={(value) =>
                                  this.handleChangeSede(key, value, "address")
                                }
                              />
                            </div>
                            <div
                              className="col-2 col-sm-2"
                              style={{ marginTop: 3.3 + "%" }}
                            >
                              <Button
                                type="button"
                                color="red"
                                className="btn-danger"
                                onClick={() => this.deleteSedes(key)}
                              >
                                <Icon name="trash"></Icon>
                              </Button>
                            </div>
                          </div>
                        );
                      })}
                    </React.Fragment>
                  )}

                  <div className="text-center row">
                    <div className="col col-sm">
                      <Button
                        small={true}
                        type="button"
                        className="btn-orange shadow text-black-important poppins-regular"
                        onClick={() => this.onAddSedes()}
                      >
                        Agregar sede
                      </Button>
                    </div>
                  </div> */}
                  <br />
                  <div className="text-center row">
                    <div className="col col-sm">
                      <Button
                        submitted={isSubmit}
                        block
                        type="submit"
                        className="btn-orange shadow text-black-important poppins-regular"
                      >
                        Guardar
                      </Button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ContactCreate;
