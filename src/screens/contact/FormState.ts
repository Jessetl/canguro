import { DeliveryPhone, VenueModel } from "models";

export type State = {
  isSubmit: boolean;
  id: number;
  email: string;
  tlf: string;
  twitter: string;
  youtube: string;
  instagram: string;
  facebook: string;
  whatsapp: string;
  address: string;
  error: string;
  link_app_android: string;
  link_app_ios: string;
  schedule: string;
  deliveries_phones: DeliveryPhone[];
  sedes: VenueModel[];
};

export const initialState: State = {
  isSubmit: false,
  twitter: "",
  id: 0,
  email: "",
  tlf: "",
  error: "",
  youtube: "",
  instagram: "",
  facebook: "",
  whatsapp: "",
  address: "",
  link_app_android: "",
  link_app_ios: "",
  schedule: "",
  deliveries_phones: [],
  sedes: [],
};
