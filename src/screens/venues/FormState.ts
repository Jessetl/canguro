import { StateModel, VenueForm, VenueModel } from "models";
import { CountryModel } from "models/countryModel";

export type State = {
  isSubmitted: boolean;
  error: string;
  form: VenueForm;
  states: StateModel[];
  countries: CountryModel[];
};

export const initialState: State = {
  isSubmitted: false,
  error: "",
  form: {
    name: "",
    address: "",
    image: "",
    phone: "",
    web_url: "",
    ig_url: "",
    country_id: "",
    state_id: "",
    type: "",
    image_url: "",
  },
  states: [],
  countries: [],
};

export type VenueProps = {
  venue: VenueModel;
  onEdit: (id: number) => void;
  onDelete: (id: number) => void;
};
