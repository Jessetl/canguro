import React, { useState } from "react";
import { Card, Button, Select, TitleModal, MultipleText } from "components";

const VenuesVenezuela = () => {
  const [files, setFiles] = useState();

  const handler = (e: React.FormEvent<HTMLFormElement>): void => {
    e.preventDefault();
  };

  const uploadSingleFile = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.currentTarget.files) {
      var reader = new FileReader();

      reader.onload = (event: any) => {
        setFiles(event.target.result);
      };

      reader.readAsDataURL(e.currentTarget.files[0]);
    }
  };

  //estados traidos de la bd
  const options = [{ value: 3, label: "Blues" }];

  let imagePrint = null;

  if (files) {
    imagePrint = (
      <img
        src={files}
        className="d-block rounded imageVenues position-static"
      />
    );
  }

  return (
    <div className="col-10 pt-4 m-auto justify-content-center">
      <Card className="border-orange">
        <form onSubmit={handler} noValidate autoComplete="off" className="mt-4">
          <TitleModal className="titleVenue">Canguro en Venezuela</TitleModal>
          <div className="col-5 pl-0 pr-0 ml-auto mb-2">
            <Button
              className="font-bold textVenue button-rounded"
              color="orange"
              block
              type="submit"
            >
              Agregar Nuevo +
            </Button>
          </div>

          <Card className="border-orange">
            <div className="row">
              <div className="col-4">
                <div className="text-center">
                  <label htmlFor="photo" className="border rounded">
                    {files ? (
                      ""
                    ) : (
                      <p className="imageTextVenues">Upload Image</p>
                    )}
                    {imagePrint}
                  </label>
                </div>
                <input
                  type="file"
                  id="photo"
                  name="photo"
                  accept="image/png, image/jpeg"
                  color="orange"
                  onChange={uploadSingleFile}
                  hidden
                />
              </div>
              <div className="col-4 text-center">
                {/*opciones de estados */}
                <Select options={[{ value: 1, label: "Aragua" }]} />

                <div className="mt-5 fontVenue">
                  Direcciones
                  <MultipleText
                    color="white"
                    name="direcciones"
                    type="text"
                    placeholder="Escribir dirección"
                    addButton="Nueva Dirección"
                    className="textVenue"
                  />
                </div>
              </div>
              <div className="col-4 text-center">
                <div className="fontVenue marginVenues">
                  Telefonos
                  <MultipleText
                    color="white"
                    name="direcciones"
                    type="text"
                    placeholder="Escribir teléfono"
                    addButton="Nuevo Teléfono"
                  />
                </div>
              </div>
            </div>
          </Card>
        </form>
      </Card>
    </div>
  );
};

export default VenuesVenezuela;
