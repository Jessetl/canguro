import React, { FunctionComponent, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import axios, { CancelTokenSource } from "axios";

import { VenueProps } from "./FormState";
import { VenueModel } from "models";
import { Button, Loading, Empty, Pagination } from "components";
import { VenueService } from "services";
import { confirm, handlerError, showSuccess } from "utils";

import editSvg from "assets/icons/edit.svg";
import deleteSvg from "assets/icons/delete.svg";

export default function VenuesList() {
  const history = useHistory();
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [page, setPage] = useState<number>(1);
  const [lastPage, setLastPage] = useState<number>(1);
  const [venues, setVenues] = useState<VenueModel[]>([]);

  const cancelToken = axios.CancelToken;

  useEffect(() => {
    const source = cancelToken.source();

    (async () => {
      getAll(source);
      return () => source.cancel("Cancel by user");
    })();
  }, [page, cancelToken]); // eslint-disable-line react-hooks/exhaustive-deps

  const getAll = async (source: CancelTokenSource) => {
    try {
      const { data, last_page, to } = await VenueService.getAll(page, source);

      setLastPage(last_page);
      setVenues(data);

      if (!to && page > 1) {
        setPage(page - 1);
      }

      setIsLoading(false);
    } catch (err) {
      setIsLoading(false);
      handlerError(err);
    }
  };

  const onDelete = async (venueId: number) => {
    const hasDelete = await confirm({ text: "¿Desea eliminar esta sede?" });

    if (!!hasDelete) {
      const source = cancelToken.source();

      await VenueService.delete(venueId);

      showSuccess();
      getAll(source);
    }
  };

  const onChangePage = async (page: number) => {
    setIsLoading(true);

    const source = cancelToken.source();

    try {
      const { data, last_page } = await VenueService.getAll(page, source);

      setIsLoading(false);
      setPage(page);
      setLastPage(last_page);
      setVenues(data);
    } catch (err) {
      handlerError(err);
    }
  };

  const toCreate = () => history.push("/admin/venues/create");

  const toEdit = (id: number) => history.push(`/admin/venues/${id}/edit`);

  if (isLoading) {
    return <Loading />;
  }

  return (
    <div className="container">
      <div className="row mb-3">
        <div className="col col-sm-12 col-md-12 col-lg-12 col-xl-12">
          <h2 className="text-black my-3">Sedes</h2>
        </div>
      </div>

      <div className="row mb-3">
        <div className="col col-sm-12 col-md-12 col-lg-12 col-xl-12">
          <Button
            color="orange"
            title="Crear Sede"
            className="float-right text-dark"
            onClick={toCreate}
          >
            Crear Nuevo
          </Button>
        </div>
      </div>

      <div className="row mx-0 rounded-lg shadow-sm bg-white mb-3">
        <div className="col-md-2 px-2 py-2 rounded-left font-bold border border-right-0 d-flex align-items-center">
          Nombre
        </div>
        <div className="col-md-2 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Tipo
        </div>
        <div className="col-md-2 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Dirección
        </div>
        <div className="col-md-2 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Teléfono
        </div>
        <div className="col-md-2 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          País/Estado
        </div>
        <div className="col-md-2 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Acciones
        </div>
      </div>

      {venues.map((row: VenueModel) => (
        <RowVenues
          key={row.id}
          venue={row}
          onEdit={(id: number) => toEdit(id)}
          onDelete={onDelete}
        />
      ))}

      {venues.length === 0 && <Empty />}

      <div className="row my-3">
        <div className="col-md">
          <Pagination pages={lastPage} active={page} onChange={onChangePage} />
        </div>
      </div>
    </div>
  );
}

const RowVenues: FunctionComponent<VenueProps> = (props) => {
  const { venue, onEdit, onDelete } = props;

  const name = venue.state?.name || venue.country?.name;
  const typeName = venue.state?.name ? "Local" : "Mundo";

  return (
    <div className="row mx-0 rounded-lg shadow-sm bg-white mb-3">
      <div className="col-md-2 px-2 py-2 bg-gray rounded-left font-bold d-flex align-items-center">
        {venue.name}
      </div>
      <div className="col-md-2 px-2 py-2 bg-gray font-bold d-flex align-items-center">
        {typeName}
      </div>
      <div className="col-md-2 px-2 py-2 bg-gray font-bold d-flex align-items-center">
        {venue.address}
      </div>
      <div className="col-md-2 px-2 py-2 bg-gray font-bold d-flex align-items-center">
        {venue.phone}
      </div>
      <div className="col-md-2 px-2 py-2 bg-gray font-bold d-flex align-items-center">
        {name}
      </div>
      <div className="col-md-2 px-2 py-2 text-center border border-right-0 d-flex align-items-center justify-content-center">
        <Button
          color="dark"
          title="Editar"
          small
          className="rounded rounded-circle mr-1"
          onClick={(event: React.FormEvent<HTMLButtonElement>): void => {
            event.preventDefault();
            onEdit(venue.id);
          }}
        >
          <img src={editSvg} alt="Editar" />
        </Button>
        <Button
          color="dark"
          title="Eliminar"
          small
          className="rounded rounded-circle"
          onClick={(event: React.FormEvent<HTMLButtonElement>): void => {
            event.preventDefault();
            onDelete(venue.id);
          }}
        >
          <img src={deleteSvg} alt="Eliminar" />
        </Button>
      </div>
    </div>
  );
};
