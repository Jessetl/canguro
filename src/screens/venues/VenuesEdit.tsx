import React from "react";
import { RouteComponentProps } from "react-router-dom";
import axios, { CancelTokenSource } from "axios";

import { initialState, State } from "./FormState";
import { VenueForm } from "models";
import { VenueService, StateService } from "services";
import { Button, PageTitle, Submitted, File } from "components";
import { Input, Select, Textarea } from "components";
import { handlerError, showSuccess } from "utils";
import { SELECT_LOCAL, SELECT_MUNDO, TYPE_VENUES } from "utils";

interface MatchParams {
  id?: string;
}

type Props = RouteComponentProps<MatchParams>;

class VenueEdit extends React.Component<Props, State> {
  public state: State;
  public source: CancelTokenSource;

  constructor(props: Props) {
    super(props);

    this.state = initialState;

    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();
  }

  componentDidMount() {
    this.load();
  }

  componentWillUnmount() {
    this.source.cancel("Cancel by user");
  }

  load = async () => {
    const { id } = this.props.match.params;

    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();

    try {
      const data = await StateService.getCountryAndState(this.source);

      this.setState(
        {
          countries: data.countries,
          states: data.states,
        },
        () => {
          if (!!id) {
            this.getVenueData();
          }
        }
      );
    } catch (error) {
      handlerError(error);
    }
  };

  getVenueData = async () => {
    const { id } = this.props.match.params;

    if (!!id) {
      try {
        const venue = await VenueService.getById(parseInt(id), this.source);

        if (!!venue) {
          const parseVenue: VenueForm = {
            ...venue,
            type: !!venue.state_id ? SELECT_LOCAL : SELECT_MUNDO,
            state_id: venue.state_id || "",
            country_id: venue.country_id || "",
          };

          console.log("venue", parseVenue);

          this.setState({
            form: parseVenue,
          });
        }
      } catch (err) {
        handlerError(err);
      }
    }
  };

  handleSubmit = async (
    event: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    const { isSubmitted, form } = this.state;

    event.preventDefault();

    if (!!!isSubmitted) {
      this.setState({ isSubmitted: true });

      try {
        await VenueService.update(form);

        this.setState({ isSubmitted: false });
        showSuccess();

        this.props.history.push("/admin/venues");
      } catch (error) {
        this.setState({ isSubmitted: false });
        handlerError(error);
      }
    }
  };

  handleChangeSelect = (key: string) => {
    return (event: React.ChangeEvent<HTMLSelectElement>): void => {
      const { value, name } = event.currentTarget;
      console.log("handleChangeSelect: key:", key, name);

      const parseValue = typeof value === "string" ? parseInt(value) : value;

      this.setState((prevState: any) => {
        return {
          ...prevState,
          [key]: {
            ...prevState[key],
            [name]: parseValue || "",
          },
        };
      });
    };
  };

  handleChange = (key: string) => {
    return (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value, name } = event.currentTarget;
      console.log("handleChange: key:", key, name);

      this.setState((prevState: any) => {
        return {
          ...prevState,
          [key]: {
            ...prevState[key],
            [name]: value,
          },
        };
      });
    };
  };

  handleChangeFile = (key: string) => {
    return (event: React.ChangeEvent<HTMLInputElement>) => {
      const { name, value } = event.target;

      this.setState((prevState: any) => {
        return {
          ...prevState,
          [key]: {
            ...prevState[key],
            [name]: value,
          },
        };
      });
    };
  };

  render() {
    const { isSubmitted, form, states, countries } = this.state;

    return (
      <div className="container">
        <PageTitle backUrl={`/admin/venues`}>Editar Sede</PageTitle>
        <div className="row justify-content-center mt-5">
          <div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
            <div className="card">
              <div className="card-body">
                <form
                  onSubmit={this.handleSubmit}
                  noValidate
                  autoComplete="off"
                >
                  <Input
                    label="Nombre"
                    name="name"
                    color="grey"
                    onChange={this.handleChange("form")}
                    value={form.name}
                  />
                  <Input
                    label="Teléfono"
                    type="number"
                    name="phone"
                    color="grey"
                    onChange={this.handleChange("form")}
                    value={form.phone}
                  />
                  <Textarea
                    name="address"
                    label="Dirección"
                    color="grey"
                    onChange={this.handleChange("form")}
                    value={form.address}
                  />

                  <Select
                    label="Tipo"
                    options={TYPE_VENUES.map(({ value, label }) => ({
                      value: value,
                      label: label,
                    }))}
                    name="type"
                    onChange={this.handleChangeSelect("form")}
                    valueSelect={form.type}
                  />

                  {form.type === SELECT_LOCAL && (
                    <Select
                      label="Estado"
                      options={states.map(({ id, name }) => ({
                        value: id,
                        label: name,
                      }))}
                      name="state_id"
                      onChange={this.handleChangeSelect("form")}
                      valueSelect={form.state_id}
                    />
                  )}

                  {form.type === SELECT_MUNDO && (
                    <Select
                      label="País"
                      options={countries.map(({ id, name }) => ({
                        value: id,
                        label: name,
                      }))}
                      name="country_id"
                      onChange={this.handleChangeSelect("form")}
                      valueSelect={form.country_id}
                    />
                  )}

                  <Input
                    label="Instagram"
                    name="ig_url"
                    color="grey"
                    onChange={this.handleChange("form")}
                    value={form.ig_url}
                  />
                  <Input
                    label="Sitio web"
                    name="web_url"
                    color="grey"
                    onChange={this.handleChange("form")}
                    value={form.web_url}
                  />

                  <label>Imagen</label>
                  <File
                    placeholder={
                      form.image ? "Cambiar imagen" : "Agregar Imagen"
                    }
                    placeholderSuccess="Imagen Agregada"
                    showCheck={true}
                    onChange={this.handleChangeFile("form")}
                    name="image"
                    value={form.image}
                    inputStyle={{
                      display: "contents",
                    }}
                    className="btn-product"
                  />

                  {isSubmitted ? (
                    <Submitted />
                  ) : (
                    <Button color="orange" type="submit" block>
                      Guardar
                    </Button>
                  )}
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default VenueEdit;
