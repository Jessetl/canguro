import React, { ReactNode } from "react";
import axios, { CancelTokenSource } from "axios";

import { RouteComponentProps, withRouter } from "react-router";

import { Textarea, Button } from "components";
import { TitleModal, PageTitle } from "components";
import { handlerError, showSuccess } from "utils";
import { initialState, State } from "./FormState";

import { UsModel } from "models";
import UsService from "services/modules/usService";

import { WarrantyModel } from "models";
import WarrantyService from "services/modules/warrantyService";

type Props = RouteComponentProps;

class WarrantyCreate extends React.PureComponent<Props, State> {
  public state: State;
  public source: CancelTokenSource;

  constructor(props: Props) {
    super(props);

    this.state = initialState;

    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();
  }

  componentDidMount() {
    this.getWarranty();
  }

  getWarranty = () => {
    WarrantyService.index()
      .then((warranty) => {
        if (warranty.length > 0) {
          this.setState((prevState: any) => {
            return {
              ...prevState,
              id: warranty[0].id,
              politics: warranty[0].politics,
              warranty: warranty[0].warranty,
              shipments: warranty[0].shipments,
              terms_and_conditions: warranty[0].terms_and_conditions,
            };
          });
        }
      })
      .catch((err) => handlerError(err));
  };

  handleChange = (key: string) => {
    return (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value, name } = event.currentTarget;
      this.setState((prevState: any) => {
        return {
          ...prevState,
          [key]: value,
        };
      });
    };
  };

  handleSubmit = async (
    event: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    event.preventDefault();
    const {
      politics,
      warranty,
      terms_and_conditions,
      shipments,
      id,
    } = this.state;
    if (this.state.isSubmit) {
      return;
    }

    this.setState({
      isSubmit: true,
    });

    const wr: WarrantyModel = {
      id: 0,
      politics,
      warranty,
      terms_and_conditions,
      shipments,
    };

    if (id > 0) {
      WarrantyService.update(id, wr)
        .then((wr) => {
          showSuccess();
          setTimeout(() => {
            this.props.history.push("/admin/warranty");
          }, 1000);
        })
        .catch(handlerError)
        .finally(() => {
          this.setState({ isSubmit: false });
        });
    } else {
      WarrantyService.store(wr, this.source)
        .then(() => {
          showSuccess();

          setTimeout(() => {
            this.props.history.push("/admin/warranty");
          }, 1000);
        })
        .catch(handlerError)
        .finally(() => this.setState({ isSubmit: false }));
    }
  };

  render(): ReactNode {
    const {
      isSubmit,
      politics,
      warranty,
      terms_and_conditions,
      shipments,
    } = this.state;

    return (
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-12">
            <TitleModal className="pt-4 pb-3 text-center text-black poppins-semibold">
              Garantías
            </TitleModal>
            <div className="card mt-auto mb-auto">
              <div className="card-body">
                <form
                  onSubmit={this.handleSubmit}
                  noValidate
                  autoComplete="off"
                >
                  <div className="row zoneMVCreate mb-4">
                    <div className="col-12 col-sm-12 col-md-6 text-center">
                      <Textarea
                        label={"Términos y condciones"}
                        name={"terms_and_conditions"}
                        value={terms_and_conditions}
                        onChange={this.handleChange("terms_and_conditions")}
                        className="poppins-regular"
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md-6 text-center">
                      <Textarea
                        label={"Garantías"}
                        name={"warranty"}
                        value={warranty}
                        onChange={this.handleChange("warranty")}
                        className="poppins-regular"
                      />
                    </div>
                  </div>

                  <div className="row zoneMVCreate mb-4">
                    <div className="col-12 col-sm-12 col-md-6 text-center">
                      <Textarea
                        label={"Políticas de privacidad"}
                        name={"politics"}
                        value={politics}
                        onChange={this.handleChange("politics")}
                        className="poppins-regular"
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md-6 text-center">
                      <Textarea
                        label={"Envíos"}
                        name={"shipments"}
                        value={shipments}
                        onChange={this.handleChange("shipments")}
                        className="poppins-regular"
                      />
                    </div>
                  </div>

                  <div className="text-center row">
                    <div className="col col-sm">
                      <Button
                        submitted={isSubmit}
                        block
                        type="submit"
                        className="btn-orange shadow text-black-important poppins-regular"
                      >
                        Guardar
                      </Button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(WarrantyCreate);
