
export type State = {
  isSubmit: boolean;
  id: number;
  politics: string;
  warranty: string;
  shipments: string;
  terms_and_conditions: string;
};

export const initialState: State = {
  isSubmit: false,
  id: 0,
  politics: "",
  warranty: "",
  shipments: "",
  terms_and_conditions: "",
};