import React, { ReactNode } from "react";
import { RouteComponentProps } from "react-router-dom";
import { Card, Input, Button } from "components";
import { AuthService } from "services";
import { handlerError } from "functions";

type State = {
  submitted: boolean;
  resText: string;
  form: {
    password: string;
    password_confirmation: string;
  };
};

type Props = RouteComponentProps<{
  token: string;
  id: string;
}>;

class RestorePassword extends React.PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      submitted: false,
      resText: "",
      form: {
        password: "",
        password_confirmation: "",
      },
    };
  }

  handleChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const { value, name } = event.currentTarget;

    this.setState(({ form }) => ({
      form: {
        ...form,
        [name]: value,
      },
    }));
  };

  handleSubmit = (event: React.FormEvent<HTMLFormElement>): void => {
    event.preventDefault();

    const { resText, form, submitted } = this.state;
    const { match } = this.props;

    if (submitted || resText) {
      return;
    }

    AuthService.restorePassword({
      password: form.password,
      password_confirmation: form.password_confirmation,
      token: match.params.token,
      id: match.params.id,
    })
      .then((response: string) => {
        this.setState({ resText: response });
      })
      .catch(handlerError)
      .finally(() => this.setState({ submitted: false }));
  };

  render(): ReactNode {
    const { form, resText } = this.state;
    const { history } = this.props;

    return (
      <main>
        <div className="register-background bg-gray">
          <div className="wrapper-content align-items-center">
            <div className="container">
              <div className="row justify-content-center">
                <Card className="wrapper-register">
                  <form
                    onSubmit={this.handleSubmit}
                    noValidate
                    autoComplete="off"
                  >
                    <div className="text-center row">
                      <div className="col col-md col-lg">
                        <h2 className="text-orange font-weight-bold">
                          Recupera tu contraseña
                        </h2>
                        <p className="text-center mb-4">
                          <small className="text-black">
                            (Los campos con
                            <span className="text-orange"> * </span> son
                            obligatorios.)
                          </small>
                        </p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-12 col-sm-12 col-md">
                        <Input
                          required
                          color="gray"
                          type="password"
                          name="password"
                          label="Contraseña"
                          maxlength={20}
                          onChange={this.handleChange}
                          value={form.password}
                        />
                      </div>
                      <div className="col-12 col-sm-12 col-md">
                        <Input
                          required
                          color="gray"
                          type="password"
                          name="password_confirmation"
                          label="Confirmar contraseña"
                          className="bg-gray"
                          maxlength={20}
                          onChange={this.handleChange}
                          value={form.password_confirmation}
                        />
                      </div>
                    </div>
                    {resText ? (
                      <React.Fragment>
                        <div className="row text-center">
                          <div className="col col-md col-lg">
                            <small className="font-weight-bold text-black">
                              {resText}
                            </small>
                          </div>
                        </div>
                        <div className="row text-center">
                          <div className="col col-md col-lg">
                            <Button
                              className="btn-xl button-rounded"
                              color="orange"
                              shadow="shadow-button"
                              type="button"
                              onClick={() => history.replace("/login")}
                            >
                              Ir a Iniciar sesión
                            </Button>
                          </div>
                        </div>
                      </React.Fragment>
                    ) : (
                      <div className="row text-center">
                        <div className="col col-md col-lg">
                          <Button
                            className="btn-xl button-rounded"
                            color="orange"
                            shadow="shadow-button"
                            type="submit"
                          >
                            Continuar
                          </Button>
                        </div>
                      </div>
                    )}
                  </form>
                </Card>
              </div>
            </div>
          </div>
          <div className="login-background-footer"></div>
        </div>
      </main>
    );
  }
}

export default RestorePassword;
