import { BannerForm, BannerModel } from "models";

export type BannerProps = {
  banner: BannerModel;
  onEdit: (id: number) => void;
  onDelete: (id: number) => void;
  onChangeStatus: (id: number) => void;
  onToggleViewImage: (url: string) => void;
};

export type State = {
  isSubmitted: boolean;
  error: string;
  form: BannerForm;
  uploaded: boolean;
};

export const initialState: State = {
  isSubmitted: false,
  error: "",
  form: {
    name: "",
    type: "",
    url: "",
    image_name: "",
    texto: "",
  },
  uploaded: false,
};
