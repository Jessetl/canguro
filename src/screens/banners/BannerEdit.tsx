import React from "react";
import { RouteComponentProps } from "react-router-dom";
import axios, { CancelTokenSource } from "axios";

import { initialState, State } from "./FormState";
import { BannerForm } from "models";
import { BannerService } from "services";
import { Button, PageTitle, Submitted, Image, Icon } from "components";
import { Input, Select, Textarea } from "components";
import { BANNERS, handlerError, showSuccess } from "utils";

interface MatchParams {
  id?: string;
}

type Props = RouteComponentProps<MatchParams>;

class BannerEdit extends React.Component<Props, State> {
  public state: State;
  public source: CancelTokenSource;

  constructor(props: Props) {
    super(props);

    this.state = initialState;

    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();
  }

  componentDidMount() {
    this.load();
  }

  componentWillUnmount() {
    this.source.cancel("Cancel by user");
  }

  load = async () => {
    const { id } = this.props.match.params;

    if (!!id) {
      try {
        const data = await BannerService.getById(parseInt(id));

        const parseBanner: BannerForm = {
          ...data,
          image_name: "",
        };

        this.setState({
          form: {
            ...parseBanner,
            url: !!parseBanner.url ? data.image_url : "",
          },
          uploaded: !!parseBanner.url,
        });
      } catch (error) {
        handlerError(error);
      }
    }
  };

  handleSubmit = async (
    event: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    const { isSubmitted, form } = this.state;

    event.preventDefault();

    if (!!!isSubmitted) {
      this.setState({ isSubmitted: true });

      try {
        await BannerService.update(form);

        this.setState({ isSubmitted: false });
        showSuccess();

        this.props.history.push("/admin/banners");
      } catch (error) {
        this.setState({ isSubmitted: false });
        handlerError(error);
      }
    }
  };

  handleChangeSelect = (key: string) => {
    return (event: React.ChangeEvent<HTMLSelectElement>): void => {
      const { value, name } = event.currentTarget;
      console.log("handleChangeSelect: key:", key, name);

      this.setState((prevState: any) => {
        return {
          ...prevState,
          [key]: {
            ...prevState[key],
            [name]: value,
          },
        };
      });
    };
  };

  handleChange = (key: string) => {
    return (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value, name } = event.currentTarget;
      console.log("handleChange: key:", key, name);

      this.setState((prevState: any) => {
        return {
          ...prevState,
          [key]: {
            ...prevState[key],
            [name]: value,
          },
        };
      });
    };
  };

  handleChangeFile = (key: string) => {
    return async (event: React.ChangeEvent<HTMLInputElement>) => {
      const { name } = event.target;

      if (event.currentTarget.files) {
        const base64 = await convertBase64(event.currentTarget.files[0]);

        this.setState((prevState: any) => {
          return {
            ...prevState,
            [key]: {
              ...prevState[key],
              [name]: base64,
              image_name: name,
            },
            uploaded: true,
          };
        });
      }
    };
  };

  render() {
    const { isSubmitted, form, uploaded } = this.state;

    return (
      <div className="container">
        <PageTitle backUrl={`/admin/banners`}>Editar banner</PageTitle>
        <div className="row justify-content-center mt-5">
          <div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
            <div className="card">
              <div className="card-body">
                <form
                  onSubmit={this.handleSubmit}
                  noValidate
                  autoComplete="off"
                >
                  <Input
                    label="Nombre"
                    name="name"
                    color="grey"
                    onChange={this.handleChange("form")}
                    value={form.name}
                  />
                  <Select
                    label="Tipo de banner"
                    options={BANNERS}
                    name="type"
                    onChange={this.handleChangeSelect("form")}
                    valueSelect={form.type}
                  />
                  <Textarea
                    name="texto"
                    label="Texto"
                    color="grey"
                    onChange={this.handleChange("form")}
                    value={form.texto}
                  />

                  <div className="row zoneMVCreate text-center my-2">
                    {!uploaded && (
                      <div className="col-12 col-sm-12 col-md-12">
                        <div className="text-center">
                          <label className="upload">
                            <input
                              type="file"
                              onChange={this.handleChangeFile("form")}
                              accept="image/png, image/jpeg"
                              name="url"
                            />
                          </label>
                        </div>
                      </div>
                    )}

                    {uploaded && (
                      <div className="col-12 col-sm-12 col-md-12 mb-3 text-center">
                        <Image
                          name="file"
                          source={form.url}
                          width="200"
                          height="150"
                          alt="img"
                          style={{ marginBottom: 7 }}
                        />
                        <Button
                          type="button"
                          color="orange"
                          title="Quitar"
                          small
                          className="rounded rounded-circle mt-2 mr-1"
                          onClick={() =>
                            this.setState((prevState: any) => {
                              return {
                                form: {
                                  ...prevState.form,
                                  url: "",
                                },
                                uploaded: false,
                              };
                            })
                          }
                        >
                          <Icon name="remove" color="white" />
                        </Button>
                      </div>
                    )}
                  </div>

                  {isSubmitted ? (
                    <Submitted />
                  ) : (
                    <Button color="orange" type="submit" block>
                      Guardar
                    </Button>
                  )}
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const convertBase64 = (file: any) => {
  return new Promise((resolve, reject) => {
    const fileReader: any = new FileReader();

    fileReader.onload = () => {
      resolve(fileReader.result);
    };

    fileReader.onerror = (error: string) => {
      reject(error);
    };

    fileReader.readAsDataURL(file);
  });
};

export default BannerEdit;
