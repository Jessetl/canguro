import React, { useState, useEffect, FunctionComponent } from "react";
import axios, { CancelTokenSource } from "axios";
import { useHistory } from "react-router-dom";

import { BannerProps } from "./FormState";
import { BannerModel } from "models";
import { BannerService } from "services";
import { confirm, handlerError, showSuccess } from "utils";
import { Icon, Modal, Button, Empty, Pagination, Loading } from "components";

import editSvg from "assets/icons/edit.svg";
import deleteSvg from "assets/icons/delete.svg";

export default function Banners() {
  const history = useHistory();
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [banners, setBanners] = useState<BannerModel[]>([]);
  const [lastPage, setLastPage] = useState<number>(1);
  const [page, setPage] = useState<number>(1);
  const [imgurl, setImgurl] = useState<string>("");
  const [modal, setModal] = useState<boolean>(false);

  const cancelToken = axios.CancelToken;

  useEffect(() => {
    const source = cancelToken.source();

    (async () => {
      getAll(source);
      setIsLoading(false);
      return () => source.cancel("Cancel by user");
    })();
  }, [cancelToken]); // eslint-disable-line react-hooks/exhaustive-deps

  const getAll = (source: CancelTokenSource) => {
    BannerService.getAll(page, source)
      .then((response) => {
        const { data, last_page } = response;

        setBanners(data);
        setLastPage(last_page);
        setIsLoading(false);
      })
      .catch(handlerError);
  };

  const onChangePage = async (page: number) => {
    setIsLoading(true);

    const source = cancelToken.source();

    setPage(page);
    getAll(source);
  };

  const onDelete = async (bannerId: number) => {
    const hasDelete = await confirm({ text: "¿Desea eliminar este banner?" });

    if (!hasDelete) {
      return;
    }

    await BannerService.delete(bannerId);

    showSuccess();
    setBanners(banners.filter(({ id }) => id !== bannerId));
  };

  const onToggleViewImage = (url: string) => {
    setImgurl(url);
    setModal(true);
  };

  const toCreate = () => history.push("/admin/banners/create");

  const toEdit = (id: number) => history.push(`/admin/banners/${id}/edit`);

  if (isLoading) {
    return <Loading />;
  }

  return (
    <div className="container">
      {modal && (
        <Modal
          className="d-flex align-items-center"
          onClose={() => setModal(false)}
          visible
          header={false}
        >
          <img src={imgurl} alt="img" width="400" height="300" />
        </Modal>
      )}

      <div className="row mb-3">
        <div className="col col-sm-12 col-md-12 col-lg-12 col-xl-12">
          <h2 className="text-black my-3">Banners</h2>
        </div>
      </div>

      <div className="row mb-3">
        <div className="col col-sm-12 col-md-12 col-lg-12 col-xl-12">
          <Button
            color="orange"
            title="Crear Banner"
            className="float-right text-dark"
            onClick={toCreate}
          >
            Crear Nuevo
          </Button>
        </div>
      </div>

      <div className="row mx-0 rounded-lg shadow-sm bg-white mb-3">
        <div className="col-md-4 px-2 py-2 rounded-left font-bold d-flex align-items-center">
          Nombre
        </div>
        <div className="col-md-4 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Texto
        </div>
        <div className="col-md-4 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Acciones
        </div>
      </div>

      {banners.map((banner) => (
        <RowBanner
          key={banner.id}
          banner={banner}
          onEdit={toEdit}
          onDelete={onDelete}
          onChangeStatus={() => console.log("hola")}
          onToggleViewImage={onToggleViewImage}
        />
      ))}

      {banners.length === 0 && <Empty />}

      <div className="row my-3">
        <div className="col-md">
          <Pagination pages={lastPage} active={page} onChange={onChangePage} />
        </div>
      </div>
    </div>
  );
}

const RowBanner: FunctionComponent<BannerProps> = (props) => {
  const { banner, onEdit, onDelete, onToggleViewImage } = props;

  return (
    <div className="row mx-0 rounded-lg shadow-sm bg-white mb-3">
      <div className="col-md-4 px-2 py-2 bg-gray rounded-left font-bold d-flex align-items-center">
        {banner.name}
      </div>
      <div className="col-md-4 px-2 py-2 bg-gray font-bold d-flex align-items-center text-overflow-ellipsis">
        {banner.texto}
      </div>
      <div className="col-md-4 px-2 py-2 text-center border border-right-0 d-flex align-items-center justify-content-center">
        <Button
          color="dark"
          title="Ver imagen"
          small
          className="rounded rounded-circle"
          onClick={(_: React.FormEvent<HTMLButtonElement>): void =>
            onToggleViewImage(banner.image_url)
          }
        >
          <Icon name="eye" color="white" />
        </Button>
        <Button
          color="dark"
          title="Editar"
          small
          className="rounded rounded-circle"
          onClick={(_: React.FormEvent<HTMLButtonElement>): void =>
            onEdit(banner.id)
          }
        >
          <img src={editSvg} alt="Ver ó Editar" />
        </Button>
        <Button
          color="dark"
          title="Eliminar"
          small
          className="rounded rounded-circle"
          onClick={(_: React.FormEvent<HTMLButtonElement>): void =>
            onDelete(banner.id)
          }
        >
          <img src={deleteSvg} alt="eliminar" />
        </Button>
      </div>
    </div>
  );
};
