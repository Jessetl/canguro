import React, { useState, useEffect, FunctionComponent } from "react";
import { useHistory, useParams } from "react-router-dom";
import axios from "axios";

import { Test } from "models";
import { TestService } from "services";

import { TestProps } from "./FormState";
import { Loading, Pagination, Empty, Button, Modal, Input } from "components";
import { handlerError } from "utils";

import eyeSvg from "assets/icons/eye.svg";

export default function TestList() {
  const history = useHistory();
  const { id } = useParams();
  const [tests, setTests] = useState<Test[]>([]);
  const [testForm, setTestForm] = useState<Test>();
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [page, setPage] = useState<number>(1);
  const [lastPage, setLastPage] = useState<number>(1);
  const [shouldTestForm, setShouldTestForm] = useState<boolean>(false);
  const cancelToken = axios.CancelToken;

  useEffect(() => {
    const source = cancelToken.source();

    (async () => {
      try {
        const { data, last_page } = await TestService.tests(id, page, source);

        setIsLoading(false);
        setLastPage(last_page);
        setTests(data);
      } catch (err) {
        handlerError(err);
      }

      return () => source.cancel("Cancel by user");
    })();
  }, [page, cancelToken]);

  const onChangePage = async (page: number) => {
    const source = cancelToken.source();
    setIsLoading(true);

    try {
      const { data, last_page } = await TestService.tests(id, page, source);

      setIsLoading(false);
      setPage(page);
      setLastPage(last_page);
      setTests(data);
    } catch (err) {
      handlerError(err);
    }
  };

  const toggleResetModal = () => setShouldTestForm(false);

  const toView = (test: Test) => {
    setTestForm(test);
    setShouldTestForm(true);
  };

  if (isLoading) {
    return <Loading />;
  }

  return (
    <div className="container">
      {shouldTestForm && (
        <Modal
          className="d-flex align-items-center"
          onClose={toggleResetModal}
          visible
          size="md"
          header
          title={testForm?.user?.name}
        >
          <div
            style={{
              maxHeight: "500px",
              overflowY: "auto",
              overflowX: "hidden",
            }}
          >
            {testForm?.forms && (
              <React.Fragment>
                {testForm.forms.map((row, key) => {
                  return (
                    <div className="row" key={key.toString()}>
                      <div className="col-12 col-sm-12 col-md">
                        <label htmlFor={row.name}>{row.translation}</label>
                        {row.long || row.high || row.width ? (
                          <div className="row">
                            <div className="col-12 col-sm-12 col-md">
                              <div className="form-group">
                                <Input
                                  disabled
                                  color="white"
                                  type="text"
                                  step="0.001"
                                  defaultValue={row.long}
                                />
                              </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md">
                              <Input
                                disabled
                                color="white"
                                type="text"
                                step="0.001"
                                defaultValue={row.high}
                              />
                            </div>
                            {row.width && (
                              <div className="col-12 col-sm-12 col-md">
                                <Input
                                  disabled
                                  color="white"
                                  type="text"
                                  step="0.001"
                                  defaultValue={row.width}
                                />
                              </div>
                            )}
                          </div>
                        ) : (
                          <div className="form-group">
                            <Input
                              disabled
                              color="white"
                              type="text"
                              step="0.001"
                              defaultValue={row.relative_value}
                            />
                          </div>
                        )}
                      </div>
                      <div className="col-12 col-sm-12 col-md">
                        <div className="form-group">
                          <Input
                            disabled
                            labelClass="invisible"
                            label="Resultado"
                            color="white"
                            type="text"
                            defaultValue={row.absolute_value}
                          />
                        </div>
                      </div>
                    </div>
                  );
                })}
              </React.Fragment>
            )}
          </div>
        </Modal>
      )}

      <div className="row rounded-lg shadow-sm bg-white mb-3">
        <div className="col-md-3 px-2 py-2 rounded-left font-bold d-flex align-items-center">
          Tipo de Formulario
        </div>
        <div className="col-md-2 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Edad Cronológica
        </div>
        <div className="col-md-2 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Edad Tipo
        </div>
        <div className="col-md-2 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Edad Diferencial
        </div>
        <div className="col-md-3 px-2 py-2 text-center border border-right-0 d-flex align-items-center justify-content-center">
          Acciones
        </div>
      </div>

      {tests.map((test) => (
        <RowTest key={test.id} test={test} toView={toView} />
      ))}

      {tests.length === 0 && <Empty />}

      <div className="row my-3">
        <div className="col-md">
          <Pagination pages={lastPage} active={page} onChange={onChangePage} />
        </div>
      </div>
    </div>
  );
}

const RowTest: FunctionComponent<TestProps> = (props) => {
  const { test, toView } = props;

  return (
    <div className="row rounded-lg shadow-sm bg-white mb-3">
      <div className="col-md-3 px-2 py-2 bg-orange rounded-left font-bold d-flex align-items-center">
        {test.type_name}
      </div>
      <div className="col-md-2 px-2 py-2 bg-orange font-bold d-flex align-items-center">
        {test.chronological}
      </div>
      <div className="col-md-2 px-2 py-2 bg-orange font-bold d-flex align-items-center">
        {test.age_type}
      </div>
      <div className="col-md-2 px-2 py-2 bg-orange font-bold d-flex align-items-center">
        {test.differential}
      </div>
      <div className="col-md-3 px-2 py-2 text-center border border-right-0 d-flex align-items-center justify-content-center">
        <Button
          color="orange"
          title="Ver Tests"
          small
          className="rounded rounded-circle"
          onClick={(event: React.FormEvent<HTMLButtonElement>): void => {
            event.preventDefault();
            toView(test);
          }}
        >
          <img src={eyeSvg} alt="Ver Tests" />
        </Button>
      </div>
    </div>
  );
};
