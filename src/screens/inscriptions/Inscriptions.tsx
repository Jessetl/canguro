import React, { useState, useEffect, FunctionComponent } from "react";
import axios, { CancelTokenSource } from "axios";
import moment from "moment";

import { InscriptionProps } from "./FormState";
import { Inscription } from "models";
import { InscriptionService } from "services";
import { Empty, Loading, Icon, Modal, Button, Pagination } from "components";
import { showSuccess, confirm, handlerError } from "utils";

export default function Inscriptions() {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [inscriptions, setInscriptions] = useState<Inscription[]>([]);
  const [showModal, setShowModal] = useState<boolean>(false);
  const [lastPage, setLastPage] = useState<number>(1);
  const [page, setPage] = useState<number>(1);
  const [imgurl, setImgurl] = useState<string>("");

  const cancelToken = axios.CancelToken;

  useEffect(() => {
    const source = cancelToken.source();

    (async () => {
      getAll(source);
      return () => source.cancel("Cancel by user");
    })();
  }, [cancelToken]); // eslint-disable-line react-hooks/exhaustive-deps

  const getAll = (source: CancelTokenSource) => {
    InscriptionService.getAll(page, source)
      .then((response) => {
        const { data, last_page } = response;

        setInscriptions(data);
        setPage(page);
        setLastPage(last_page);
        setIsLoading(false);
      })
      .catch(handlerError);
  };

  const onChangePage = async (page: number) => {
    setIsLoading(true);

    const source = cancelToken.source();

    InscriptionService.getAll(page, source)
      .then((response) => {
        const { data, last_page } = response;

        setInscriptions(data);
        setPage(page);
        setLastPage(last_page);
        setIsLoading(false);
      })
      .catch(handlerError);
  };

  const onToggleViewImage = (url: string) => {
    setImgurl(url);
    setShowModal(true);
  };

  const onChangeStatus = (inscription: Inscription) => {
    const find = inscriptions.find(({ id }) => id === inscription.id);
    const key = inscriptions.findIndex(({ id }) => id === inscription.id);

    if (!!find) {
      let copyInscriptions = [...inscriptions];

      copyInscriptions[key] = {
        ...copyInscriptions[key],
        approve: inscription.approve,
        status_text: inscription.status_text,
      };

      setInscriptions(copyInscriptions);
    }
  };

  const toApprove = async (inscriptionId: number) => {
    const hasApprove = await confirm({
      text: "¿Desea aprobar esta inscripción?",
    });

    if (!!hasApprove) {
      (async () => {
        setIsLoading(true);

        const source = cancelToken.source();

        try {
          const inscription = await InscriptionService.approve(
            inscriptionId,
            source
          );

          onChangeStatus(inscription);
          showSuccess();
          setIsLoading(false);
        } catch (err) {
          setIsLoading(false);
          handlerError(err);
        }

        return () => source.cancel("Cancel by user");
      })();
    }
  };

  const toReject = async (inscriptionId: number) => {
    const hasReject = await confirm({
      text: "¿Desea rechazar esta inscripción?",
    });

    if (!!hasReject) {
      (async () => {
        setIsLoading(true);

        const source = cancelToken.source();

        try {
          const inscription = await InscriptionService.reject(
            inscriptionId,
            source
          );

          onChangeStatus(inscription);
          showSuccess();
          setIsLoading(false);
        } catch (err) {
          setIsLoading(false);
          handlerError(err);
        }

        return () => source.cancel("Cancel by user");
      })();
    }
  };

  if (isLoading) {
    return <Loading />;
  }

  return (
    <div className="container">
      {showModal && (
        <Modal
          className="d-flex align-items-center"
          onClose={() => setShowModal(false)}
          visible
          header={false}
        >
          {!!imgurl && (
            <div style={{ maxHeight: 300, overflowY: "auto" }}>
              <img src={imgurl} alt="Voucher" className="img-fluid" />
            </div>
          )}
          {!imgurl && <label>No se ha cargado ningún voucher</label>}
        </Modal>
      )}

      <div className="row mb-3">
        <div className="col col-sm-12 col-md-12 col-lg-12 col-xl-12">
          <h2 className="text-black my-3">Inscripciones</h2>
        </div>
      </div>

      <div className="row mx-0 rounded-lg shadow-sm bg-white mb-3">
        <div className="col-md-2 px-2 py-2 rounded-left font-bold d-flex align-items-center">
          Curso
        </div>
        <div className="col-md-2 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Fecha
        </div>
        <div className="col-md-2 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Nombre
        </div>
        <div className="col-md-1 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Teléfono
        </div>
        <div className="col-md-2 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Correo
        </div>
        <div className="col-md-1 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Estatus
        </div>
        <div className="col-md-2 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Acciones
        </div>
      </div>

      {inscriptions.map((inscription) => (
        <RowInscription
          key={inscription.id}
          inscription={inscription}
          onApprove={toApprove}
          onReject={toReject}
          onToggleViewImage={onToggleViewImage}
        />
      ))}

      {inscriptions.length === 0 && <Empty />}

      <div className="row my-3">
        <div className="col-md">
          <Pagination pages={lastPage} active={page} onChange={onChangePage} />
        </div>
      </div>
    </div>
  );
}

const RowInscription: FunctionComponent<InscriptionProps> = (props) => {
  const { inscription, onToggleViewImage, onApprove, onReject } = props;

  return (
    <div className="row mx-0 rounded-lg shadow-sm bg-white mb-3">
      <div className="col-md-2 px-2 py-2 bg-gray rounded-left font-bold d-flex align-items-center">
        {inscription.courses?.name || "-"}
      </div>
      <div className="col-md-2 px-2 py-2 bg-gray font-bold d-flex align-items-center text-overflow-ellipsis">
        {moment(inscription.created_at).format("DD-MM-YYYY")}
      </div>
      <div className="col-md-2 px-2 py-2 bg-gray font-bold d-flex align-items-center text-overflow-ellipsis">
        {inscription.name}
      </div>
      <div className="col-md-1 px-2 py-2 bg-gray font-bold d-flex align-items-center text-overflow-ellipsis">
        <a
          className="text-white"
          href={`tel:${inscription.phone}`}
          title={inscription.phone}
        >
          {inscription.phone}
        </a>
      </div>
      <div className="col-md-2 px-2 py-2 bg-gray font-bold d-flex align-items-center text-overflow-ellipsis">
        {inscription.email}
      </div>
      <div className="col-md-1 px-2 py-2 bg-gray font-bold d-flex align-items-center text-overflow-ellipsis">
        {inscription.status_text}
      </div>
      <div className="col-md-2 px-2 py-2 font-bold d-flex align-items-center justify-content-center">
        {!inscription.approve && (
          <React.Fragment>
            <Button
              color="dark"
              title="Aprobar"
              type="button"
              small
              className="rounded rounded-circle mr-1"
              onClick={(
                _: React.MouseEvent<HTMLButtonElement, MouseEvent>
              ): void => {
                onApprove(inscription.id);
              }}
            >
              <Icon name="check" color="white" />
            </Button>
            <Button
              color="dark"
              title="Rechazar"
              type="button"
              small
              className="rounded rounded-circle mr-1"
              onClick={(
                _: React.MouseEvent<HTMLButtonElement, MouseEvent>
              ): void => {
                onReject(inscription.id);
              }}
            >
              <Icon name="times" color="white" />
            </Button>
          </React.Fragment>
        )}
        <Button
          color="dark"
          title="Ver imagen"
          small
          className="rounded rounded-circle"
          onClick={(e: React.FormEvent<HTMLButtonElement>): void => {
            onToggleViewImage(inscription.image_url);
          }}
        >
          <Icon name="eye" color="white" />
        </Button>
      </div>
    </div>
  );
};
