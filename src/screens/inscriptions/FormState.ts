import { Inscription } from "models";

export type InscriptionProps = {
  inscription: Inscription;
  onApprove: (id: number) => void;
  onReject: (id: number) => void;
  onToggleViewImage: (url: string) => void;
};
