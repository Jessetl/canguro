import React, { ReactNode } from "react";
import axios, { CancelTokenSource } from "axios";
import { RouteComponentProps, withRouter } from "react-router-dom";

import { Footer, Header } from "components";
import { ContactService } from "services";
import { ContactModel } from "models";
import { handlerError } from "utils";

import { RootState } from "reducers";
import { connect, ConnectedProps } from "react-redux";

type State = {
  isLoading: boolean;
};

const mapState = (state: RootState) => ({
  info: state.contact,
});

const mapDispatch = {
  dispatchInfo: (contact: ContactModel) => ({
    info: contact,
    type: "Info/SET",
  }),
};

const connector = connect(mapState, mapDispatch);

type Props = ConnectedProps<typeof connector> & RouteComponentProps;

const initialState: State = {
  isLoading: true,
};

class LayoutGuest extends React.PureComponent<Props, State> {
  public source: CancelTokenSource;

  constructor(props: Props) {
    super(props);

    this.state = initialState;
    const cancelToken = axios.CancelToken;

    this.source = cancelToken.source();
  }

  componentDidMount() {
    (async () => {
      try {
        const { dispatchInfo } = this.props;

        const contact = await ContactService.getInformation(this.source);
        dispatchInfo(contact);

        this.setState({ isLoading: false });
      } catch (error) {
        this.setState({ isLoading: false });
        handlerError(error);
      }
      return () => this.source.cancel("Cancel by user");
    })();
  }

  render(): ReactNode {
    const { children, history } = this.props;
    const { isLoading } = this.state;

    const isHiddenComponents: boolean = history.location.pathname === "/login";

    return (
      <React.Fragment>
        <div id="wrapper">
          {!isLoading && !isHiddenComponents && <Header />}
          <div className="">{children}</div>
          {!isLoading && !isHiddenComponents && <Footer />}
        </div>
      </React.Fragment>
    );
  }
}

export default withRouter(connector(LayoutGuest));
