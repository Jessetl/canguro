import React, { useState, useEffect } from "react";
import { RouteComponentProps } from "react-router-dom";
import axios, { CancelTokenSource } from "axios";
import { Inscription, CourseModel } from "models";

import { Button, Input, DatePicker, Icon } from "components";
import { TitleModal, PageTitle } from "components";
import { CourseService, InscriptionService } from "services";
import { useHistory, useLocation } from "react-router-dom";
import { handlerError, Notification } from "functions";


const CoursesForm = () => {

    const history = useHistory();
    const location = useLocation();

    const [iscription, setInscription] = useState<Inscription>();
    const [course, setCourse] = useState<CourseModel>();
    const [name, setName] = useState<string>('');
    const [cedula, setCedula] = useState<string>('');
    const [email, setEmail] = useState<string>('');
    const [phone, setPhone] = useState<string>('');
    const [reference, setReference] = useState<string>('');
    const [approve, setApprove] = useState<boolean>(false);
    const [image, setImage] = useState<string>('');
    const [imageName, setImageName] = useState<string>('');
    const [isSubmit, setIsSubmit] = useState<boolean>(false);

    const cancelToken = axios.CancelToken;
    const source = cancelToken.source();

    useEffect(() => {
        if(location.state){
            const locationstate:any  = location.state;
            setCourse(locationstate.course);
        }
    }, [location]);

    const handleSubmit = (e: any) =>{
        e.preventDefault();
        if(name === '' || cedula === '' ||  phone === '' || email === '' || reference === ''  ){
            Notification('Debe llenar todos los campos', 'warning')
        } else {
            setIsSubmit(true);
            const data = {
                name,
                cedula,
                phone,
                email,
                reference,
                approve,
                curso_id: course?.id,
                file: image
            }
            InscriptionService.store(data, source)
            .then(resp => {
                Notification(resp.message, 'success')
                setTimeout(() => {
                    history.push("/admin/inscriptions");
                }, 1000);   
            })   
            .catch(handlerError) 
        }
    }

    const changeFile =  (e: any) => {
        let file =  e.target.files[0];
        setImageName(file.name);
        createImage(file);
    }

    const createImage = (file: File) => {
        let reader = new FileReader();
        reader.onload = (e: any) => {
            setImage(e.target.result);
            //setUploaded(true);
        };
        reader.readAsDataURL(file);
    }

    const onChangeApprove = () => {
        let status = approve;
        status = !approve;
        setApprove(status);
    };

    return(
        <div className="container">
        <PageTitle backUrl="/admin/cursos">Crear Inscripción</PageTitle>
        <div className="row justify-content-center">
          <div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
            <TitleModal>Registrar Inscripción</TitleModal>
            <div className="card">
              <div className="card-body">
                <form
                  onSubmit={handleSubmit}
                  noValidate
                  autoComplete="off"
                >
                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        label={"Nombre"}
                        name={"name"}
                        color="grey"
                        type="text"
                        value={name}
                        onChange={(e) =>{
                            setName(e.target.value)
                        }}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        label={"Cédula"}
                        name={"cedula"}
                        color="grey"
                        type="text"
                        value={cedula}
                        onChange={(e) =>{
                            setCedula(e.target.value)
                        }}
                      />
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        label={"Correo"}
                        name={"email"}
                        color="grey"
                        type="email"
                        value={email}
                        onChange={(e) =>{
                            setEmail(e.target.value)
                        }}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        label={"Teléfono"}
                        name={"phone"}
                        color="grey"
                        type="text"
                        value={phone}
                        onChange={(e) =>{
                            setPhone(e.target.value)
                        }}
                      />
                    </div>
                  </div>

                  <div className="row">

                    <div className="col-12 col-sm-12 col-md">
                      <Input
                          label={"Referencia"}
                          name={"reference"}
                          color="grey"
                          type="text"
                          value={reference}
                          onChange={(e) =>{
                              setReference(e.target.value)
                          }}
                        />
                    </div>     

                    <div className="col-12 col-sm-12 col-md">
                      <label htmlFor="">Aprobar</label>
                      <Button
                        title="Status"
                        small
                        onClick={(
                          event: React.FormEvent<HTMLButtonElement>
                        ): void => {
                          event.preventDefault();
                          onChangeApprove();
                        }}
                      >
                        <Icon
                          name={
                            approve
                              ? "toggle-on fa-2x mt-1"
                              : "toggle-off fa-2x mt-1"
                          }
                        />
                      </Button>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 col-sm-12 col-md-12 zoneMVCreate">
                      <div className="text-center">
                          <label className="upload">
                              <input type="file" onChange={changeFile} accept="image/png, image/jpeg, application/pdf"/>
                          </label>
                      </div>
                    </div>
                  </div>
                  <hr />
                  <br />
                  <div className="text-center row">
                    <div className="col col-sm">
                      <Button
                        submitted={isSubmit}
                        block
                        type="submit"
                        className="btn-orange shadow"
                      >
                        Guardar
                      </Button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
}

export default CoursesForm;