import { CourseModel, CourseForm } from "models";
import moment from "moment";

export type State = {
  isSubmitted: boolean;
  error: string;
  form: CourseForm;
};

export const initialState: State = {
  isSubmitted: false,
  error: "",
  form: {
    name: "",
    description: "",
    ponentes: "",
    cost: 0.0,
    cupos_disponibles: 0,
    status: false,
    date: new Date(),
  },
};

export const todaySubtract = moment().subtract(18, "year").toDate();

export type CourseProps = {
  course: CourseModel;
  onEdit: (id: number) => void;
  onDelete: (id: number) => void;
  onChangeStatus: (id: number) => void;
};
