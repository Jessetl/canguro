import React, { ReactNode } from "react";
import { RouteComponentProps } from "react-router-dom";
import axios, { CancelTokenSource } from "axios";
import moment from "moment";

import { initialState, State } from "./FormState";
import { CourseForm } from "models";
import { CourseService } from "services";
import { Button, Input, DatePicker, Icon, Submitted } from "components";
import { PageTitle } from "components";
import { handlerError, showSuccess } from "utils";

interface MatchParams {
  id?: string;
}

type Props = RouteComponentProps<MatchParams>;

class CourseEdit extends React.PureComponent<Props, State> {
  public state: State;
  public source: CancelTokenSource;

  constructor(props: Props) {
    super(props);

    this.state = initialState;

    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();
  }

  componentDidMount() {
    this.load();
  }

  componentWillUnmount() {
    this.source.cancel("Cancel by user");
  }

  load = async () => {
    const { id } = this.props.match.params;

    if (!!id) {
      try {
        const data = await CourseService.getById(parseInt(id));

        const parseCourse: CourseForm = {
          ...data,
          date: moment(data.date).toDate(),
        };

        this.setState({
          form: {
            ...parseCourse,
          },
        });
      } catch (error) {
        handlerError(error);
      }
    }
  };

  handleChange = (key: string) => {
    return (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value, name } = event.currentTarget;
      console.log("handleChange: key:", key, name);

      this.setState((prevState: any) => {
        return {
          ...prevState,
          [key]: {
            ...prevState[key],
            [name]: value,
          },
        };
      });
    };
  };

  handleChangeDatePicker = (value: Date) => {
    this.setState((prevState: any) => {
      return {
        ...prevState,
        form: {
          ...prevState.form,
          date: value,
        },
      };
    });
  };

  onChangeStatus = () => {
    this.setState((prevState: any) => {
      return {
        ...prevState,
        form: {
          ...prevState.form,
          status: !prevState.form.status,
        },
      };
    });
  };

  handleSubmit = async (
    event: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    event.preventDefault();

    if (!!!this.state.isSubmitted) {
      this.setState({
        isSubmitted: true,
      });

      const parseCourse: CourseForm = {
        ...this.state.form,
      };

      CourseService.update(parseCourse, this.source)
        .then(() => {
          showSuccess();
          this.props.history.push("/admin/course");
        })
        .catch(handlerError)
        .finally(() => this.setState({ isSubmitted: false }));
    }
  };

  render(): ReactNode {
    const { isSubmitted, form } = this.state;

    return (
      <div className="container">
        <PageTitle backUrl="/admin/course">Editar Curso</PageTitle>
        <div className="row justify-content-center mt-5">
          <div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
            <div className="card">
              <div className="card-body">
                <form
                  onSubmit={this.handleSubmit}
                  noValidate
                  autoComplete="off"
                >
                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        label="Nombre"
                        name="name"
                        color="grey"
                        type="text"
                        onChange={this.handleChange("form")}
                        value={form.name}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        label="Costo"
                        name="cost"
                        color="grey"
                        type="number"
                        onChange={this.handleChange("form")}
                        value={form.cost}
                      />
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        label="Descripción"
                        name="description"
                        color="grey"
                        type="text"
                        onChange={this.handleChange("form")}
                        value={form.description}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        label="Ponentes"
                        name="ponentes"
                        color="grey"
                        type="text"
                        onChange={this.handleChange("form")}
                        value={form.ponentes}
                      />
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <DatePicker
                        label="Fecha"
                        name="date"
                        onChange={(value) => {
                          if (!!value) {
                            this.handleChangeDatePicker(value);
                          }
                        }}
                        value={form.date}
                      />
                    </div>

                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        label="Cupos disponibles"
                        name="cupos_disponibles"
                        color="grey"
                        type="number"
                        value={form.cupos_disponibles}
                        onChange={this.handleChange("form")}
                      />
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <label htmlFor="">Estatus</label>
                      <Button
                        title="Estatus"
                        small
                        onClick={(
                          event: React.FormEvent<HTMLButtonElement>
                        ): void => {
                          event.preventDefault();
                          this.onChangeStatus();
                        }}
                      >
                        <Icon
                          name={
                            form.status
                              ? "toggle-on fa-2x mt-1"
                              : "toggle-off fa-2x mt-1"
                          }
                        />
                      </Button>
                    </div>
                  </div>

                  {isSubmitted ? (
                    <Submitted />
                  ) : (
                    <Button block type="submit" className="btn-orange shadow">
                      Guardar
                    </Button>
                  )}
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CourseEdit;
