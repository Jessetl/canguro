import React, { useState, useEffect, FunctionComponent } from "react";
import { useHistory } from "react-router-dom";
import axios, { CancelTokenSource } from "axios";

import { CourseModel } from "models";
import { CourseService } from "services";

import { CourseProps } from "./FormState";
import { Loading, Pagination, Empty, Icon, Button } from "components";
import { showSuccess, confirm, handlerError } from "utils";

import editSvg from "assets/icons/edit.svg";
import deleteSvg from "assets/icons/delete.svg";

export default function CourseList() {
  const history = useHistory();
  const [courses, setCourses] = useState<CourseModel[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [page, setPage] = useState<number>(1);
  const [lastPage, setLastPage] = useState<number>(1);

  const cancelToken = axios.CancelToken;

  useEffect(() => {
    const source = cancelToken.source();

    (async () => {
      getAll(source);
      return () => source.cancel("Cancel by user");
    })();
  }, [page, cancelToken]); // eslint-disable-line react-hooks/exhaustive-deps

  const getAll = async (source: CancelTokenSource) => {
    try {
      const { data, last_page, to } = await CourseService.getAll(page, source);

      setLastPage(last_page);
      setCourses(data);

      if (!to && page > 1) {
        setPage(page - 1);
      }

      setIsLoading(false);
    } catch (err) {
      setIsLoading(false);
      handlerError(err);
    }
  };

  const onChangePage = (page: number) => {
    (async () => {
      setIsLoading(true);

      const source = cancelToken.source();

      try {
        const { data, last_page } = await CourseService.getAll(page, source);

        setIsLoading(false);
        setLastPage(last_page);
        setPage(page);
        setCourses(data);
      } catch (err) {
        setIsLoading(false);
        handlerError(err);
      }

      return () => source.cancel("Cancel by user");
    })();
  };

  const onDelete = async (courseId: number) => {
    const hasDelete = await confirm({ text: "¿Desea eliminar el curso?" });

    if (!!hasDelete) {
      const source = cancelToken.source();

      await CourseService.delete(courseId);

      showSuccess();
      getAll(source);
    }
  };

  const onChangeStatus = async (courseId: number) => {
    const find = courses.find(({ id }) => id === courseId);
    const key = courses.findIndex(({ id }) => id === courseId);

    if (!!find) {
      try {
        const course = await CourseService.status(courseId);

        let copyCourses = [...courses];

        copyCourses[key] = {
          ...copyCourses[key],
          status: course.status,
        };

        showSuccess();
        setCourses(copyCourses);
      } catch (error) {
        handlerError(error);
      }
    }
  };

  const toCreate = () => history.push("/admin/course/create");
  const toEdit = (id: number) => history.push(`/admin/course/${id}/edit`);

  if (isLoading) {
    return <Loading />;
  }

  return (
    <div className="container">
      <div className="row mb-3">
        <div className="col col-sm-12 col-md-12 col-lg-12 col-xl-12">
          <h2 className="text-black my-3">Cursos</h2>
        </div>
      </div>

      <div className="row mb-3">
        <div className="col col-sm-12 col-md-12 col-lg-12 col-xl-12">
          <Button
            color="orange"
            title="Crear Curso"
            className="float-right text-dark"
            onClick={toCreate}
          >
            Crear Nuevo
          </Button>
        </div>
      </div>

      <div className="row mx-0 rounded-lg shadow-sm bg-white mb-3">
        <div className="col-md-2 px-2 py-2 rounded-left font-bold d-flex align-items-center">
          Nombre
        </div>
        <div className="col-md-2 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Fecha
        </div>
        <div className="col-md-1 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Costo
        </div>
        <div className="col-md-2 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Cupos disponibles
        </div>
        <div className="col-md-2 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Estatus
        </div>
        <div className="col-md-3 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Acciones
        </div>
      </div>

      {courses.map((course) => (
        <RowCourse
          key={course.id}
          course={course}
          onEdit={toEdit}
          onDelete={onDelete}
          onChangeStatus={onChangeStatus}
        />
      ))}

      {courses.length === 0 && <Empty />}

      <div className="row my-3">
        <div className="col-md">
          <Pagination
            pages={lastPage}
            active={page}
            onChangePage={onChangePage}
          />
        </div>
      </div>
    </div>
  );
}

const RowCourse: FunctionComponent<CourseProps> = (props) => {
  const { course, onEdit, onDelete, onChangeStatus } = props;

  return (
    <div className="row mx-0 rounded-lg shadow-sm bg-white mb-3">
      <div className="col-md-2 px-2 py-2 bg-gray rounded-left font-bold d-flex align-items-center">
        {course.name}
      </div>
      <div className="col-md-2 px-2 py-2 bg-gray font-bold d-flex align-items-center">
        {course.date}
      </div>
      <div className="col-md-1 px-2 py-2 bg-gray font-bold d-flex align-items-center">
        {course.cost}
      </div>
      <div className="col-md-2 px-2 py-2 bg-gray font-bold d-flex align-items-center">
        {course.cupos_disponibles}
      </div>

      <div className="col-md-2 px-2 py-2 bg-gray font-bold d-flex align-items-center">
        {course.status ? "Activo" : "Inactivo"}
      </div>
      <div className="col-md-1 px-2 py-2 text-center border border-right-0 d-flex align-items-center justify-content-center">
        <Button
          color="dark"
          title="Ver ó Editar"
          small
          className="rounded rounded-circle"
          onClick={(event: React.FormEvent<HTMLButtonElement>): void => {
            event.preventDefault();
            onEdit(course.id);
          }}
        >
          <img src={editSvg} alt="Ver ó Editar" />
        </Button>
      </div>
      <div className="col-md-1 px-2 py-2 text-center border border-right-0 d-flex align-items-center justify-content-center">
        <Button
          color="dark"
          title={course.status ? "Activar" : "Desactivar"}
          small
          className="rounded rounded-circle"
          onClick={(_: React.FormEvent<HTMLButtonElement>): void =>
            onChangeStatus(course.id)
          }
        >
          <Icon name={course.status ? "check" : "times"} color="white" />
        </Button>
      </div>
      <div className="col-md-1 px-2 py-2 text-center border border-right-0 d-flex align-items-center justify-content-center">
        <Button
          color="dark"
          title="Eliminar"
          small
          className="rounded rounded-circle"
          onClick={(event: React.FormEvent<HTMLButtonElement>): void => {
            event.preventDefault();
            onDelete(course.id);
          }}
          disabled={!!course.inscriptions_count}
        >
          <img src={deleteSvg} alt="eliminar" />
        </Button>
      </div>
    </div>
  );
};
