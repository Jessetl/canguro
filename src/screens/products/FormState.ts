import { Product, ProductForm } from "models";

export type ProductProps = {
  product: Product;
  onEdit: (id: number) => void;
  onDelete: (id: number) => void;
  onChangeStatus: (id: number) => void;
  onToggleViewImage: (url: string) => void;
};

export type State = {
  isSubmitted: boolean;
  error: string;
  form: ProductForm;
  uploaded: boolean;
};

export const initialState: State = {
  isSubmitted: false,
  error: "",
  form: {
    name: "",
    url: "",
    description: "",
    status: 0,
    image_products: [],
    image_deleted: [],
  },
  uploaded: false,
};
