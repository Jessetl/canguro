import React, { useState, useEffect, FunctionComponent } from "react";
import axios, { CancelTokenSource } from "axios";
import { useHistory } from "react-router-dom";

import { ProductProps } from "./FormState";
import { Product } from "models";
import { ProductsService } from "services";
import { Loading, Icon, Modal, Button, Empty, Pagination } from "components";
import { confirm, showSuccess, handlerError } from "utils";

import editSvg from "assets/icons/edit.svg";
import deleteSvg from "assets/icons/delete.svg";

export default function ProductList() {
  const history = useHistory();
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [products, setProducts] = useState<Product[]>([]);
  const [page, setPage] = useState<number>(1);
  const [lastPage, setLastPage] = useState<number>(1);
  const [imgurl, setImgurl] = useState<string>("");
  const [modal, setModal] = useState<boolean>(false);

  const cancelToken = axios.CancelToken;

  useEffect(() => {
    const source = cancelToken.source();

    (async () => {
      getAll(source);
      return () => source.cancel("Cancel by user");
    })();
  }, [page, cancelToken]); // eslint-disable-line react-hooks/exhaustive-deps

  const getAll = (source: CancelTokenSource) => {
    ProductsService.getAll(page, source)
      .then((response) => {
        const { data, last_page, to } = response;

        setProducts(data);
        setLastPage(last_page);

        if (!to && page > 1) {
          setPage(page - 1);
        }
      })
      .catch(handlerError)
      .finally(() => setIsLoading(false));
  };

  const onChangePage = async (page: number) => {
    setIsLoading(true);

    const source = cancelToken.source();

    ProductsService.getAll(page, source)
      .then((response) => {
        const { data, last_page } = response;

        setProducts(data);
        setPage(page);
        setLastPage(last_page);
        setIsLoading(false);
      })
      .catch(handlerError);
  };

  const onDelete = async (productId: number) => {
    const hasDelete = await confirm({ text: "¿Desea eliminar este producto?" });

    if (!!hasDelete) {
      const source = cancelToken.source();

      await ProductsService.delete(productId);

      showSuccess();
      getAll(source);
    }
  };

  const onToggleViewImage = (url: string) => {
    setImgurl(url);
    setModal(true);
  };

  const onChangeStatus = async (productId: number) => {
    const find = products.find(({ id }) => id === productId);
    const key = products.findIndex(({ id }) => id === productId);

    if (!!find) {
      try {
        const product = await ProductsService.status(productId);

        let copyProducts = [...products];

        copyProducts[key] = {
          ...copyProducts[key],
          status: product.status,
          status_text: product.status_text,
        };

        showSuccess();
        setProducts(copyProducts);
      } catch (error) {
        handlerError(error);
      }
    }
  };

  const toCreate = () => history.push("/admin/products/create");

  const toEdit = (id: number) => history.push(`/admin/products/${id}/edit`);

  if (isLoading) {
    return <Loading />;
  }

  return (
    <div className="container">
      {modal && (
        <Modal
          className="d-flex align-items-center"
          onClose={() => setModal(false)}
          visible
          header={false}
        >
          {!!imgurl && (
            <div
              style={{
                backgroundImage: `url(${imgurl})`,
                backgroundPosition: "center center",
                backgroundSize: "cover",
                width: 400,
                height: 400,
              }}
            ></div>
          )}
        </Modal>
      )}

      <div className="row mb-3">
        <div className="col col-sm-12 col-md-12 col-lg-12 col-xl-12">
          <h2 className="text-black my-3">Productos</h2>
        </div>
      </div>

      <div className="row mb-3">
        <div className="col col-sm-12 col-md-12 col-lg-12 col-xl-12">
          <Button
            color="orange"
            title="Crear Producto"
            className="float-right text-dark"
            onClick={toCreate}
          >
            Crear Nuevo
          </Button>
        </div>
      </div>

      <div className="row mx-0 rounded-lg shadow-sm bg-white mb-3">
        <div className="col-md-3 px-2 py-2 rounded-left font-bold d-flex align-items-center text-overflow-ellipsis">
          Nombre
        </div>
        <div className="col-md-3 px-2 py-2 rounded-left font-bold d-flex align-items-center text-overflow-ellipsis">
          Status
        </div>
        <div className="col-md-3 px-2 py-2 font-bold border border-right-0 d-flex align-items-center text-overflow-ellipsis">
          Descripción
        </div>
        <div className="col-md-3 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Acciones
        </div>
      </div>

      {products.map((product) => (
        <RowProduct
          key={product.id}
          product={product}
          onEdit={toEdit}
          onDelete={onDelete}
          onChangeStatus={onChangeStatus}
          onToggleViewImage={onToggleViewImage}
        />
      ))}

      {products.length === 0 && <Empty />}

      <div className="row my-3">
        <div className="col-md">
          <Pagination pages={lastPage} active={page} onChange={onChangePage} />
        </div>
      </div>
    </div>
  );
}

const RowProduct: FunctionComponent<ProductProps> = (props) => {
  const { product, onEdit, onDelete } = props;
  const { onChangeStatus, onToggleViewImage } = props;

  return (
    <div className="row mx-0 rounded-lg shadow-sm bg-white mb-3">
      <div className="col-md-3 px-2 py-2 bg-gray rounded-left font-bold d-flex align-items-center text-overflow-ellipsis">
        {product.name}
      </div>
      <div className="col-md-3 px-2 py-2 bg-gray font-bold d-flex align-items-center text-overflow-ellipsis">
        {product.status_text}
      </div>
      <div className="col-md-3 px-2 py-2 bg-gray font-bold d-flex align-items-center text-overflow-ellipsis">
        {product.description}
      </div>
      <div className="col-md-3 px-2 py-2 text-center border border-right-0 d-flex align-items-center justify-content-center">
        <Button
          color="dark"
          title="Ver imagen"
          small
          className="rounded rounded-circle"
          onClick={(_: React.FormEvent<HTMLButtonElement>): void =>
            onToggleViewImage(product.image_url)
          }
        >
          <Icon name="eye" color="white" />
        </Button>
        <Button
          color="dark"
          title="Editar"
          small
          className="rounded rounded-circle"
          onClick={(_: React.FormEvent<HTMLButtonElement>): void =>
            onEdit(product.id)
          }
        >
          <img src={editSvg} alt="Ver ó Editar" />
        </Button>
        <Button
          color="dark"
          title={product.status ? "Activar" : "Desactivar"}
          small
          className="rounded rounded-circle"
          onClick={(_: React.FormEvent<HTMLButtonElement>): void =>
            onChangeStatus(product.id)
          }
        >
          <Icon name={product.status ? "check" : "times"} color="white" />
        </Button>
        <Button
          color="dark"
          title="Eliminar"
          small
          className="rounded rounded-circle"
          onClick={(_: React.FormEvent<HTMLButtonElement>): void =>
            onDelete(product.id)
          }
        >
          <img src={deleteSvg} alt="eliminar" />
        </Button>
      </div>
    </div>
  );
};
