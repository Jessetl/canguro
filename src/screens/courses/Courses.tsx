import React, { useState, useCallback, useEffect } from "react";
import axios from "axios";
import moment from "moment";

import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import interactionPlugin, { DateClickArg } from "@fullcalendar/interaction";

import { Banner, Header, Footer, Loading } from "components";
import CoursesRegister from "./CoursesRegister";

import { yellowPoint } from "assets/img";

import { CourseService, BannerService } from "services";
import { CourseModel, BannerModel } from "models";
import { handlerError } from "utils";

const Courses = () => {
  const format: string = "YYYY-MM-DD";

  const [visible, setVisible] = React.useState(false);
  const [course, setCourse] = useState<CourseModel | null>(null);
  const [listado, setListado] = useState<CourseModel[]>([]);
  const [event] = useState(true);
  const [id, setId] = useState<number | null>(null);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [banners, setBanner] = useState<BannerModel[]>([]);

  const cancelToken = axios.CancelToken;

  useEffect(() => {
    const source = cancelToken.source();

    (async () => {
      try {
        const courses = await CourseService.get(source);
        const banners = await BannerService.getBannerGeneral(source);

        setListado(courses);
        setBanner(banners);
        setIsLoading(false);
      } catch (err) {
        handlerError(err);
      }

      return () => source.cancel("Cancel by user");
    })();
  }, [cancelToken]); // eslint-disable-line react-hooks/exhaustive-deps

  const formatEvents = () => {
    return listado.map((data) => {
      const { date } = data;
      const title = data.name;
      const display = "background";
      const backgroundColor = "#ffe44f";
      const textColor = "#000";

      return {
        title,
        date,
        display,
        backgroundColor,
        textColor,
      };
    });
  };

  const handleDateClick = useCallback(
    (arg: DateClickArg) => {
      const course = listado.find(({ date }) =>
        moment(date, format).isSame(moment(arg.dateStr, format).toDate())
      );

      setVisible(false);
      setCourse(null);

      if (!!course) {
        const parseCourse = {
          ...course,
          available: course.cupos_disponibles - course.inscriptions_count,
        };

        setId(course.id);
        setCourse(parseCourse);
      }
    },
    [listado]
  );

  const onToggleFormRegister = () => {
    if (!!course) {
      setId(course.id);
      setVisible(!visible);
    }
  };

  const clear = () => {
    const course = listado.find(({ id: courseId }) => courseId === id);
    const courseKey = listado.findIndex(({ id: courseId }) => courseId === id);

    setId(null);
    setVisible(!visible);

    if (!!course) {
      let copyCourses = [...listado];

      copyCourses[courseKey] = {
        ...copyCourses[courseKey],
        inscriptions_count: copyCourses[courseKey].inscriptions_count + 1,
      };

      const copyCourse = {
        ...course,
        available:
          course.cupos_disponibles - copyCourses[courseKey].inscriptions_count,
      };

      setListado(copyCourses);
      setCourse(copyCourse);
    }
  };

  let banner_image;
  if (banners.length > 0) {
    banner_image = banners[0].image_url;
  } else {
    banner_image = "";
  }

  if (isLoading) {
    return <Loading />;
  }

  return (
    <div className="bg-black-light w-100 h-100">
      <Banner title="Cursos" banner_image={banner_image} />
      {event && (
        <div className="container py-5">
          <div className="row">
            <div className="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">
              <div className="w-100 newCalendar">
                <div className="d-flex align-items-center my-3">
                  <img
                    src={yellowPoint}
                    className="img-fliud mr-2"
                    alt="Calendario"
                    width="16"
                  />
                  <span className="text-white poppins-regular">
                    Cursos programados
                  </span>
                </div>
                <FullCalendar
                  plugins={[dayGridPlugin, interactionPlugin]}
                  dateClick={handleDateClick}
                  initialView="dayGridMonth"
                  locale="esLocale"
                  headerToolbar={{
                    left: "prev",
                    center: "title",
                    right: "next",
                  }}
                  dayHeaderClassNames="headerCalendar"
                  dayCellClassNames="daysCalendar"
                  slotLaneClassNames="laneCalendar"
                  selectable
                  events={formatEvents()}
                />
              </div>
            </div>
            {course && (
              <div className="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7">
                <div className="mt-0 mt-md-5 ml-0 ml-md-5">
                  <p className="typeCalendar text-center text-sm-left">Curso</p>
                  <h1 className="titleCalendar">{course.name}</h1>
                  <p className="typeCalendar">
                    {moment(course.date, "YYYY-MM-DD").format(
                      "DD [de] MMMM [de] YYYY"
                    )}
                  </p>
                  <p className="descriptionCalendar">{course.description}</p>
                  <div className="d-flex">
                    <p className="capacityCalendar mr-5 mt-2">
                      Capacidad:
                      <span className="descriptionCalendar ml-2">
                        {course.cupos_disponibles} Cupos
                      </span>
                    </p>
                    <p className="capacityCalendar mt-2">
                      Disponibles:
                      <span className="descriptionCalendar ml-2">
                        {course.available} Cupos
                      </span>
                    </p>
                  </div>
                  <h3 className="priceCalendar">{`$ ${course.cost}`}</h3>
                  {!!course.available && (
                    <div className="row">
                      <div className="col">
                        <button
                          className="poppins-regular usButton mt-3"
                          onClick={onToggleFormRegister}
                        >
                          Inscribirme
                        </button>
                      </div>
                    </div>
                  )}
                </div>
              </div>
            )}
          </div>
        </div>
      )}

      {visible && (
        <div className="bg-gray w-100">
          <CoursesRegister curso_id={id} clear={clear} />
        </div>
      )}
    </div>
  );
};

export default Courses;
