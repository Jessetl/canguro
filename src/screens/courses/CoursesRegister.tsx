import React, { useState } from "react";
import { Card, Button, TitleModal, Input, Submitted } from "components";
import { handlerError, Notification } from "functions";
import { InscriptionService } from "services";
import axios from "axios";
import { showWarning } from "utils";

const CoursesRegister = (props: any) => {
  const [files, setFiles] = useState("");
  const [name, setName] = useState("");
  const [surname, setSurname] = useState("");
  const [cedula, setCedula] = useState("");
  const [phone, setPhone] = useState("");
  const [email, setEmail] = useState("");
  const [fileName, setFileName] = useState("");
  const [fileType, setFileType] = useState("");
  const [isSubmit, setIsSubmit] = useState<boolean>(false);

  const cancelToken = axios.CancelToken;
  const source = cancelToken.source();

  const handler = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    if (props.curso_id <= 0) {
      return Notification(
        "Primero debes seleccionar el curso a realizar",
        "warning"
      );
    } else {
      if (
        name === "" ||
        cedula === "" ||
        phone === "" ||
        email === "" ||
        surname === "" ||
        fileName === "" ||
        fileType === ""
      ) {
        return Notification("Debe llenar todos los campos", "warning");
      }

      const re = /^([\da-z_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/;
      if (!re.test(email)) {
        return Notification(
          "Debe digitar una dirección de correo electrónico válido",
          "warning"
        );
      } else {
        setIsSubmit(true);

        const data = {
          name: name + " " + surname,
          cedula,
          phone,
          email,
          approve: 0,
          curso_id: props.curso_id,
          file: files,
          fileName: fileName,
          fileType: fileType,
        };

        InscriptionService.store(data, source)
          .then((resp) => {
            Notification(resp.message, "success");
            clear();
          })
          .catch((error) => {
            const { data } = error.response;

            if (data.error) {
              return showWarning(data.error);
            }

            handlerError(error);
          })
          .finally(() => setIsSubmit(false));
      }
    }
  };

  const uploadSingleFile = (e: any) => {
    const file = e.target.files[0];
    setFileName(file.name);
    setFileType(file.type);
    createImage(file);
  };

  const createImage = (file: File) => {
    let reader = new FileReader();
    reader.onload = (e: any) => {
      setFiles(e.target.result);
    };
    reader.readAsDataURL(file);
  };

  const clear = () => {
    setIsSubmit(false);
    setName("");
    setCedula("");
    setSurname("");
    setEmail("");
    setPhone("");
    setFileName("");
    setFileType("");
    props.clear();
  };

  //estados traidos de la bd
  // const options = [{ value: 3, label: "Blues" }];

  // const ImagePrint = (
  //   <img
  //     src={files}
  //     className="d-block rounded imageVenues position-static"
  //     alt="Img"
  //   />
  // );

  return (
    <div className="col-md-8 pt-2 m-auto justify-content-center w-100 h-100">
      <form
        onSubmit={handler}
        noValidate
        autoComplete="off"
        className="mt-2 mt-sm-4 w-100 h-100"
      >
        <TitleModal className="titleCoursesRegister">Registro</TitleModal>
        <p className="text-center text-grey m-0">
          Ingrese los datos para formar parte del curso
        </p>

        <Card className="formRegisterCourses w-100 h-100 mb-0">
          <div className="row pl-auto pr-auto">
            <div className="col-11 col-sm-6 m-auto">
              <div className="text-center">
                <Input
                  name={"name"}
                  color="grey"
                  type="text"
                  className="rounded"
                  placeholder="Nombre"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                />
              </div>
            </div>
            <div className="col-11 col-sm-6  m-auto">
              <div className="text-center">
                <Input
                  name={"lastname"}
                  color="grey"
                  type="text"
                  className="rounded"
                  placeholder="Apellido"
                  value={surname}
                  onChange={(e) => setSurname(e.target.value)}
                />
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-11 col-sm-6  m-auto">
              <div className="text-center">
                <Input
                  name={"ci"}
                  color="grey"
                  type="text"
                  className="rounded"
                  placeholder="C.I"
                  value={cedula}
                  onChange={(e) => setCedula(e.target.value)}
                />
              </div>
            </div>
            <div className="col-11 col-sm-6  m-auto">
              <div className="text-center">
                <Input
                  name={"phone"}
                  color="grey"
                  type="text"
                  className="rounded"
                  placeholder="Teléfono"
                  value={phone}
                  onChange={(e) => setPhone(e.target.value)}
                />
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-11 col-sm-6  m-auto">
              <div className="text-center">
                <Input
                  name={"email"}
                  color="grey"
                  type="text"
                  className="rounded"
                  placeholder="Correo"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </div>
            </div>
            <div className="col-11 col-sm-6  ml-auto mr-auto mb-2">
              <div className="text-center border borderCoursesRegister m-0 p-0">
                <label
                  htmlFor="photo"
                  className="w-100 text-white imageCoursesRegister"
                >
                  {files ? (
                    <div>Comprobacion de pago añadida</div>
                  ) : (
                    <div>Adjuntar comprobación de pago</div>
                  )}
                </label>
              </div>
              <input
                type="file"
                id="photo"
                name="photo"
                accept="image/png, image/jpeg, .pdf"
                color="orange"
                onChange={uploadSingleFile}
                hidden
              />
            </div>
            {isSubmit ? (
              <div className="col-12 col-sm-6  ml-auto mr-auto mb-2">
                <Submitted />
              </div>
            ) : (
              <div className="mx-auto my-3">
                <Button
                  submit={isSubmit}
                  className="font-bold textVenue button-rounded buttonCoursesRegister"
                  block
                  type="submit"
                >
                  Enviar
                </Button>
              </div>
            )}
          </div>
        </Card>
      </form>
    </div>
  );
};

export default CoursesRegister;
