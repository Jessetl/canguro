import React from "react";
import axios, { CancelTokenSource } from "axios";

import { handlerError } from "utils";
import { RouteComponentProps } from "react-router";
// import { Scrollbars } from "react-custom-scrollbars";

import { initialState, State } from "./FormState";
import UsService from "services/modules/usService";
import BannerService from "services/modules/bannerService";
import BlogHome from "../../components/BlogHome";
import TrabajaConNosotros from "../../components/trabajaConNosotros";
import RecibeInformacion from "../../components/recibeInformacion";

import { Banner } from "components";
import { CanguroMundo, Identidad } from "components";
import { ButtonsMobile, MapChart, BrandsInfo } from "components";
import { CarouselItems, UsArea, InstaSlider } from "components";

import icoDescargas from "../../assets/img/canguro-descargas.png";

interface MatchParams {
  id?: string;
}

type Props = RouteComponentProps<MatchParams>;

class Web extends React.Component<Props, State> {
  public state: State;
  public source: CancelTokenSource;

  private brandRef = React.createRef<HTMLDivElement>();
  private contactRef = React.createRef<HTMLDivElement>();

  handlerInformation = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
  };

  constructor(props: Props) {
    super(props);

    this.state = initialState;

    this.brandRef = React.createRef();
    this.contactRef = React.createRef();

    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();
  }

  componentDidMount() {
    const { match } = this.props;

    this.getUs();
    this.getBanners();

    if (!!match.params.id) {
      const { params } = match;

      switch (params.id) {
        case "brands":
          return this.setScrollBrandView();
      }
    }
  }

  setScrollBrandView = () => {
    setTimeout(() => {
      this.brandRef.current?.scrollIntoView({
        behavior: "smooth",
        block: "center",
      });
    }, 500);
  };

  getUs = () => {
    UsService.index()
      .then((us) => {
        if (us.length > 0) {
          this.setState((prevState: any) => {
            return {
              ...prevState,
              moral: us[0].moral,
              us_image: us[0].us_image_url,
            };
          });
        }
      })
      .catch((err) => handlerError(err));
  };

  getBanners = () => {
    BannerService.getBannerPrincipal(this.source)
      .then((banner) => {
        if (banner.length > 0) {
          this.setState((prevState: any) => {
            return {
              ...prevState,
              banner_image: banner[0].image_url,
              banner_message: banner[0].texto,
            };
          });
        }
      })
      .catch((err) => handlerError(err));
  };

  render() {
    const { moral, us_image, banner_image, banner_message } = this.state;

    return (
      <div className="w-100">
        <Banner
          title="Tecnología • Vanguardia • Innovación"
          banner_image={banner_image}
          message={banner_message}
          principal
        />

        <div className="bgBackground">
          <CarouselItems />
        </div>

        <div className="btn-sex">
          <div className="buttonMobile h-100 w-100 mb-3">
            <div className="container">
              <div className="row h-50">
                <div className="col-md-4 align-self-center">
                  <h1>
                    DESCARGA <br />
                    NUESTRA <br />
                    APP
                  </h1>
                </div>
                <div className="col-md-4 align-self-center">
                  <img
                    className="img-can"
                    alt="icoDescargas"
                    src={icoDescargas}
                  />
                </div>
                <div className="col-md-4 align-self-center">
                  <ButtonsMobile />
                </div>
              </div>
            </div>
          </div>
        </div>

        <div>
          <UsArea title="Nosotros" message={moral} image={us_image} />
        </div>

        <div>
          <InstaSlider />
        </div>

        <div className="m-auto mapWidth">
          <MapChart />
        </div>

        <div className="p-0 m-auto w-100 canguroWorldText">
          <h2 className="poppins-semibold">Canguro en el mundo</h2>
          <CanguroMundo />
        </div>

        <BlogHome />

        <div className="m-0 p-0 bgFooter identity">
          <h3 className="poppins-semibold text-center titleIdentity textColorYellow m-0">
            Identidades Canguro
          </h3>
          <Identidad />
        </div>

        <div className="bgInformation informationMarks m-0 p-0">
          <div id="brands" ref={this.brandRef} className="informationMarks2">
            <div className="container">
              <h3 className="poppins-semibold text-center titleBrands textColorYellow  m-0">
                Nuestras marcas
              </h3>
            </div>
            <BrandsInfo />
          </div>
        </div>

        <div className="informationBG w-100 h-100 m-0 p-0 overflow-hidden">
          <div className="col-sm-10 col-md-6 w-100 h-100 informationPadding">
            <RecibeInformacion />
          </div>
        </div>

        <div className="w-100 h-100 text-center cvPadding ">
          <TrabajaConNosotros />
        </div>
      </div>
    );
  }
}

export default Web;
