export type State = {
  isSubmit: boolean;
  moral: string;
  us_image: string;
  link_app_android: string;
  link_app_ios: string;
  banner_image: string;
  banner_message: string;
};

export const initialState: State = {
  isSubmit: false,
  moral: "",
  us_image: "",
  link_app_android: "",
  link_app_ios: "",
  banner_image: "",
  banner_message: "",
};
