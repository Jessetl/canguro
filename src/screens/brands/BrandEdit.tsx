import React from "react";
import axios, { CancelTokenSource } from "axios";
import { RouteComponentProps, withRouter } from "react-router-dom";

import { State, initialState } from "./FormState";
import { BrandService } from "services";
import { showSuccess, handlerError } from "utils";
import { Button, Submitted, PageTitle, Image, Icon } from "components";
import { Input, Textarea } from "components";
import { BrandForm } from "models";

interface MatchParams {
  id?: string;
}

type Props = RouteComponentProps<MatchParams>;

class BrandEdit extends React.Component<Props, State> {
  public state: State;
  public source: CancelTokenSource;

  constructor(props: Props) {
    super(props);

    this.state = initialState;

    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();
  }

  componentDidMount() {
    this.load();
  }

  componentWillUnmount() {
    this.source.cancel("Cancel by user");
  }

  load = async () => {
    const { id } = this.props.match.params;

    if (!!id) {
      try {
        const data = await BrandService.getById(parseInt(id));

        const parseBrand: BrandForm = {
          ...data,
        };

        this.setState({
          form: {
            ...parseBrand,
            image_url: !!data.image_url ? data.image_url_full : "",
            logo: !!data.logo ? data.logo_url : "",
            logoGold: !!data.logoGold ? data.logo_gold_url : "",
          },
          image_url: !!parseBrand.image_url,
          logo: !!parseBrand.logo,
          logoGold: !!parseBrand.logoGold,
        });
      } catch (error) {
        handlerError(error);
      }
    }
  };

  handleSubmit = async (
    event: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    const { isSubmitted } = this.state;

    event.preventDefault();

    if (!!!isSubmitted) {
      this.setState({ isSubmitted: true });

      BrandService.update(this.state.form)
        .then((_) => {
          this.setState({ isSubmitted: false });
          showSuccess();
          this.props.history.push("/admin/brands");
        })
        .catch((error) => {
          this.setState({ isSubmitted: false });
          handlerError(error);
        });
    }
  };

  handleChange = (key: string) => {
    return (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value, name } = event.currentTarget;
      console.log("handleChange: key:", key, name);

      this.setState((prevState: any) => {
        return {
          ...prevState,
          [key]: {
            ...prevState[key],
            [name]: value,
          },
        };
      });
    };
  };

  handleChangeFile = (key: string) => {
    return async (event: React.ChangeEvent<HTMLInputElement>) => {
      const { name } = event.target;

      if (event.currentTarget.files) {
        const base64 = await convertBase64(event.currentTarget.files[0]);

        this.setState((prevState: any) => {
          return {
            ...prevState,
            [key]: {
              ...prevState[key],
              [name]: base64,
            },
            [name]: true,
          };
        });
      }
    };
  };

  render() {
    const { isSubmitted, form, image_url, logo, logoGold } = this.state;

    return (
      <div className="container">
        <PageTitle backUrl={`/admin/brands`}>Editar Marca</PageTitle>
        <div className="row justify-content-center mt-5">
          <div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
            <div className="card">
              <div className="card-body">
                <form
                  onSubmit={this.handleSubmit}
                  noValidate
                  autoComplete="off"
                >
                  <Input
                    label="Nombre"
                    name="name"
                    color="grey"
                    onChange={this.handleChange("form")}
                    value={form.name}
                  />
                  <Input
                    label="Facebook"
                    name="fb_url"
                    color="grey"
                    onChange={this.handleChange("form")}
                    value={form.fb_url}
                  />
                  <Input
                    label="Twitter"
                    name="tw_url"
                    color="grey"
                    onChange={this.handleChange("form")}
                    value={form.tw_url}
                  />
                  <Input
                    label="Instagram"
                    name="ig_url"
                    color="grey"
                    onChange={this.handleChange("form")}
                    value={form.ig_url}
                  />
                  <Textarea
                    name="description"
                    label="Descripcion"
                    onChange={this.handleChange("form")}
                    value={form.description}
                  />

                  <div className="row zoneMVCreate my-2">
                    {!image_url && (
                      <div className="col-12 col-sm-12 col-md-12">
                        <label className="text-left poppins-regular">
                          Imagen
                        </label>
                        <div className="text-center">
                          <label className="upload">
                            <input
                              type="file"
                              onChange={this.handleChangeFile("form")}
                              accept="image/png, image/jpeg"
                              name="image_url"
                            />
                          </label>
                        </div>
                      </div>
                    )}

                    {image_url && (
                      <div className="col-12 col-sm-12 col-md-12 mb-3 text-center">
                        <label className="text-left poppins-regular">
                          Imagen
                        </label>
                        <Image
                          name="file"
                          source={form.image_url}
                          width="200"
                          height="150"
                          alt="img"
                          style={{ marginBottom: 7 }}
                        />
                        <Button
                          type="button"
                          color="orange"
                          title="Quitar"
                          small
                          className="rounded rounded-circle mt-2 mr-1"
                          onClick={() =>
                            this.setState((prevState: any) => {
                              return {
                                form: {
                                  ...prevState.form,
                                  image_url: "",
                                },
                                image_url: false,
                              };
                            })
                          }
                        >
                          <Icon name="remove" color="white" />
                        </Button>
                      </div>
                    )}
                  </div>

                  <div className="row zoneMVCreate my-2">
                    {!logo && (
                      <div className="col-12 col-sm-12 col-md-12">
                        <label className="text-left poppins-regular">
                          Logo
                        </label>
                        <div className="text-center">
                          <label className="upload">
                            <input
                              type="file"
                              onChange={this.handleChangeFile("form")}
                              accept="image/png, image/jpeg"
                              name="logo"
                            />
                          </label>
                        </div>
                      </div>
                    )}

                    {logo && (
                      <div className="col-12 col-sm-12 col-md-12 mb-3 text-center">
                        <label className="text-left poppins-regular">
                          Logo
                        </label>
                        <Image
                          name="file"
                          source={form.logo}
                          width="200"
                          height="150"
                          alt="img"
                          style={{ marginBottom: 7 }}
                        />
                        <Button
                          type="button"
                          color="orange"
                          title="Quitar"
                          small
                          className="rounded rounded-circle mt-2 mr-1"
                          onClick={() =>
                            this.setState((prevState: any) => {
                              return {
                                form: {
                                  ...prevState.form,
                                  logo: "",
                                },
                                logo: false,
                              };
                            })
                          }
                        >
                          <Icon name="remove" color="white" />
                        </Button>
                      </div>
                    )}
                  </div>

                  <div className="row zoneMVCreate my-2">
                    {!logoGold && (
                      <div className="col-12 col-sm-12 col-md-12">
                        <label className="text-left poppins-regular">
                          Logo Gold
                        </label>
                        <div className="text-center">
                          <label className="upload">
                            <input
                              type="file"
                              onChange={this.handleChangeFile("form")}
                              accept="image/png, image/jpeg"
                              name="logoGold"
                            />
                          </label>
                        </div>
                      </div>
                    )}

                    {logoGold && (
                      <div className="col-12 col-sm-12 col-md-12 mb-3 text-center">
                        <label className="text-left poppins-regular">
                          Logo Gold
                        </label>
                        <Image
                          name="file"
                          source={form.logoGold}
                          width="200"
                          height="150"
                          alt="img"
                          style={{ marginBottom: 7 }}
                        />
                        <Button
                          type="button"
                          color="orange"
                          title="Quitar"
                          small
                          className="rounded rounded-circle mt-2 mr-1"
                          onClick={() =>
                            this.setState((prevState: any) => {
                              return {
                                form: {
                                  ...prevState.form,
                                  logoGold: "",
                                },
                                logoGold: false,
                              };
                            })
                          }
                        >
                          <Icon name="remove" color="white" />
                        </Button>
                      </div>
                    )}
                  </div>

                  {isSubmitted ? (
                    <Submitted />
                  ) : (
                    <Button color="orange" block type="submit">
                      Guardar
                    </Button>
                  )}
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const convertBase64 = (file: any) => {
  return new Promise((resolve, reject) => {
    const fileReader: any = new FileReader();

    fileReader.onload = () => {
      resolve(fileReader.result);
    };

    fileReader.onerror = (error: string) => {
      reject(error);
    };

    fileReader.readAsDataURL(file);
  });
};

export default withRouter(BrandEdit);
