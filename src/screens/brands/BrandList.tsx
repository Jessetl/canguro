import React, { FunctionComponent, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import axios, { CancelTokenSource } from "axios";

import { BrandProps } from "./FormState";
import { Brand } from "models";
import { Button, Loading, Empty, Pagination, Icon } from "components";
import { BrandService } from "services";
import { showSuccess, confirm, handlerError } from "utils";

import editSvg from "assets/icons/edit.svg";
import deleteSvg from "assets/icons/delete.svg";

export default function BrandList() {
  const history = useHistory();
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [page, setPage] = useState<number>(1);
  const [lastPage, setLastPage] = useState<number>(1);
  const [brands, setBrands] = useState<Brand[]>([]);

  const cancelToken = axios.CancelToken;

  useEffect(() => {
    const source = cancelToken.source();

    (async () => {
      getAll(source);
      return () => source.cancel("Cancel by user");
    })();
  }, [page, cancelToken]); // eslint-disable-line react-hooks/exhaustive-deps

  const getAll = async (source: CancelTokenSource) => {
    try {
      const { data, last_page, to } = await BrandService.getAll(page, source);

      setLastPage(last_page);
      setBrands(data);

      if (!to && page > 1) {
        setPage(page - 1);
      }

      setIsLoading(false);
    } catch (err) {
      setIsLoading(false);
      handlerError(err);
    }
  };

  const onDelete = async (brandId: number) => {
    const hasDelete = await confirm({ text: "¿Desea eliminar esta marca?" });

    if (!!hasDelete) {
      const source = cancelToken.source();

      await BrandService.delete(brandId);

      showSuccess();
      getAll(source);
    }
  };

  const onChangePage = async (page: number) => {
    setIsLoading(true);

    const source = cancelToken.source();

    try {
      const { data, last_page } = await BrandService.getAll(page, source);

      setIsLoading(false);
      setPage(page);
      setLastPage(last_page);
      setBrands(data);
    } catch (err) {
      handlerError(err);
    }
  };

  const onChangeStatus = async (brandId: number) => {
    const find = brands.find(({ id }) => id === brandId);
    const key = brands.findIndex(({ id }) => id === brandId);

    if (!!find) {
      try {
        const brand = await BrandService.status(brandId);

        let copyBrands = [...brands];

        copyBrands[key] = {
          ...copyBrands[key],
          status: brand.status,
        };

        showSuccess();
        setBrands(copyBrands);
      } catch (error) {
        handlerError(error);
      }
    }
  };

  const toCreate = () => history.push("/admin/brands/create");
  const toEdit = (id: number) => history.push(`/admin/brands/${id}/edit`);

  if (isLoading) {
    return <Loading />;
  }

  return (
    <div className="container">
      <div className="row mb-3">
        <div className="col col-sm-12 col-md-12 col-lg-12 col-xl-12">
          <h2 className="text-black my-3">Marcas</h2>
        </div>
      </div>

      <div className="row mb-3">
        <div className="col col-sm-12 col-md-12 col-lg-12 col-xl-12">
          <Button
            color="orange"
            title="Crear Marca"
            className="float-right text-dark"
            onClick={toCreate}
          >
            Crear Nuevo
          </Button>
        </div>
      </div>

      <div className="row mx-0 rounded-lg shadow-sm bg-white mb-3">
        <div className="col-md-2 px-2 py-2 rounded-left font-bold border border-right-0 d-flex align-items-center">
          Nombre
        </div>
        <div className="col-md-2 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Descripción
        </div>
        <div className="col-md-2 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Facebook
        </div>
        <div className="col-md-2 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Twitter
        </div>
        <div className="col-md-2 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Instagram
        </div>
        <div className="col-md-2 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Acciones
        </div>
      </div>

      {brands.map((row: Brand) => (
        <RowBrand
          key={row.id}
          brand={row}
          onEdit={(id: number) => toEdit(id)}
          onDelete={onDelete}
          onChangeStatus={onChangeStatus}
        />
      ))}

      {brands.length === 0 && <Empty />}

      <div className="row my-3">
        <div className="col-md">
          <Pagination pages={lastPage} active={page} onChange={onChangePage} />
        </div>
      </div>
    </div>
  );
}

const RowBrand: FunctionComponent<BrandProps> = (props) => {
  const { brand, onEdit, onDelete, onChangeStatus } = props;

  return (
    <div className="row mx-0 rounded-lg shadow-sm bg-white mb-3">
      <div className="col-md-2 px-2 py-2 bg-gray rounded-left font-bold d-flex align-items-center text-overflow-ellipsis">
        {brand.name}
      </div>
      <div className="col-md-2 px-2 py-2 bg-gray font-bold d-flex align-items-center text-overflow-ellipsis">
        {brand.description}
      </div>
      <div className="col-md-2 px-2 py-2 bg-gray font-bold d-flex align-items-center text-overflow-ellipsis">
        {brand.fb_url}
      </div>
      <div className="col-md-2 px-2 py-2 bg-gray font-bold d-flex align-items-center text-overflow-ellipsis">
        {brand.tw_url}
      </div>
      <div className="col-md-2 px-2 py-2 bg-gray font-bold d-flex align-items-center text-overflow-ellipsis">
        {brand.ig_url}
      </div>
      <div className="col-md-2 px-2 py-2 text-center border border-right-0 d-flex align-items-center justify-content-center">
        <Button
          color="dark"
          title="Ver ó Editar"
          small
          className="rounded rounded-circle"
          onClick={(_: React.FormEvent<HTMLButtonElement>): void =>
            onEdit(brand.id)
          }
        >
          <img src={editSvg} alt="Ver ó Editar" />
        </Button>
        <Button
          color="dark"
          title={brand.status ? "Activar" : "Desactivar"}
          small
          className="rounded rounded-circle"
          onClick={(_: React.FormEvent<HTMLButtonElement>): void =>
            onChangeStatus(brand.id)
          }
        >
          <Icon name={brand.status ? "check" : "times"} color="white" />
        </Button>
        <Button
          color="dark"
          title="Eliminar"
          small
          className="rounded rounded-circle"
          onClick={(_: React.FormEvent<HTMLButtonElement>): void =>
            onDelete(brand.id)
          }
        >
          <img src={deleteSvg} alt="Eliminar" />
        </Button>
      </div>
    </div>
  );
};
