import { BrandForm, Brand } from "models";

export type State = {
  isSubmitted: boolean;
  data: Brand[];
  form: BrandForm;
  image_url: boolean;
  logo: boolean;
  logoGold: boolean;
};

export const initialState: State = {
  isSubmitted: false,
  data: [],
  form: {
    image_url: "",
    name: "",
    description: "",
    fb_url: "",
    tw_url: "",
    ig_url: "",
    logo: "",
    logoGold: "",
    status: 0,
  },
  image_url: false,
  logo: false,
  logoGold: false,
};

export type BrandProps = {
  brand: Brand;
  onEdit: (id: number) => void;
  onDelete: (id: number) => void;
  onChangeStatus: (id: number) => void;
};
