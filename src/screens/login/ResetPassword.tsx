import React, { ReactNode } from "react";
import { Input, Button } from "components";
import { AuthService } from "services";
import { handlerError } from "functions";
import { STEP_FIRST, STEP_SECOND } from "utils";

type State = {
  submitted: boolean;
  step: number;
  resText: string;
  form: {
    email: string;
  };
};

type Props = {
  onClose: (event: React.MouseEvent<HTMLSpanElement, MouseEvent>) => void;
};

class ResetPassword extends React.PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      submitted: false,
      step: STEP_FIRST,
      resText: "",
      form: {
        email: "",
      },
    };
  }

  handleChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const { value, name } = event.currentTarget;

    this.setState(({ form }) => ({
      form: {
        ...form,
        [name]: value,
      },
    }));
  };

  handleSubmit = (event: React.FormEvent<HTMLFormElement>): void => {
    event.preventDefault();

    const { submitted, form } = this.state;

    if (!!!submitted) {
      this.setState({ submitted: true });

      AuthService.resetPassword({ email: form.email })
        .then((response: string) => {
          this.setState({ step: STEP_SECOND, resText: response });
        })
        .catch(handlerError)
        .finally(() => this.setState({ submitted: false }));
    }
  };

  render(): ReactNode {
    const { step, form, resText } = this.state;

    return step === STEP_FIRST ? (
      <form onSubmit={this.handleSubmit} noValidate autoComplete="off">
        <div className="row text-center">
          <div className="col col-md col-lg">
            <h2 className="text-dark font-heady">No te preocupes</h2>
          </div>
        </div>
        <div className="row mt-3">
          <div className="col col-md col-lg">
            <Input
              labelClass="font-min-gray"
              color="gray"
              label="Introduce tu correo electrónico y te enviaremos un link de recuperación"
              name="email"
              type="text"
              onChange={this.handleChange}
              value={form.email}
            />
          </div>
        </div>
        <div className="row">
          <div className="col col-md col-lg">
            <Button
              className="font-bold button-rounded"
              outline="orange"
              block
              type="button"
              onClick={this.props.onClose}
            >
              Cancelar
            </Button>
          </div>
          <div className="col col-md col-lg">
            <Button
              className="font-bold button-rounded"
              color="orange"
              shadow="shadow-button"
              block
              type="submit"
            >
              Enviar
            </Button>
          </div>
        </div>
      </form>
    ) : (
      <React.Fragment>
        <div className="row text-center">
          <div className="col col-md col-lg">
            <h2 className="text-dark font-heady">Verifica en tu correo</h2>
            <p className="mb-5 mt-4 font-weight-light">{resText}</p>
          </div>
        </div>
        <div className="row text-center">
          <div className="col col-md col-lg">
            <Button
              className="btn-xl button-rounded"
              color="orange"
              shadow="shadow-button"
              type="button"
              onClick={this.props.onClose}
            >
              Continuar
            </Button>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default ResetPassword;
