import React from "react";
import axios, { CancelTokenSource } from "axios";

import { connect, ConnectedProps } from "react-redux";
import { RouteComponentProps } from "react-router-dom";
import { Card, Input, Modal, Button } from "components";

import { RootState } from "reducers";
import { User, Token, Form } from "models";

import { AuthService } from "services";
import { handlerError } from "functions";

import { logo } from "assets/img";

import ResetPassword from "./ResetPassword";

const mapState = (state: RootState) => ({
  user: state.user,
  token: state.token,
  form: state.form,
});

const mapDispatch = {
  dispatchUser: (user: User) => ({ user: user, type: "User/SET" }),
  dispatchToken: (token: Token) => ({ token: token, type: "Token/SET" }),
  dispatchForm: (form: Form) => ({ form: form, type: "Form/SET" }),
};

const connector = connect(mapState, mapDispatch);

type Props = ConnectedProps<typeof connector> & RouteComponentProps;

const initialState = {
  errors: [],
  showErrors: false,
  submitted: false,
  shouldReset: false,
  form: {
    email: "",
    password: "",
  },
};

type State = Readonly<typeof initialState>;

class Login extends React.PureComponent<Props, State> {
  public state: State;
  public source: CancelTokenSource;

  constructor(props: Props) {
    super(props);

    this.state = initialState;
    const cancelToken = axios.CancelToken;

    this.source = cancelToken.source();
  }

  handleChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const { value, name } = event.currentTarget;

    this.setState(({ form }) => ({
      form: {
        ...form,
        [name]: value,
      },
    }));
  };

  toggleResetModal = (event: React.MouseEvent<HTMLSpanElement, MouseEvent>) => {
    this.setState((state) => ({ shouldReset: !state.shouldReset }));
  };

  handleSubmit = (event: React.FormEvent<HTMLFormElement>): void => {
    const { form, submitted } = this.state;
    const { history } = this.props;
    const { dispatchToken, dispatchUser } = this.props;

    event.preventDefault();

    if (submitted) {
      return;
    }

    this.setState({ submitted: true });

    AuthService.login(form, this.source)
      .then((auth) => {
        const { token, token_type, user } = auth;

        dispatchToken({
          token: token,
          type: token_type,
        });

        dispatchUser(user);

        history.replace("/admin/us");
      })
      .catch(handlerError)
      .finally(() => this.setState({ submitted: false }));
  };

  render() {
    const { submitted, form, shouldReset } = this.state;

    return (
      <main>
        <div className="backgroundColor">
          <div className="login-background">
            <div className="wrapper-content align-items-center">
              <div className="container">
                {shouldReset && (
                  <Modal
                    className="d-flex align-items-center"
                    onClose={this.toggleResetModal}
                    visible
                    header={false}
                  >
                    <ResetPassword onClose={this.toggleResetModal} />
                  </Modal>
                )}

                <div className="row justify-content-center">
                  <div className="row w-100">
                    <div className="col col-md col-lg text-center">
                      <img
                        className="img-fluid imgLogin"
                        src={logo}
                        alt="Logo"
                      />
                    </div>
                  </div>
                  <Card className="cardLogin shadow-dark">
                    <form
                      onSubmit={this.handleSubmit}
                      noValidate
                      autoComplete="off"
                    >
                      <div className="row">
                        <div className="col col-md col-lg">
                          <Input
                            color="white"
                            name="email"
                            type="text"
                            placeholder="Correo electrónico"
                            onChange={this.handleChange}
                            value={form.email}
                            className="poppins-regular"
                          />
                        </div>
                      </div>
                      <div className="row">
                        <div className="col col-md col-lg">
                          <Input
                            color="white"
                            name="password"
                            type="password"
                            placeholder="Contraseña"
                            onChange={this.handleChange}
                            value={form.password}
                            className="poppins-regular"
                          />
                        </div>
                      </div>
                      <div className="text-center row my-3">
                        <div className="col col-md col-lg">
                          <Button
                            className=" button-rounded buttonLogin poppins-regular"
                            submitted={submitted}
                            block
                            type="submit"
                          >
                            Inicia Sesión
                          </Button>
                        </div>
                      </div>
                    </form>
                    <div className="row text-center marginLogin">
                      <div className="col col-md col-lg">
                        <span
                          className="pointer text-dark font-smaller font-weight-bold missedButton poppins-regular"
                          onClick={this.toggleResetModal}
                        >
                          ¿Recuperar contraseña?
                        </span>
                      </div>
                    </div>
                    {/* <div className="row text-center marginLogin">
                      <div className="col col-md col-lg">
                        <span
                          className="pointer text-dark font-smaller font-weight-bold poppins-regular"
                          onClick={() => {}}
                        >
                          Registrarse
                        </span>
                      </div>
                    </div> */}
                  </Card>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    );
  }
}

export default connector(Login);
