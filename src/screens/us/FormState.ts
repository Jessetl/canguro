import { Empty } from "components";

export type State = {
  isSubmit: boolean;
  id: number;
  map: string;
  error: string;
  mision: string,
  mision_image: string,
  vision: string,
  vision_image: string,
  moral: string,
  us_image: string,
  kangaroo_to_world: string,
  identity_kangaroo: string,
  banner_image: string,
};

export const initialState: State = {
  isSubmit: false,
  map: "",
  id: 0,
  error: "",
  mision: "",
  mision_image:"",
  moral: "",
  us_image:"",
  vision: "",
  vision_image:"",
  kangaroo_to_world: "",
  identity_kangaroo: "",
  banner_image: "",
};

