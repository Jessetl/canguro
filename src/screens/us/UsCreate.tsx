import React, { ReactNode } from "react";
//import { RouteComponentProps } from "react-router-dom";
import axios, { CancelTokenSource } from "axios";

import { UsModel } from "models";

import { RouteComponentProps, withRouter } from "react-router";

import { Textarea, Button } from "components";
import { TitleModal, PageTitle } from "components";
import { handlerError, showSuccess } from "utils";
import { initialState, State } from "./FormState";
import UsService from "services/modules/usService";

type Props = RouteComponentProps;

class UsCreate extends React.PureComponent<Props, State> {
  public state: State;
  public source: CancelTokenSource;

  constructor(props: Props) {
    super(props);

    this.state = initialState;

    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();
  }

  uploadImage = (key: string) => {
    return async (e: React.ChangeEvent<HTMLInputElement>) => {
      if (e.currentTarget.files) {
        const file = e.currentTarget.files[0];
        const base64 = await this.convertBase64(file);
        this.setState((prevState: any) => {
          return {
            ...prevState,
            [key]: base64,
          };
        });

        //const file = e.currentTarget.files;
        //const base64 = await this.convertBase64(file);
      }
    };
  };

  convertBase64 = (file: any) => {
    return new Promise((resolve, reject) => {
      const fileReader: any = new FileReader();

      fileReader.onload = () => {
        resolve(fileReader.result);
      };

      fileReader.onerror = (error: string) => {
        reject(error);
      };

      fileReader.readAsDataURL(file);
    });
  };

  componentDidMount() {
    this.getUs();
  }

  getUs = () => {
    UsService.index()
      .then((us) => {
        if (us.length > 0) {
          this.setState((prevState: any) => {
            return {
              ...prevState,
              id: us[0].id,
              map: us[0].map,
              mision: us[0].mision,
              misionImage: us[0].mision_image,
              vision: us[0].vision,
              visionImage: us[0].vision_image,
              kangaroo_to_world: us[0].kangaroo_to_world,
              identity_kangaroo: us[0].identity_kangaroo,
              moral: us[0].moral,
              us_image: us[0].us_image,
            };
          });
        }
      })
      .catch((err) => handlerError(err));
  };

  handleChange = (key: string) => {
    return (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value, name } = event.currentTarget;
      this.setState((prevState: any) => {
        return {
          ...prevState,
          [key]: value,
        };
      });
    };
  };

  handleSubmit = async (
    event: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    event.preventDefault();
    const {
      mision,
      mision_image,
      vision,
      vision_image,
      kangaroo_to_world,
      identity_kangaroo,
      us_image,
      moral,
      map,
      id,
    } = this.state;
    if (this.state.isSubmit) {
      return;
    }

    this.setState({
      isSubmit: true,
    });

    const us: UsModel = {
      id: 0,
      map,
      mision,
      mision_image,
      vision,
      vision_image,
      kangaroo_to_world: "default",
      identity_kangaroo: "default",
      moral,
      us_image,
    };

    if (id > 0) {
      UsService.update(id, us)
        .then((us) => {
          showSuccess();
          setTimeout(() => {
            this.props.history.push("/admin/us");
          }, 1000);
        })
        .catch(handlerError)
        .finally(() => {
          this.setState({ isSubmit: false });
        });
    } else {
      UsService.store(us, this.source)
        .then(() => {
          showSuccess();

          setTimeout(() => {
            this.props.history.push("/admin/us");
          }, 1000);
        })
        .catch(handlerError)
        .finally(() => this.setState({ isSubmit: false }));
    }
  };

  render(): ReactNode {
    const {
      isSubmit,
      mision,
      mision_image,
      map,
      vision,
      vision_image,
      moral,
      us_image,
      kangaroo_to_world,
      identity_kangaroo,
    } = this.state;

    return (
      <div className="container">
        {/*<PageTitle backUrl="/admin/stories">Edición de nosotros</PageTitle>*/}
        <div className="row justify-content-center">
          <div className="col-12">
            <TitleModal className="pt-4 pb-3 text-center text-black poppins-semibold">
              Nosotros
            </TitleModal>
            <div className="card mt-auto mb-auto">
              <div className="card-body">
                <form
                  onSubmit={this.handleSubmit}
                  noValidate
                  autoComplete="off"
                >
                  <div className="row zoneMVCreate mb-4">
                    <div className="col-12 col-sm-12 col-md-6 text-center">
                      <Textarea
                        label={"Misión"}
                        name={"mision"}
                        value={mision}
                        onChange={this.handleChange("mision")}
                        className="poppins-regular"
                      />
                      <div className="text-center">
                        <label className="upload">
                          <input
                            type="file"
                            onChange={this.uploadImage("mision_image")}
                            accept="image/png, image/jpeg"
                          />
                        </label>
                      </div>
                    </div>
                    <div className="col-12 col-sm-12 col-md-6 text-center">
                      <Textarea
                        label={"Visión"}
                        name={"vision"}
                        value={vision}
                        onChange={this.handleChange("vision")}
                        className="poppins-regular"
                      />
                      <div className="text-center">
                        <label className="upload">
                          <input
                            type="file"
                            onChange={this.uploadImage("vision_image")}
                            accept="image/png, image/jpeg"
                          />
                        </label>
                      </div>
                    </div>
                  </div>

                  <div className="row zoneMVCreate mb-4">
                    <div className="col-12 col-sm-12 text-center">
                      <Textarea
                        label={"Descripcion"}
                        name={"moral"}
                        value={moral}
                        onChange={this.handleChange("moral")}
                        className="poppins-regular"
                      />
                    </div>
                    <div className="col-12 col-sm-12 text-center">
                      <label className="upload">
                        <input
                          type="file"
                          onChange={this.uploadImage("us_image")}
                          accept="image/png, image/jpeg"
                        />
                      </label>
                    </div>
                  </div>

                  {/*
                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <Textarea
                        label={'Canguro en el mundo'}
                        name={'kangaroo_to_world'}
                        value={kangaroo_to_world}
                        onChange={this.handleChange("kangaroo_to_world")}
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <Textarea
                        label={'Diferentes identidades Canguro '}
                        name={'identity_kangaroo '}
                        value={identity_kangaroo}
                        onChange={this.handleChange("identity_kangaroo")}
                      />


                    </div>
                  </div>
                  */}

                  <div className="text-center row">
                    <div className="col col-sm">
                      <Button
                        submitted={isSubmit}
                        block
                        type="submit"
                        className="btn-orange shadow text-black-important poppins-regular"
                      >
                        Guardar
                      </Button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(UsCreate);
