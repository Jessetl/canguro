import React, { ReactNode, useState, useEffect } from "react";
import axios, { CancelTokenSource } from "axios";
import { RouteComponentProps } from "react-router";

import { handlerError } from "utils";

import { puntos } from "assets/img";

//import { puntos } from "assets/img";

import { initialState, State } from "./FormState";
import UsService from "services/modules/usService";
import BrandService from "services/modules/brandService";
import BannerService from "services/modules/bannerService";
import { Brand } from "models";

import icoDescargas from "../../assets/img/canguro-descargas.png";

import {
  Header,
  ButtonsMobile,
  Accordion,
  Banner,
  Footer,
  UsArea,
} from "components";

const Brands = () => {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [brands, setBrands] = useState<Brand[]>([]);
  const cancelToken = axios.CancelToken;

  useEffect(() => {
    const source = cancelToken.source();

    (async () => {
      try {
        const { data } = await BrandService.index(source);
        setIsLoading(false);
        setBrands(data);
      } catch (err) {
        console.log("Error");
        handlerError(err);
      }

      return () => source.cancel("Cancel by user");
    })();
  }, [cancelToken]);

  let logo0 = "",
    logo1 = "",
    logo2 = "",
    logo3 = "";

  if (brands.length > 0) {
    for (let i = 0; i < brands.length; i++) {
      if (i === 0) {
        logo0 = brands[0].logo_url;
        logo1 = "";
        logo2 = "";
        logo3 = "";
      } else if (i === 1) {
        logo0 = brands[0].logo_url;
        logo1 = brands[1].logo_url;
        logo2 = "";
        logo3 = "";
      } else if (i === 2) {
        logo0 = brands[0].logo_url;
        logo1 = brands[1].logo_url;
        logo2 = brands[2].logo_url;
        logo3 = "";
      } else if (i === 3) {
        logo0 = brands[0].logo_url;
        logo1 = brands[1].logo_url;
        logo2 = brands[2].logo_url;
        logo3 = brands[3].logo_url;
      }
    }
  }

  return (
    <div className="row w-100 h-25 ml-auto mr-auto text-center mt-3 mb-3">
      {logo0 && (
        <div className="col-3">
          <img src={logo0} alt="logo" className="iconsHome" />
        </div>
      )}
      {logo1 && (
        <div className="col-3">
          <img src={logo1} alt="Logo" className="iconsHome" />
        </div>
      )}
      {logo2 && (
        <div className="col-3">
          <img src={logo2} alt="Logo" className="iconsHome" />
        </div>
      )}
      {logo3 && (
        <div className="col-3">
          <img src={logo3} alt="Logo" className="iconsHome" />
        </div>
      )}
    </div>
  );
};

type Props = RouteComponentProps;

class Us extends React.PureComponent<Props, State> {
  public state: State;
  public source: CancelTokenSource;

  constructor(props: Props) {
    super(props);

    this.state = initialState;

    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();
  }

  componentDidMount() {
    this.getUs();
    this.getBanners();
  }

  getUs = () => {
    UsService.index()
      .then((us) => {
        if (us.length > 0) {
          this.setState((prevState: any) => {
            return {
              ...prevState,
              id: us[0].id,
              mision: us[0].mision,
              mision_image: us[0].mision_image_url,
              vision: us[0].vision,
              vision_image: us[0].vision_image_url,
              moral: us[0].moral,
              us_image: us[0].us_image_url,
            };
          });
        }
      })
      .catch((err) => handlerError(err));
  };

  getBanners = () => {
    BannerService.getBannerGeneral(this.source)
      .then((banner) => {
        if (banner.length > 0) {
          this.setState((prevState: any) => {
            return {
              ...prevState,
              banner_image: banner[0].image_url,
            };
          });
        }
      })
      .catch((err) => handlerError(err));
  };

  render(): ReactNode {
    const {
      mision,
      mision_image,
      vision,
      vision_image,
      moral,
      us_image,
      banner_image,
    } = this.state;

    return (
      <div>
        <Banner title="Nosotros" banner_image={banner_image} />
        <UsArea title="Nosotros" message={moral} image={us_image} principal />

        <Brands />

        <div className="row paddingMV usBackground h-100 w-100">
          <div className="w-100 position-absolute">
            <div className="w-100 d-flex flex-row">
              <img src={puntos} alt="puntos" className=" pointsImageTop" />
            </div>

            <div className="w-100 d-flex flex-row-reverse">
              <img src={puntos} alt="puntos" className="pointsImage" />
            </div>
          </div>

          <div
            className="col-sm-5 col-10 cardMV m-auto mt-2 mb-2"
            style={{ backgroundImage: "url(" + mision_image + ")" }}
          >
            <p className="cardButton">
              <h2 className="poppins-medium text-white">Misión</h2>
              <p className="poppins-regular text-white text-center descriptionMision">
                {mision}
              </p>
            </p>
          </div>
          <div
            className="col-sm-5 col-10 cardMV m-auto mb-2"
            style={{ backgroundImage: "url(" + vision_image + ")" }}
          >
            <p className="cardButton">
              <h2 className="poppins-medium text-white">Visión</h2>
              <p className="poppins-regular text-white text-center descriptionMision">
                {vision}
              </p>
            </p>
          </div>
        </div>

        <div className="btn-sex">
          <div className="buttonMobile h-100 w-100 mb-3">
            <div className="container">
              <div className="row h-50">
                <div className="col-md-4 align-self-center">
                  <h1>
                    DESCARGA <br />
                    NUESTRA <br />
                    APP
                  </h1>
                </div>
                <div className="col-md-4 align-self-center">
                  <img
                    className="img-can"
                    alt="icoDescargas"
                    src={icoDescargas}
                  />
                </div>
                <div className="col-md-4 align-self-center">
                  <ButtonsMobile />
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="row p-0 m-0 w-100 usBackground pb-4">
          <div className="col-11 m-auto p-0 w-100">
            <p className="usTitleCarousel poppins-regular text-center">
              Efecto Canguro
            </p>
            <Accordion />
          </div>
        </div>
      </div>
    );
  }
}

export default Us;
