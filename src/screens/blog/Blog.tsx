import React, { useState, useEffect } from "react";
import { useTypedSelector } from "reducers";
import axios from "axios";

import { BlogService } from "services";
import { BlogModel } from "models";
import { handlerError, STATUS_ACTIVE } from "utils";
import { Banner, Posts } from "components";
import { Loading, PaginationPosts } from "components";
import { prop } from "functions";

import anuncio from "../../assets/img/anuncio.jpg";
import facebookSvg from "../../assets/icons/facebook-rounded.svg";
import instagramSvg from "../../assets/icons/instagram-rounded.svg";
import tiktokSvg from "../../assets/icons/tiktok-rounded.svg";
import { useHistory } from "react-router-dom";

const Blog = () => {
  const feed = useTypedSelector(prop("contact"));
  const history = useHistory();

  const [posts, setPosts]: any = useState<BlogModel[]>([]);
  const [blogVisited, setBlogVisited] = useState<BlogModel[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(true);

  const [total, setTotal] = useState(0);
  const [perPage, setPerPage] = useState(0);
  const [page, setPage] = useState<number>(1);

  const cancelToken = axios.CancelToken;

  useEffect(() => {
    const blogs = localStorage.getItem("posts");
    const source = cancelToken.source();

    BlogService.getAll(page, source)
      .then((response) => {
        const { data, total, per_page } = response;

        const rawBlog = data
          .map((blog) => {
            return {
              ...blog,
              image: blog.image_blogs[0] ? blog.image_blogs[0].image_url : "",
            };
          })
          .filter(({ status }) => status === STATUS_ACTIVE);

        if (!!blogs) {
          setBlogVisited(JSON.parse(blogs));
        }

        setPosts(rawBlog);
        setPerPage(per_page);
        setTotal(total);
        setIsLoading(false);
      })
      .catch(handlerError);
  }, [page, cancelToken]); // eslint-disable-line react-hooks/exhaustive-deps

  const goToBlog = (blogId: number) =>
    history.push({
      pathname: `/blog/${blogId}`,
      state: { posts: posts },
    });

  if (isLoading) {
    return <Loading />;
  }

  return (
    <div className="BgBackground">
      <Banner title="Blog" />
      <div className="row w-100 h-100 m-0">
        <div className="col-10 col-sm-12 col-md-3 col-lg-3 col-xl-3 p-0 columnView order-first order-sm-last">
          {!!blogVisited.length && (
            <div className="w-100 p-0">
              <div className="anuncio-blog">
                <div
                  style={{
                    position: "relative",
                    display: "flex",
                    justifyContent: "center",
                  }}
                >
                  <img src={anuncio} alt="Banner" className="anuncio" />

                  {!!feed && (
                    <React.Fragment>
                      {feed.facebook && (
                        <a
                          href={feed.facebook}
                          target="_blank"
                          rel="noopener noreferrer"
                          style={{
                            position: "absolute",
                            bottom: 15,
                            marginRight: 95,
                          }}
                        >
                          <img src={facebookSvg} alt="Facebook" width="35" />
                        </a>
                      )}
                      {feed.instagram && (
                        <a
                          href={feed.instagram}
                          target="_blank"
                          rel="noopener noreferrer"
                          style={{
                            position: "absolute",
                            bottom: 15,
                            marginRight: 95,
                            marginLeft: 95,
                          }}
                        >
                          <img src={instagramSvg} alt="Instagram" width="35" />
                        </a>
                      )}
                      {feed.twitter && (
                        <a
                          href={feed.twitter}
                          target="_blank"
                          rel="noopener noreferrer"
                          style={{
                            position: "absolute",
                            bottom: 15,
                            marginLeft: 95,
                          }}
                        >
                          <img src={tiktokSvg} alt="TikTok" width="35" />
                        </a>
                      )}
                    </React.Fragment>
                  )}
                </div>
              </div>

              <div className="etiquetas">
                <h2 className="textTitleVisitBlog pb-2 mt-2 pb-sm-2 mt-sm-0 poppins-regular text-center text-sm-left">
                  Etiquetas
                </h2>
                <div className="row mx-0">
                  {blogVisited.map((blog, key) => {
                    return (
                      <div
                        className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 py-1 px-0"
                        key={key.toString()}
                      >
                        <div className="box-eti text-overflow-ellipsis mr-2">
                          {blog.title}
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>

              <h2 className="textTitleVisitBlog pb-2 mt-2 pb-sm-2 mt-sm-0 poppins-regular text-center text-sm-left">
                Vistos
              </h2>
              {blogVisited.map((blog, key) => {
                return (
                  <div
                    className="row justify-content-center p-0 ml-auto mr-auto"
                    key={key.toString()}
                  >
                    <div
                      className="row col-sm-12 p-0 marginViews pointer"
                      onClick={() => goToBlog(blog.id)}
                    >
                      <div className="col-10 col-sm-6 m-0 p-0 w-100 ml-auto mr-auto imageVistBlog">
                        <img
                          src={blog.image}
                          alt="First"
                          className="w-100 m-auto imageVist"
                        />
                      </div>
                      <div className="col-10 col-sm-5 p-0 m-0 mx-auto">
                        <p className="p-0 poppins-regular text-white textVisitBlog text-center text-sm-left">
                          {blog.title}
                        </p>
                        <p className="popins-regular text-white textDescriptionVisitBlog text-center text-sm-left">
                          <span className="poppins-regular text-mute">
                            {blog.date_es}
                          </span>
                        </p>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          )}
        </div>

        <div
          className="col-10 col-sm-8 col-md-8 col-lg-8 col-xl-8 p-x mr-auto mr-md-none ml-auto ml-md-auto order-1 order-last order-sm-first
        "
        >
          <Posts posts={posts} />

          <PaginationPosts
            postsPerPage={perPage}
            totalPosts={total}
            paginate={(page: number) => setPage(page)}
            currentPage={page}
          />
        </div>
      </div>
    </div>
  );
};

export default Blog;
