import React, { ReactNode } from "react";
import axios, { CancelTokenSource } from "axios";
import { RouteComponentProps } from "react-router";

import { initialState, State } from "./FormState";
import { BlogForm, ImageBlogForm } from "models";
import { BlogService } from "services";
import { Input, Button } from "components";
import { Icon, Submitted, PageTitle } from "components";
import { handlerError, showSuccess } from "utils";
import { TYPE_POST } from "utils";

import { ContentBlock, DraftEditorCommand } from "draft-js";
import { getDefaultKeyBinding, RichUtils } from "draft-js";
import { Editor as BlockEditor, EditorState } from "draft-js";
import { stateToHTML } from "draft-js-export-html";

import ToggleButton from "react-bootstrap/ToggleButton";
import ButtonGroup from "react-bootstrap/ButtonGroup";

type Props = RouteComponentProps;

type BlockStyleControlsProps = {
  editorState: EditorState;
  onToggle: (style: string) => unknown;
};

type StyleButtonProps = {
  active: boolean;
  label: string;
  style: string;
  onToggle: (style: string) => unknown;
};

type InlineStyleControlsProps = {
  editorState: EditorState;
  onToggle: (style: string) => void;
};

const styleMap = {
  CODE: {
    backgroundColor: "rgba(0, 0, 0, 0.05)",
    fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
    fontSize: 16,
    padding: 2,
  },
};

class BlogCreate extends React.Component<Props, State> {
  public state: State;
  public source: CancelTokenSource;

  private inputFileRef: React.RefObject<HTMLInputElement>;
  private editorRef: React.RefObject<BlockEditor>;

  constructor(props: Props) {
    super(props);

    this.state = initialState;

    this.inputFileRef = React.createRef();
    this.editorRef = React.createRef();

    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();
  }

  handleChange = (key: string) => {
    return (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value, name } = event.currentTarget;

      this.setState((prevState: any) => {
        return {
          ...prevState,
          [key]: {
            ...prevState[key],
            [name]: value,
          },
        };
      });
    };
  };

  handleImageBlogs = (key: string) => {
    return async (event: React.ChangeEvent<HTMLInputElement>) => {
      const { name } = event.target;

      if (event.currentTarget.files) {
        const base64 = await convertBase64(event.currentTarget.files[0]);

        const parseImage: ImageBlogForm = {
          id: this.state.form.image_blogs.length,
          name: base64,
          image_url: "",
        };

        this.setState((prevState: any) => {
          return {
            ...prevState,
            form: {
              ...prevState.form,
              [name]: [...prevState.form[name], parseImage],
            },
          };
        });
      }
    };
  };

  handleSubmit = async (
    event: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    const { isSubmitted } = this.state;

    event.preventDefault();

    if (!!!isSubmitted) {
      this.setState({
        isSubmitted: true,
      });

      const parseBlog: BlogForm = {
        ...this.state.form,
        description: stateToHTML(this.state.editorState.getCurrentContent()),
      };

      try {
        await BlogService.store(parseBlog);

        this.setState({ isSubmitted: false });
        showSuccess();

        this.props.history.push("/admin/blogs");
      } catch (error) {
        this.setState({ isSubmitted: false });
        handlerError(error);
      }
    }
  };

  toggleBlockType = (blockType: string) => {
    this.setState((prevState: any) => {
      return {
        editorState: RichUtils.toggleBlockType(
          prevState.editorState,
          blockType
        ),
      };
    });
  };

  toggleInlineStyle = (inlineStyle: string) => {
    this.setState((prevState: any) => {
      return {
        editorState: RichUtils.toggleInlineStyle(
          prevState.editorState,
          inlineStyle
        ),
      };
    });
  };

  handleKeyCommand = (
    command: DraftEditorCommand,
    editorState: EditorState
  ) => {
    const newState = RichUtils.handleKeyCommand(editorState, command);
    if (newState) {
      this.setState({ editorState: newState });
      return "handled";
    }

    return "not-handled";
  };

  mapKeyToEditorCommand = (event: any) => {
    if (event.keyCode === 9 /* TAB */) {
      const newEditorState = RichUtils.onTab(event, this.state.editorState, 4);

      if (newEditorState !== this.state.editorState) {
        this.setState({ editorState: newEditorState });
      }

      return null;
    }

    return getDefaultKeyBinding(event);
  };

  focus = () => {
    this.editorRef.current?.focus();
  };

  render(): ReactNode {
    const { isSubmitted, form, editorState } = this.state;

    let className = "RichEditor-editor";

    const contentState = editorState.getCurrentContent();

    if (!contentState.hasText()) {
      if (contentState.getBlockMap().first().getType() !== "unstyled") {
        className += " RichEditor-hidePlaceholder";
      }
    }

    return (
      <div className="container">
        <PageTitle backUrl={`/admin/blogs`}>Registro de Blogs</PageTitle>
        <div className="row justify-content-center mt-5">
          <div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
            <div className="card">
              <div className="card-body">
                <form
                  onSubmit={this.handleSubmit}
                  noValidate
                  autoComplete="off"
                >
                  <ButtonGroup toggle className="w-100 mb-4">
                    {TYPE_POST.map((radio: any, idx: any) => (
                      <ToggleButton
                        key={idx}
                        type="radio"
                        variant="orange"
                        name="radio"
                        value={radio.value}
                        checked={form.type === radio.value}
                        onChange={(_) => {
                          this.setState((prevState: any) => {
                            return {
                              ...prevState,
                              form: {
                                ...prevState.form,
                                type: radio.value,
                              },
                            };
                          });
                        }}
                      >
                        {radio.name}
                      </ToggleButton>
                    ))}
                  </ButtonGroup>
                  <div className="row">
                    <div className="col col-md col-lg">
                      <Input
                        color="gray"
                        name="title"
                        type="text"
                        label="Título"
                        value={form.title}
                        onChange={this.handleChange("form")}
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col col-md col-lg">
                      <div className="RichEditor-root">
                        <BlockStyleControls
                          editorState={editorState}
                          onToggle={this.toggleBlockType}
                        />
                        <InlineStyleControls
                          editorState={editorState}
                          onToggle={this.toggleInlineStyle}
                        />
                      </div>
                      <div className={className} onClick={this.focus}>
                        <BlockEditor
                          blockStyleFn={getBlockStyle}
                          customStyleMap={styleMap}
                          editorState={editorState}
                          handleKeyCommand={this.handleKeyCommand}
                          keyBindingFn={this.mapKeyToEditorCommand}
                          onChange={(editorState) =>
                            this.setState({ editorState: editorState })
                          }
                          placeholder=""
                          ref={this.editorRef}
                          spellCheck={true}
                        />
                      </div>
                    </div>
                  </div>

                  <div className="row my-3 text-center">
                    <div className="col col-sm">
                      <input
                        ref={this.inputFileRef}
                        type="file"
                        id="image_blogs"
                        name="image_blogs"
                        accept="image/png, image/jpeg"
                        color="orange"
                        onChange={this.handleImageBlogs("form")}
                        hidden
                      />

                      <Button
                        type="button"
                        block
                        className="btn-orange shadow"
                        onClick={() => this.inputFileRef.current?.click()}
                      >
                        Agregar imagen
                      </Button>
                    </div>
                  </div>

                  {!!form.image_blogs && (
                    <div className="row">
                      {form.image_blogs.map((row, key) => {
                        return (
                          <div
                            className="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4"
                            key={key.toString()}
                          >
                            <label
                              htmlFor={`photo${key}`}
                              className="border rounded"
                            >
                              {row.name && (
                                <div
                                  className="img-blog"
                                  style={{
                                    backgroundImage: `url(${row.name})`,
                                    backgroundPosition: "center center",
                                    backgroundSize: "cover",
                                    width: 250,
                                    height: 250,
                                  }}
                                ></div>
                              )}
                            </label>
                            <Button
                              type="button"
                              color="red"
                              block
                              className="btn-danger mb-2"
                              onClick={(_) => {
                                this.setState((prevState: any) => {
                                  return {
                                    ...prevState,
                                    form: {
                                      ...prevState.form,
                                      image_blogs: prevState.form.image_blogs.filter(
                                        ({ id }: { id: number }) =>
                                          id !== row.id
                                      ),
                                    },
                                  };
                                });
                              }}
                            >
                              <Icon name="trash"></Icon>
                            </Button>
                          </div>
                        );
                      })}
                    </div>
                  )}

                  {isSubmitted ? (
                    <Submitted />
                  ) : (
                    <Button color="orange" type="submit" block>
                      Guardar
                    </Button>
                  )}
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const blockStyles = [
  { label: "H1", style: "header-one" },
  { label: "H2", style: "header-two" },
  { label: "H3", style: "header-three" },
  { label: "H4", style: "header-four" },
  { label: "H5", style: "header-five" },
  { label: "H6", style: "header-six" },
  { label: "Cita", style: "blockquote" },
  { label: "Lista", style: "unordered-list-item" },
  { label: "Lista ordenada", style: "ordered-list-item" },
];

var inlineStyles = [
  { label: "Negrita", style: "BOLD" },
  { label: "Cursiva", style: "ITALIC" },
  { label: "Subrayada", style: "UNDERLINE" },
];

const BlockStyleControls = ({
  editorState,
  onToggle,
}: BlockStyleControlsProps) => {
  const selection = editorState.getSelection();
  const blockType = editorState
    .getCurrentContent()
    .getBlockForKey(selection.getStartKey())
    .getType();

  return (
    <div className="RichEditor-controls">
      {blockStyles.map((type) => (
        <StyleButton
          key={type.label}
          active={type.style === blockType}
          label={type.label}
          onToggle={onToggle}
          style={type.style}
        />
      ))}
    </div>
  );
};

const StyleButton = ({ active, label, style, onToggle }: StyleButtonProps) => {
  return (
    <span
      className={`RichEditor-styleButton ${
        active ? "RichEditor-activeButton" : ""
      }`}
      onMouseDown={(event) => {
        event.preventDefault();
        onToggle(style);
      }}
    >
      {label}
    </span>
  );
};

const InlineStyleControls = ({
  editorState,
  onToggle,
}: InlineStyleControlsProps) => {
  const currentStyle = editorState.getCurrentInlineStyle();

  return (
    <div className="RichEditor-controls">
      {inlineStyles.map((type) => (
        <StyleButton
          key={type.label}
          active={currentStyle.has(type.style)}
          label={type.label}
          onToggle={onToggle}
          style={type.style}
        />
      ))}
    </div>
  );
};

function getBlockStyle(block: ContentBlock) {
  switch (block.getType()) {
    case "blockquote":
      return "RichEditor-blockquote";
    default:
      return "";
  }
}

const convertBase64 = (file: any) => {
  return new Promise((resolve, reject) => {
    const fileReader = new FileReader();

    fileReader.onload = () => {
      resolve(fileReader.result);
    };

    fileReader.onerror = (error) => {
      reject(error);
    };

    fileReader.readAsDataURL(file);
  });
};

export default BlogCreate;
