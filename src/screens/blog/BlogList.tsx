import React, { useState, useEffect, FunctionComponent } from "react";
import axios, { CancelTokenSource } from "axios";
import { useHistory } from "react-router-dom";

import { BlogProps } from "./FormState";
import { BlogModel } from "models";
import { BlogService } from "services";
import { showSuccess, confirm, handlerError } from "utils";
import { Button, Empty, Pagination, Loading, Icon } from "components";

import editSvg from "assets/icons/edit.svg";
import deleteSvg from "assets/icons/delete.svg";

export default function BlogList() {
  const history = useHistory();
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [blogs, setBlogs] = useState<BlogModel[]>([]);
  const [lastPage, setLastPage] = useState<number>(1);
  const [page, setPage] = useState<number>(1);

  const cancelToken = axios.CancelToken;

  useEffect(() => {
    const source = cancelToken.source();

    (async () => {
      getAll(source);
      return () => source.cancel("Cancel by user");
    })();
  }, [page, cancelToken]); // eslint-disable-line react-hooks/exhaustive-deps

  const getAll = (source: CancelTokenSource) => {
    BlogService.getAll(page, source)
      .then((response) => {
        const { data, last_page, to } = response;

        setBlogs(data);
        setLastPage(last_page);

        if (!to && page > 1) {
          setPage(page - 1);
        }
      })
      .catch(handlerError)
      .finally(() => setIsLoading(false));
  };

  const onChangePage = async (page: number) => {
    setIsLoading(true);

    const source = cancelToken.source();

    setPage(page);
    getAll(source);
  };

  const onDelete = async (blogId: number) => {
    const hasDelete = await confirm({ text: "¿Desea eliminar esta entrada?" });

    if (!!hasDelete) {
      const source = cancelToken.source();

      await BlogService.delete(blogId);

      showSuccess();
      getAll(source);
    }
  };

  const onChangeStatus = async (blogId: number) => {
    const find = blogs.find(({ id }) => id === blogId);
    const key = blogs.findIndex(({ id }) => id === blogId);

    if (!!find) {
      try {
        const blog = await BlogService.status(blogId);

        let copyBlogs = [...blogs];

        copyBlogs[key] = {
          ...copyBlogs[key],
          status: blog.status,
        };

        showSuccess();
        setBlogs(copyBlogs);
      } catch (error) {
        handlerError(error);
      }
    }
  };

  const toCreate = () => history.push("/admin/blogs/create");
  const toEdit = (id: number) => history.push(`/admin/blogs/${id}/edit`);

  if (isLoading) {
    return <Loading />;
  }

  return (
    <div className="container">
      <div className="row mb-3">
        <div className="col col-sm-12 col-md-12 col-lg-12 col-xl-12">
          <h2 className="text-black my-3">Blogs</h2>
        </div>
      </div>

      <div className="row mb-3">
        <div className="col col-sm-12 col-md-12 col-lg-12 col-xl-12">
          <Button
            color="orange"
            title="Crear Blog"
            className="float-right text-dark"
            onClick={toCreate}
          >
            Crear Nuevo
          </Button>
        </div>
      </div>

      <div className="row mx-0 rounded-lg shadow-sm bg-white mb-3">
        <div className="col-md-3 px-2 py-2 rounded-left font-bold d-flex align-items-center">
          Titulo
        </div>
        <div className="col-md-4 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Descripción
        </div>
        <div className="col-md-3 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Tipo
        </div>
        <div className="col-md-2 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Acciones
        </div>
      </div>

      {blogs.map((blog) => (
        <RowBanner
          key={blog.id}
          blog={blog}
          onEdit={toEdit}
          onChangeStatus={onChangeStatus}
          onDelete={onDelete}
        />
      ))}

      {blogs.length === 0 && <Empty />}

      <div className="row my-3">
        <div className="col-md">
          <Pagination pages={lastPage} active={page} onChange={onChangePage} />
        </div>
      </div>
    </div>
  );
}

const RowBanner: FunctionComponent<BlogProps> = (props) => {
  const { blog, onEdit, onDelete, onChangeStatus } = props;

  return (
    <div className="row mx-0 rounded-lg shadow-sm bg-white mb-3">
      <div className="col-md-3 px-2 py-2 bg-gray rounded-left font-bold d-flex align-items-center text-overflow-ellipsis">
        {blog.title}
      </div>
      <div className="col-md-4 px-2 py-2 bg-gray font-bold d-flex align-items-center text-overflow-ellipsis">
        <span dangerouslySetInnerHTML={{ __html: blog.description }}></span>
      </div>
      <div className="col-md-3 px-2 py-2 bg-gray font-bold d-flex align-items-center text-overflow-ellipsis">
        {blog.type}
      </div>
      <div className="col-md-2 px-2 py-2 text-center border border-right-0 d-flex align-items-center justify-content-center">
        <Button
          color="dark"
          title="Editar"
          small
          className="rounded rounded-circle"
          onClick={(e: React.FormEvent<HTMLButtonElement>): void => {
            onEdit(blog.id);
          }}
        >
          <img src={editSvg} alt="Editar" />
        </Button>
        <Button
          color="dark"
          title={blog.status ? "Desactivar" : "Activar"}
          small
          className="rounded rounded-circle"
          onClick={(_: React.FormEvent<HTMLButtonElement>): void =>
            onChangeStatus(blog.id)
          }
        >
          <Icon name={blog.status ? "times" : "check"} color="white" />
        </Button>
        <Button
          color="dark"
          title="Eliminar"
          small
          className="rounded rounded-circle"
          onClick={(e: React.FormEvent<HTMLButtonElement>): void => {
            onDelete(blog.id);
          }}
        >
          <img src={deleteSvg} alt="eliminar" />
        </Button>
      </div>
    </div>
  );
};
