import { BlogModel, BlogForm } from "models";
import { EditorState } from "draft-js";

export type State = {
  isSubmitted: boolean;
  error: string;
  form: BlogForm;
  editorState: EditorState;
};

export const initialState: State = {
  isSubmitted: false,
  error: "",
  form: {
    title: "",
    description: "",
    type: "Noticia",
    status: 0,
    image: "",
    image_blogs: [],
    image_deleted: [],
  },
  editorState: EditorState.createEmpty(),
};

export type BlogProps = {
  blog: BlogModel;
  onEdit: (id: number) => void;
  onDelete: (id: number) => void;
  onChangeStatus: (id: number) => void;
};
